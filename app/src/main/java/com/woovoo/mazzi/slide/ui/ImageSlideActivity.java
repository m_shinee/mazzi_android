package com.woovoo.mazzi.slide.ui;

import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.msg.util.MsgPreferences;
import com.woovoo.mazzi.slide.ViewPagerAdapter;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.util.UrlConstants;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;

public class ImageSlideActivity extends AppCompatActivity {

    private static final String LOG_TAG = "ImageSlideActivity";
    private static final String CHANNEL_ID = "Mazzi_download";

    private JSONArray files;

    private ViewPager viewPager;
    private int previous = 0, currentPosition;
    private SimpleExoPlayerView simpleExoPlayerView;
    private DownloadManager downloadManager;
    private ArrayList<Long> downloading = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slide);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

        registerReceiver(onComplete,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        files = ProfilePreferences.getInstance().getFiles(MsgPreferences.getInstance().getConversationId());
        currentPosition = getFilePosition(getIntent().getStringExtra("selected_file"));

        ViewPagerAdapter adapter = new ViewPagerAdapter(getApplicationContext(), files);
        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(currentPosition);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;
                View view = viewPager.findViewWithTag(previous);
                if (view != null) {
                    simpleExoPlayerView = view.findViewById(R.id.videoView);
                    simpleExoPlayerView.getPlayer().setPlayWhenReady(false);
                }
                previous = viewPager.getCurrentItem();
                position += 1;
                setTitle(position + " of " + files.length());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setTitle(currentPosition + 1 + " of " + files.length());
        previous = viewPager.getCurrentItem();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.download:
                getUrl();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.image_slide_menu, menu);
        return true;
    }

    @Override
    protected void onPause() {
        View view = viewPager.findViewWithTag(currentPosition);
        if (view != null) {
            simpleExoPlayerView = view.findViewById(R.id.videoView);
            simpleExoPlayerView.getPlayer().setPlayWhenReady(false);
        }
        super.onPause();
    }

    private void getUrl() {
        try {
            String url = UrlConstants.getFileUrl(files.getString(currentPosition));
            if (url.contains("video")) {
                url += ".mp4";
            }
            tryDownload(url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void tryDownload(String url) {
        String[] split = url.split("/");
        if (split.length == 0) {
            return;
        }

        String name = split[split.length - 1];
        Uri uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setVisibleInDownloadsUi(true);
        request.setTitle(name);
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, name);
        downloading.add(downloadManager.enqueue(request));
    }

    private int getFilePosition(String fileName) {
        for (int i = files.length() - 1; i >= 0; i--) {
            try {
                if (fileName.equals(files.getString(i))) {
                    return i;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return files.length() - 1;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createNotificationChannel() {
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build();

        NotificationChannel channel = new NotificationChannel(CHANNEL_ID, getString(R.string.download_completed), NotificationManager.IMPORTANCE_HIGH);
        channel.enableLights(true);
        channel.enableVibration(true);
        channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            notificationManager.createNotificationChannel(channel);
        }
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            downloading.remove(referenceId);
            int notificationId = (int) referenceId;

            Intent fileIntent = new Intent();
            fileIntent.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), notificationId, fileIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel();
            }

            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                    .setContentTitle("Mazzi")
                    .setSmallIcon(R.mipmap.ic_logo)
                    .setContentText(getText(R.string.download_completed))
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                builder.setColor(getColor(R.color.colorPrimary));
            }

            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getApplicationContext());
            notificationManager.notify(notificationId, builder.build());
        }
    };

}