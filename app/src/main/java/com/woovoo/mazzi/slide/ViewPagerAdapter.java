package com.woovoo.mazzi.slide;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.UrlConstants;

import org.json.JSONArray;
import org.json.JSONException;

public class ViewPagerAdapter extends PagerAdapter {

    private RequestOptions requestOptions = new RequestOptions()
            .transforms(new FitCenter())
            .override(LinearLayout.LayoutParams.MATCH_PARENT);

    private Context context;
    private JSONArray files;
    private View itemView = null;

    public ViewPagerAdapter(Context context, JSONArray files) {
        this.context = context;
        this.files = files;
    }

    @Override
    public int getCount() {
        return files.length();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            String url = UrlConstants.getFileUrl(files.getString(position));
            if (files.getString(position).contains("video")) {
                itemView = inflater.inflate(R.layout.viewpager_video_item, container, false);
                itemView.setTag(position);
                SimpleExoPlayerView simpleExoPlayerView = itemView.findViewById(R.id.videoView);
                BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                TrackSelector trackSelector = new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(bandwidthMeter));
                SimpleExoPlayer simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(context, trackSelector);

                DefaultHttpDataSourceFactory defaultHttpDataSourceFactory = new DefaultHttpDataSourceFactory("mazzi_video");
                ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

                url += ".mp4";

                MediaSource mediaSource = new ExtractorMediaSource(Uri.parse(url), defaultHttpDataSourceFactory, extractorsFactory, null, null);
                simpleExoPlayerView.setPlayer(simpleExoPlayer);
                simpleExoPlayer.prepare(mediaSource);
                simpleExoPlayer.setPlayWhenReady(false);
            } else {
                itemView = inflater.inflate(R.layout.viewpager_image_item, container, false);
                ImageView imageView = itemView.findViewById(R.id.imageView);
                url = url.replace("thumb", "original");
                Glide.with(context)
                        .load(url)
                        .apply(requestOptions)
                        .into(imageView);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}