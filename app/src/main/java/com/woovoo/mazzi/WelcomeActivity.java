package com.woovoo.mazzi;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.woovoo.mazzi.authenticate.ui.LoginActivity;
import com.woovoo.mazzi.main.ui.MainActivity;
import com.woovoo.mazzi.profile.util.ProfilePreferences;

public class WelcomeActivity extends AppCompatActivity {

    private static final String LOG_TAG = "WelcomeActivity";

    private int PERMISSION_REQUEST = 20;
    private final String[] permissions = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        if (isPermissionsAllowed()) {
            launchApp();
            return;
        }
        requestStoragePermission();
    }

    //We are calling this method to check the permission status
    private boolean isPermissionsAllowed() {
        int result;
        for (String permission : permissions) {
            result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }

        return true;
    }

    //Requesting permission
    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST);
    }

    private void launchApp() {
        int SPLASH_TIME_OUT = 500;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent;
                if (ProfilePreferences.getInstance().getId() == null || ProfilePreferences.getInstance().getId().isEmpty()) {
                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                } else {
                    intent = new Intent(getApplicationContext(), MainActivity.class);
                }
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Checking the request code of our request
        if (requestCode == PERMISSION_REQUEST) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Launch app
                launchApp();
            } else {
                //Displaying another toast if permission is not granted
                AlertDialog dialog = new AlertDialog.Builder(getApplicationContext()).create();
                dialog.setTitle(getString(R.string.warning));
                dialog.setMessage(getString(R.string.permission_message));
                dialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        requestStoragePermission();
                    }
                });
                dialog.show();
            }
        }
    }
}