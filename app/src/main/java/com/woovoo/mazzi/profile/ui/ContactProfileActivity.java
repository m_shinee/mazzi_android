package com.woovoo.mazzi.profile.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.call.ui.DialingActivity;
import com.woovoo.mazzi.call.util.CallNetworkUtils;
import com.woovoo.mazzi.contact.util.Contact;
import com.woovoo.mazzi.msg.ui.MsgActivity;
import com.woovoo.mazzi.msg.util.MsgPreferences;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.util.UrlConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ContactProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG = "ContactProfileActivity";

    private ImageButton chatImageButton,
            voiceCallImageButton,
            videoCallImageButton;

    private TextView nicknameTextView,
            phoneTextView,
            birthdayTextView,
            emailTextView;

    private ImageView profileImageView;

    private Contact contact;
    private String conversationId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_profile);

        setTitle("");

        Toolbar toolbar = findViewById(R.id.profile_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        chatImageButton = findViewById(R.id.chatImageButton);
        voiceCallImageButton = findViewById(R.id.voiceCallImageButton);
        videoCallImageButton = findViewById(R.id.videoCallImageButton);

        nicknameTextView = findViewById(R.id.nicknameTextView);
        phoneTextView = findViewById(R.id.phoneTextView);
        birthdayTextView = findViewById(R.id.birthdayTextView);
        emailTextView = findViewById(R.id.emailTextView);

        profileImageView = findViewById(R.id.profileImageView);

        chatImageButton.setOnClickListener(this);
        voiceCallImageButton.setOnClickListener(this);
        videoCallImageButton.setOnClickListener(this);

        contact = new Contact();

        JSONArray userId = new JSONArray();
        userId.put(getIntent().getStringExtra("user_id"));

        JSONObject params = new JSONObject();
        try {
            params.put("phone_numbers", userId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MazziNetwork.postANRequest(UrlConstants.getContactProfile(), params)
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("error")) {
                                JSONArray array = response.getJSONArray("result");
                                contact.parseFromJSONObject(array.getJSONObject(0));
                                setter(contact);
                                fetchMsg();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(LOG_TAG, anError.getErrorDetail());
                    }
                });

    }

    private void setter(Contact contact) {
        nicknameTextView.setText(contact.getNickname());
        phoneTextView.setText(contact.getPhone());
        birthdayTextView.setText(contact.getBirthday() > 0 ? MyDateFormat.generate(contact.getBirthday(), "yy/mm/dd") : "");
        emailTextView.setText(contact.getEmail());

        if (!contact.getImage().trim().isEmpty()) {
            profileImageView.setPadding(0, 0, 0, 0);
            Glide.with(getApplicationContext())
                    .load(UrlConstants.getFileUrl(contact.getImage()))
                    .apply(RequestOptions.centerCropTransform())
                    .into(profileImageView);
        }
    }

    private void fetchMsg() {
        JSONObject params = new JSONObject();
        try {
            params.put("user_id", ProfilePreferences.getInstance().getId());
            params.put("participant_id", contact.getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final JSONArray participants = new JSONArray();
        participants.put(ProfilePreferences.getInstance().getId());
        participants.put(contact.getUserId());

        MazziNetwork.postANRequest(UrlConstants.createPrivateConversation(), params)
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("error")) {
                                conversationId = response.getString("_id");
                                CallNetworkUtils.getInstance().setConversationId(conversationId);
                                CallNetworkUtils.getInstance().setCallerName(contact.getNickname());
                                CallNetworkUtils.getInstance().setParticipantId(contact.getUserId());
                                MsgPreferences.getInstance().setConversationId(conversationId);
                                MsgPreferences.getInstance().setId("");
                                MsgPreferences.getInstance().setSeen(true);
                                MsgPreferences.getInstance().setParticipants(participants);
                                participants.remove(0);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(LOG_TAG, anError.getLocalizedMessage());
                    }
                });
    }

    private void goToMsgActivity() {
        Intent intent = new Intent(getApplicationContext(), MsgActivity.class);
        intent.putExtra("title", contact.getNickname());
        startActivity(intent);
    }

    private void goToDialingActivity(boolean isVideoCall) {
        CallNetworkUtils.getInstance().setIsVideo(isVideoCall);
        Intent intent = new Intent(getApplicationContext(), DialingActivity.class);
        intent.putExtra("is_video", isVideoCall);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (conversationId == null) {
            Toast.makeText(getApplicationContext(), getString(R.string.quota_exceeded), Toast.LENGTH_SHORT).show();
            return;
        }

        switch (v.getId()) {
            case R.id.chatImageButton:
                goToMsgActivity();
                break;
            case R.id.voiceCallImageButton:
                goToDialingActivity(false);
                break;
            case R.id.videoCallImageButton:
                goToDialingActivity(true);
                break;
        }
    }
}