package com.woovoo.mazzi.profile.util;

public class Profile {

    public static final String ID = "_id";
    public static final String IMAGE = "image";
    public static final String NICKNAME = "nickname";
    public static final String LASTNAME = "lastname";
    public static final String SURNAME = "surname";
    public static final String PHONE = "phone";
    public static final String KEY = "key";
    public static final String BIRTHDAY = "birthday";
    public static final String EMAIL = "email";
    public static final String GENDER = "gender";
    public static final String REGION = "region";
    public static final String CONFIRMED = "is_confirmed";
    public static final String DEVICE_ID = "device_id";
    public static final String CREATED_AT = "created_at";
    public static final String ONLINE_AT = "online_at";
    public static final String STATE = "state";

}