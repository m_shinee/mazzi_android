package com.woovoo.mazzi.profile.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.woovoo.mazzi.util.MyDateFormat;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.UUID;

public class ProfilePreferences extends Profile {

    private static final String LOG_TAG = "ProfilePreferences";
    private static ProfilePreferences instance;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private ProfilePreferences(Context context) {
        preferences = context.getSharedPreferences(LOG_TAG, Context.MODE_PRIVATE);
    }

    public static ProfilePreferences getInstance(Context context) {
        if (instance == null) {
            instance = new ProfilePreferences(context.getApplicationContext());
        }
        return instance;
    }

    public static ProfilePreferences getInstance() {
        if (instance != null)
            return instance;

        throw new IllegalArgumentException("Should use getInstance(Context) at least once before using this method.");
    }

    public void clear() {
        doEdit();
        clearAll();
        doApply();
    }

    private void clearAll() {
        editor.clear();
    }

    @SuppressLint("CommitPrefEdits")
    private void doEdit() {
        editor = preferences.edit();
    }

    private void doApply() {
        if (editor != null) {
            editor.apply();
            editor = null;
        }
    }

    public void putId(String id) {
        doEdit();
        editor.putString(ID, id);
        doApply();
    }

    public void putNickname(String nickname) {
        doEdit();
        editor.putString(NICKNAME, nickname);
        doApply();
    }

    public void putLastname(String lastname) {
        doEdit();
        editor.putString(LASTNAME, lastname);
        doApply();
    }

    public void putPhone(String phone) {
        doEdit();
        editor.putString(PHONE, phone);
        doApply();
    }

    public void putImage(String image) {
        doEdit();
        editor.putString(IMAGE, image);
        doApply();
    }

    public void putRegion(String region) {
        doEdit();
        editor.putString(REGION, region);
        doApply();
    }

    public void putEmail(String email) {
        doEdit();
        editor.putString(EMAIL, email);
        doApply();
    }

    public void putSurname(String surname) {
        doEdit();
        editor.putString(SURNAME, surname);
        doApply();
    }

    public void putBirthday(long birthday) {
        doEdit();
        editor.putLong(BIRTHDAY, birthday);
        doApply();
    }

    public void putConfirmed(boolean confirmed) {
        doEdit();
        editor.putBoolean(CONFIRMED, confirmed);
        doApply();
    }

    public void putOnlineAt(long onlineAt) {
        doEdit();
        editor.putLong(ONLINE_AT, onlineAt);
        doApply();
    }

    public void putDeviceId() {
        if (getDeviceId() == null) {
            doEdit();
            editor.putString(DEVICE_ID, UUID.randomUUID().toString());
            doApply();
        }
    }

    public boolean isHuaweiProtected() {
        return preferences.getBoolean("protected", false);
    }

    public String getId() {
        return preferences.getString(ID, null);
    }

    public String getNickname() {
        return preferences.getString(NICKNAME, null);
    }

    public String getLastname() {
        return preferences.getString(LASTNAME, null);
    }

    public String getRegion() {
        return preferences.getString(REGION, null);
    }

    public String getPhone() {
        return preferences.getString(PHONE, null);
    }

    public String getImage() {
        return preferences.getString(IMAGE, null);
    }

    public String getEmail() {
        return preferences.getString(EMAIL, null);
    }

    public String getSurname() {
        return preferences.getString(SURNAME, null);
    }

    public boolean isConfirmed() {
        return preferences.getBoolean(CONFIRMED, false);
    }

    public long getBirthday() {
        return preferences.getLong(BIRTHDAY, MyDateFormat.currentTime());
    }

    public long getOnlineAt() {
        return preferences.getLong(ONLINE_AT, 0);
    }

    public String getDeviceId() {
        return preferences.getString(DEVICE_ID, null);
    }

    public int getPlayingMediaPosition() {
        return preferences.getInt("media_position", -1);
    }

    public String getAccessToken() {
        return preferences.getString("access_token", "");
    }

    public boolean appIsOpened() {
        return preferences.getBoolean("opened", false);
    }

    public void putFiles(String chatId, String jsonFilesString) {
        doEdit();
        editor.putString(chatId, jsonFilesString);
        doApply();
    }

    public void putPlayingMediaPosition(int position) {
        doEdit();
        editor.putInt("media_position", position);
        doApply();
    }

    public void putAccessToken(String token) {
        doEdit();
        editor.putString("access_token", token);
        doApply();
    }

    public void putHuaweiProtected(boolean mProtected) {
        doEdit();
        editor.putBoolean("protected", mProtected);
        doApply();
    }

    public void setAppIsOpened(boolean opened) {
        doEdit();
        editor.putBoolean("opened", opened);
        doApply();
    }

    public JSONArray getFiles(String conversationId) {
        String files = preferences.getString(conversationId, null);
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray = new JSONArray(files);
            return jsonArray;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public boolean exist(String key) {
        return preferences.contains(key);
    }

}
