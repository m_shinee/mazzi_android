package com.woovoo.mazzi.profile.ui;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.woovoo.mazzi.camera.ui.PhotoKitActivity;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.util.UrlConstants;
import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.profile.util.Profile;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private final int ACTIVITY_START_CAMERA_APP = 0, SELECT_FILE_FROM_GALLERY = 1;

    private static final String LOG_TAG = "ProfileActivity";
    private static long birthdayTimeInMillis;
    private String profilePicPath;

    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private EditText nickname, lastname, surname, email;
    private ImageView profile_pic;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        profile_pic = findViewById(R.id.profile_pic);
        TextView birthday = findViewById(R.id.birthday);
        Button saveButton = findViewById(R.id.save);
        ImageButton choosePhoto = findViewById(R.id.choosePhoto);
        ImageButton choosePhotoFromGallery = findViewById(R.id.choosePhotoFromGallery);
        nickname = findViewById(R.id.nickname);
        lastname = findViewById(R.id.lastname);
        surname = findViewById(R.id.surname);
        email = findViewById(R.id.email);
        profilePicPath = ProfilePreferences.getInstance().getImage();

        if (!ProfilePreferences.getInstance().getImage().trim().isEmpty()) {
            profile_pic.setPadding(0, 0, 0, 0);
            Glide.with(getApplicationContext())
                    .load(UrlConstants.getFileUrl(ProfilePreferences.getInstance().getImage()))
                    .apply(RequestOptions.centerCropTransform())
                    .into(profile_pic);
        }
        Toolbar toolbar = findViewById(R.id.profile_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        nickname.setText(ProfilePreferences.getInstance().getNickname());
        lastname.setText(ProfilePreferences.getInstance().getLastname());
        surname.setText(ProfilePreferences.getInstance().getSurname());
        email.setText(ProfilePreferences.getInstance().getEmail());
        birthday.setText(MyDateFormat.generate(ProfilePreferences.getInstance().getBirthday(), "yyyy/MM/dd"));

        TextView phone = findViewById(R.id.profile_phone);
        phone.setText(String.format("+(%s) %s", ProfilePreferences.getInstance().getRegion(), ProfilePreferences.getInstance().getPhone()));

        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int year = Integer.parseInt(MyDateFormat.generate(ProfilePreferences.getInstance().getBirthday(), "yyyy"));
                int month = Integer.parseInt(MyDateFormat.generate(ProfilePreferences.getInstance().getBirthday(), "M"));
                int day = Integer.parseInt(MyDateFormat.generate(ProfilePreferences.getInstance().getBirthday(), "d"));

                DatePickerDialog dialog = new DatePickerDialog(
                        ProfileActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month - 1, day
                );

                if (dialog.getWindow() != null) {
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }
            }
        });

        mDateSetListener = new MyOnDateSetListener(birthday);

        saveButton.setOnClickListener(this);
        choosePhoto.setOnClickListener(this);
        choosePhotoFromGallery.setOnClickListener(this);

        choosePhotoFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save:
                saveButtonClicked();
                break;
            case R.id.choosePhoto:
                goToPhotoKitActivity();
                break;
            case R.id.choosePhotoFromGallery:
                openGallery();
                break;
        }
    }

    private static class MyOnDateSetListener implements DatePickerDialog.OnDateSetListener {
        private final TextView birthday;

        MyOnDateSetListener(TextView birthday) {
            this.birthday = birthday;
        }

        @Override
        public void onDateSet(DatePicker datePicker, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth());
            month += 1;
            String date = year + "/" + month + "/" + day;
            birthday.setText(date);
            birthdayTimeInMillis = calendar.getTimeInMillis();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {
            uploadImage(data.getStringExtra("filepath"));
        } else if (requestCode == SELECT_FILE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri selectedMediaUri = data.getData();
            if (selectedMediaUri != null) {
                String filePath = getRealPathFromUri(selectedMediaUri);
                uploadImage(filePath);
            }
        }
    }

    private void saveButtonClicked() {
        final JSONObject params = new JSONObject();
        try {
            params.put(ProfilePreferences.ID, ProfilePreferences.getInstance().getId());
            params.put(ProfilePreferences.NICKNAME, nickname.getText().toString());
            params.put(ProfilePreferences.LASTNAME, lastname.getText().toString());
            params.put(ProfilePreferences.SURNAME, surname.getText().toString());
            params.put(ProfilePreferences.EMAIL, email.getText().toString());
            params.put(ProfilePreferences.GENDER, "");
            params.put(ProfilePreferences.BIRTHDAY, birthdayTimeInMillis);
            params.put(ProfilePreferences.IMAGE, profilePicPath);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MazziNetwork.postANRequest(UrlConstants.updateProfile(), params)
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProfilePreferences.getInstance().putNickname(nickname.getText().toString());
                        ProfilePreferences.getInstance().putLastname(lastname.getText().toString());
                        ProfilePreferences.getInstance().putSurname(surname.getText().toString());
                        ProfilePreferences.getInstance().putEmail(email.getText().toString());
                        ProfilePreferences.getInstance().putBirthday(birthdayTimeInMillis);
                        Toast.makeText(getApplicationContext(), getString(R.string.success), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void goToPhotoKitActivity() {
        Intent intent = new Intent(getApplicationContext(), PhotoKitActivity.class);
        startActivityForResult(intent, ACTIVITY_START_CAMERA_APP);
    }

    private void openGallery() {
        Intent callGalleryApplicationIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        callGalleryApplicationIntent.setType("image/*");
        startActivityForResult(callGalleryApplicationIntent, SELECT_FILE_FROM_GALLERY);
    }

    public String getRealPathFromUri(Uri mediaUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(mediaUri, proj, null, null, null);
            if (cursor != null) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                String name = cursor.getString(columnIndex);
                cursor.close();
                return name;
            }
            return null;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void uploadImage(String filePath) {
        AndroidNetworking.upload(UrlConstants.uploadImage())
                .addMultipartFile("file", new File(filePath))
                .addMultipartParameter("conversation_id", ProfilePreferences.getInstance().getId())
                .setTag(LOG_TAG)
                .setPriority(Priority.MEDIUM)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        Log.d(LOG_TAG, String.valueOf(bytesUploaded));
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            profilePicPath = response.getString("url").replace("\\", "");
                            ProfilePreferences.getInstance().putImage(profilePicPath);

                            profile_pic.setPadding(0, 0, 0, 0);
                            Glide.with(getApplicationContext())
                                    .load(UrlConstants.getFileUrl(profilePicPath))
                                    .apply(RequestOptions.centerCropTransform())
                                    .into(profile_pic);

                            EventBus.getDefault().post(new Profile());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}