package com.woovoo.mazzi.call.util;

import android.util.Log;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.util.UrlConstants;

import org.json.JSONException;
import org.json.JSONObject;

public class AccessToken {

    private static final String LOG_TAG = "AccessToken";

    private static AccessToken sharedInstance;

    public static AccessToken getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new AccessToken();
        }

        return sharedInstance;
    }

    public void fetch(String roomId) {
        MazziNetwork.getANRequest(UrlConstants.getCallAccessTokenServer(), roomId)
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            ProfilePreferences.getInstance().putAccessToken(response.getString("token").trim());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(LOG_TAG, anError.getLocalizedMessage());
                    }
                });
    }

}