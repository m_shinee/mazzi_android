package com.woovoo.mazzi.call.ui;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.woovoo.mazzi.R;
import com.woovoo.mazzi.call.util.CallReceiver;
import com.woovoo.mazzi.call.util.CallNetworkUtils;
import com.woovoo.mazzi.call.util.AccessToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class DialingActivity extends Activity {

    private static final String LOG_TAG = "DialingActivity";
    private TextView callType;
    private Chronometer chronometer;
    private CallNetworkUtils callNetworkUtils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialing);
        EventBus.getDefault().register(this);

        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        ImageButton rejectButton = findViewById(R.id.rejectCall);
        TextView participantName = findViewById(R.id.participantName);
        chronometer = findViewById(R.id.dialedTime);
        callType = findViewById(R.id.callType);

        callNetworkUtils = CallNetworkUtils.getInstance();

        callNetworkUtils.setIsVideo(getIntent().getBooleanExtra("is_video", false));

        AccessToken.getInstance().fetch(callNetworkUtils.getConversationId());

        participantName.setText(callNetworkUtils.getCallerName());

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callNetworkUtils.sendDisconnected();
                finish();
            }
        });

        chronometer.stop();
        chronometer.setVisibility(View.GONE);
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long sec = ((SystemClock.elapsedRealtime() - chronometer.getBase()) / 1000) % 60;
                if (sec % 4 == 0 || sec == 1) {
                    callNetworkUtils.sendCallOrIncoming();
                }

                if (sec > 20) {
                    Toast.makeText(getApplicationContext(), getString(R.string.failed_call), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
        chronometer.start();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onDestroy() {
        chronometer.stop();
        chronometer.setVisibility(View.GONE);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void onEvent(CallReceiver callReceiver) {
        chronometer.stop();
        if (callNetworkUtils.isDialing()) {
            callType.setText(getString(R.string.calling));
            chronometer.setVisibility(View.VISIBLE);
            chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                @Override
                public void onChronometerTick(Chronometer chronometer) {
                    long sec = ((SystemClock.elapsedRealtime() - chronometer.getBase()) / 1000) % 60;
                    if (sec > 40) {
                        finish();
                    }
                }
            });
            chronometer.start();
        }

        if (callNetworkUtils.isIdle() || callNetworkUtils.isDisconnected()) {
            finish();
        }

        if (callNetworkUtils.isHolding()) {
            Intent intent;
            if (callNetworkUtils.isVideo()) {
                intent = new Intent(getApplicationContext(), VideoCallActivity.class);
            } else {
                intent = new Intent(getApplicationContext(), VoiceCallActivity.class);
            }
            startActivity(intent);
            finish();
        }
    }
}
