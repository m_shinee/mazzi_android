package com.woovoo.mazzi.call.ui;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;

import com.woovoo.mazzi.R;
import com.woovoo.mazzi.call.util.CallReceiver;
import com.woovoo.mazzi.call.util.CallNetworkUtils;
import com.woovoo.mazzi.call.util.AccessToken;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class IncomingActivity extends Activity implements View.OnClickListener {

    private static final String LOG_TAG = "IncomingActivity";

    private CallNetworkUtils networkUtils;
    private Chronometer chronometer;
    private MediaPlayer mp = new MediaPlayer();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incoming);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        EventBus.getDefault().register(this);
        networkUtils = CallNetworkUtils.getInstance();

        TextView textViewParticipantName = findViewById(R.id.participantName);
        textViewParticipantName.setText(networkUtils.getCallerName());

        ImageButton answerButton = findViewById(R.id.answerCall);
        ImageButton rejectButton = findViewById(R.id.rejectCall);

        mp = MediaPlayer.create(this, R.raw.ringtone);
        mp.start();

        chronometer = findViewById(R.id.dialedTime);
        chronometer.setVisibility(View.VISIBLE);
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long sec = ((SystemClock.elapsedRealtime() - chronometer.getBase()) / 1000) % 60;
                if (sec > 40) {
                    finish();
                }
            }
        });
        chronometer.start();

        AccessToken.getInstance().fetch(networkUtils.getConversationId());

        callRequestReceived();

        answerButton.setOnClickListener(this);
        rejectButton.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    protected void onDestroy() {
        chronometer.stop();
        chronometer.setVisibility(View.GONE);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
        mp.stop();
        mp.release();
    }

    @Subscribe
    public void onEvent(CallReceiver callReceiver) {
        if (networkUtils.isDisconnected() || networkUtils.isIdle()) {
            finish();
        }
    }

    private void callRequestReceived() {
        networkUtils.sendDialing();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.answerCall:
                answerButtonClicked();
                break;
            case R.id.rejectCall:
                rejectButtonClicked();
                break;
        }
    }

    private void answerButtonClicked() {
        networkUtils.sendHolding();
        Intent intent;
        if (networkUtils.isVideo()) {
            intent = new Intent(getApplicationContext(), VideoCallActivity.class);
        } else {
            intent = new Intent(getApplicationContext(), VoiceCallActivity.class);
        }
        startActivity(intent);
        finish();
    }

    private void rejectButtonClicked() {
        networkUtils.sendDisconnected();
        finish();
    }
}