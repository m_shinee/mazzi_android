package com.woovoo.mazzi.call.util;

import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.socket.AppSocketListener;
import com.woovoo.mazzi.socket.SocketEventConstants;

import org.json.JSONException;
import org.json.JSONObject;

public class CallNetworkUtils {

    private static final String LOG_TAG = "CallNetworkUtils";

    /**
     * Server @key params
     */
    public static final String conversationId = "conversation_id";
    public static final String callerName = "name";
    public static final String ownerId = "owner_id";
    public static final String participantId = "participant_id";
    public static final String callType = "calling";
    public static final String isVideo = "is_video";

    /**
     * State
     * int 1 = INCOMING / CALL
     * int 2 = DISCONNECTED
     * int 3 = IDLE
     * int 4 = HOLDING
     * int 5 = DIALING
     */
    public String mConversationId;
    public String mParticipantId;
    public String mCallerName;
    public int mCallType;
    public boolean mIsVideo;

    private static CallNetworkUtils instance;

    private CallNetworkUtils() {

    }

    public static CallNetworkUtils getInstance() {
        if (instance == null) {
            instance = new CallNetworkUtils();
        }

        return instance;
    }

    public void parseJSONObjectToSetter(JSONObject object) {
        try {
            setConversationId(object.getString(conversationId));
            setCallerName(object.getString(callerName));
            setParticipantId(object.getString(ownerId));
            setCallType(object.getInt(callType));
            setIsVideo(object.getBoolean(isVideo));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setConversationId(String conversationId) {
        this.mConversationId = conversationId;
    }

    public void setParticipantId(String participantId) {
        this.mParticipantId = participantId;
    }

    public void setCallType(int callType) {
        this.mCallType = callType;
    }

    public void setIsVideo(boolean isVideo) {
        this.mIsVideo = isVideo;
    }

    public void setCallerName(String callerName) {
        this.mCallerName = callerName;
    }

    public String getConversationId() {
        return mConversationId;
    }

    public String getParticipantId() {
        return mParticipantId;
    }

    public String getCallerName() {
        return mCallerName;
    }

    public int getCallType() {
        return mCallType;
    }

    public boolean isVideo() {
        return mIsVideo;
    }

    private void send() {
        JSONObject object = new JSONObject();
        try {
            object.put(conversationId, mConversationId);
            object.put(callType, mCallType);
            object.put(participantId, mParticipantId);
            object.put(isVideo, mIsVideo);

            object.put(ownerId, ProfilePreferences.getInstance().getId());
            object.put(callerName, ProfilePreferences.getInstance().getNickname());

            AppSocketListener.getInstance().emit(SocketEventConstants.call, object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void sendCallOrIncoming() {
        setCallType(1);
        send();
    }

    public void sendDisconnected() {
        setCallType(2);
        send();
    }

    public void sendIdle() {
        setCallType(3);
        send();
    }

    public void sendHolding() {
        setCallType(4);
        send();
    }

    public void sendDialing() {
        setCallType(5);
        send();
    }

    public boolean isIncoming() {
        return getCallType() == 1;
    }

    public boolean isDisconnected() {
        return getCallType() == 2;
    }

    public boolean isIdle() {
        return getCallType() == 3;
    }

    public boolean isHolding() {
        return getCallType() == 4;
    }

    public boolean isDialing() {
        return getCallType() == 5;
    }

}
