package com.woovoo.mazzi.call.ui;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.twilio.video.AudioTrack;
import com.twilio.video.ConnectOptions;
import com.twilio.video.LocalAudioTrack;
import com.twilio.video.Participant;
import com.twilio.video.Room;
import com.twilio.video.RoomState;
import com.twilio.video.TwilioException;
import com.twilio.video.Video;
import com.twilio.video.VideoTrack;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.call.util.CallNetworkUtils;
import com.woovoo.mazzi.call.util.CallReceiver;
import com.woovoo.mazzi.profile.util.ProfilePreferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Collections;

public class VoiceCallActivity extends AppCompatActivity {

    private static final int MIC_PERMISSION_REQUEST_CODE = 1;
    private static final String LOG_TAG = "VoiceCallActivity";

    /*
     * A Room represents communication between a local participant and one or more participants.
     */
    private Room room;

    /*
     * Android application UI elements
     */
    private TextView statusTextView;
    private LocalAudioTrack localAudioTrack;
    private FloatingActionButton callEndActionFab;
    private FloatingActionButton muteActionFab;
    private AudioManager audioManager;
    private Chronometer chronometer;
    private CallNetworkUtils callNetworkUtils;

    private int previousAudioMode;
    private boolean previousMicrophoneMute;
    private String roomName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_call);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD + WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED + WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        EventBus.getDefault().register(this);

        callNetworkUtils = CallNetworkUtils.getInstance();
        roomName = callNetworkUtils.getCallerName();

        statusTextView = findViewById(R.id.status_textview);
        chronometer = findViewById(R.id.dialedTime);

        muteActionFab = findViewById(R.id.mute_action_fab);
        callEndActionFab = findViewById(R.id.call_end);

        /*
         * Enable changing the volume using the up/down keys during a conversation
         */
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
//        setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);

        /*
         * Needed for setting/abandoning audio focus during call
         */
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            audioManager.setSpeakerphoneOn(true);
        }
        /*
         * Check microphone permissions. Needed in Android M.
         */
        if (!checkPermissionForMicrophone()) {
            requestPermissionForMicrophone();
        } else {
            createAudioTracks();
        }

        /*
         * Set the initial state of the UI
         */
        initializeUI();
        connectToRoom();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == MIC_PERMISSION_REQUEST_CODE) {
            boolean micPermissionGranted = true;

            for (int grantResult : grantResults) {
                micPermissionGranted &= grantResult == PackageManager.PERMISSION_GRANTED;
            }

            if (micPermissionGranted) {
                createAudioTracks();
            } else {
                Toast.makeText(getApplicationContext(),
                        R.string.permissions_needed,
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    @Subscribe
    public void onEvent(CallReceiver callReceiver) {
        if (callNetworkUtils.isDisconnected()) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        callNetworkUtils.sendDisconnected();
        EventBus.getDefault().unregister(this);
        chronometer.stop();
        /*
         * Always disconnect from the room before leaving the Activity to
         * ensure any memory allocated to the Room resource is freed.
         */
        if (room != null && room.getState() != RoomState.DISCONNECTED) {
            room.disconnect();
        }

        /*
         * Release the local audio and video tracks ensuring any memory allocated to audio
         * or video is freed.
         */
        if (localAudioTrack != null) {
            localAudioTrack.release();
            localAudioTrack = null;
        }

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

    }

    private boolean checkPermissionForMicrophone() {
        int resultMic = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
        return resultMic == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissionForMicrophone() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.RECORD_AUDIO)) {
            Toast.makeText(getApplicationContext(),
                    R.string.permissions_needed,
                    Toast.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    MIC_PERMISSION_REQUEST_CODE);
        }
    }

    private void createAudioTracks() {
        // Share your microphone
        localAudioTrack = LocalAudioTrack.create(this, true);
    }

    private void connectToRoom() {
        configureAudio(true);
        ConnectOptions.Builder connectOptionsBuilder = new ConnectOptions.Builder(ProfilePreferences.getInstance().getAccessToken())
                .roomName(roomName);

        /*
         * Add local audio track to connect options to share with participants.
         */
        if (localAudioTrack != null) {
            connectOptionsBuilder
                    .audioTracks(Collections.singletonList(localAudioTrack));
        }
        room = Video.connect(this, connectOptionsBuilder.build(), roomListener());
        setDisconnectAction();
    }

    /*
     * The initial state when there is no active room.
     */
    private void initializeUI() {
        muteActionFab.show();
        muteActionFab.setOnClickListener(muteClickListener());
    }

    /*
     * The actions performed during disconnect.
     */
    private void setDisconnectAction() {
        callEndActionFab.show();
        callEndActionFab.setOnClickListener(disconnectClickListener());
    }

    /*
     * Called when participant joins the room
     */
    private void addParticipant(Participant participant) {
        /*
         * Start listening for participant events
         */
        participant.setListener(participantListener());
    }

    /*
     * Room events listener
     */
    private Room.Listener roomListener() {
        return new Room.Listener() {
            @Override
            public void onConnected(Room room) {
                statusTextView.setText(getString(R.string.participant_connecting));
                chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                    @Override
                    public void onChronometerTick(Chronometer chronometer) {
                        long sec = ((SystemClock.elapsedRealtime() - chronometer.getBase()) / 1000) % 60;
                        if (sec > 5) {
                            statusTextView.setText(getString(R.string.connected));
                            chronometer.stop();
                        }
                    }
                });
                chronometer.start();
            }

            @Override
            public void onConnectFailure(Room room, TwilioException e) {
                Log.e(LOG_TAG, e.getMessage());
                statusTextView.setText(getString(R.string.failed_to_connect));
                configureAudio(false);
                initializeUI();
            }

            @Override
            public void onDisconnected(Room room, TwilioException e) {
                statusTextView.setText(getString(R.string.participant_disconnected, roomName));
                VoiceCallActivity.this.room = null;
                finish();
            }

            @Override
            public void onParticipantConnected(Room room, Participant participant) {
                addParticipant(participant);
            }

            @Override
            public void onParticipantDisconnected(Room room, Participant participant) {

            }

            @Override
            public void onRecordingStarted(Room room) {
                /*
                 * Indicates when media shared to a Room is being recorded. Note that
                 * recording is only available in our Group Rooms developer preview.
                 */
                Log.d(LOG_TAG, "onRecordingStarted");
            }

            @Override
            public void onRecordingStopped(Room room) {
                /*
                 * Indicates when media shared to a Room is no longer being recorded. Note that
                 * recording is only available in our Group Rooms developer preview.
                 */
                Log.d(LOG_TAG, "onRecordingStopped");
            }
        };
    }

    private Participant.Listener participantListener() {
        return new MyParticipantListener();
    }

    private View.OnClickListener disconnectClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                 * Disconnect from room
                 */
                if (room != null) {
                    room.disconnect();
                }
                finish();
            }
        };
    }

    private View.OnClickListener muteClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                 * Enable/disable the local audio track. The results of this operation are
                 * signaled to other ParticipantsPreference in the same Room. When an audio track is
                 * disabled, the audio is muted.
                 */
                if (localAudioTrack != null) {
                    boolean enable = !localAudioTrack.isEnabled();
                    localAudioTrack.enable(enable);
                    int icon = enable ?
                            R.mipmap.ic_mic_on_white : R.mipmap.ic_mic_off_white;
                    muteActionFab.setImageResource(icon);
                }
            }
        };
    }

    private void configureAudio(boolean enable) {
        if (enable) {
            previousAudioMode = audioManager.getMode();
            // Request audio focus before making any device switch
            requestAudioFocus();
            /*
             * Use MODE_IN_COMMUNICATION as the default audio mode. It is required
             * to be in this mode when playout and/or recording starts for the best
             * possible VoIP performance. Some devices have difficulties with
             * speaker mode if this is not set.
             */
            audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            /*
             * Always disable microphone mute during a WebRTC call.
             */
            previousMicrophoneMute = audioManager.isMicrophoneMute();
            audioManager.setMicrophoneMute(false);
        } else {
            audioManager.setMode(previousAudioMode);
            audioManager.abandonAudioFocus(null);
            audioManager.setMicrophoneMute(previousMicrophoneMute);
        }
    }

    private void requestAudioFocus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes playbackAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_VOICE_COMMUNICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();
            AudioFocusRequest focusRequest =
                    new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                            .setAudioAttributes(playbackAttributes)
                            .setAcceptsDelayedFocusGain(true)
                            .setOnAudioFocusChangeListener(
                                    new AudioManager.OnAudioFocusChangeListener() {
                                        @Override
                                        public void onAudioFocusChange(int i) {
                                        }
                                    })
                            .build();
            audioManager.requestAudioFocus(focusRequest);
        } else {
            audioManager.requestAudioFocus(null, AudioManager.STREAM_VOICE_CALL,
                    AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
        }
    }

    private static class MyParticipantListener implements Participant.Listener {
        @Override
        public void onAudioTrackAdded(Participant participant, AudioTrack audioTrack) {

        }

        @Override
        public void onAudioTrackRemoved(Participant participant, AudioTrack audioTrack) {

        }

        @Override
        public void onVideoTrackAdded(Participant participant, VideoTrack videoTrack) {

        }

        @Override
        public void onVideoTrackRemoved(Participant participant, VideoTrack videoTrack) {

        }

        @Override
        public void onAudioTrackEnabled(Participant participant, AudioTrack audioTrack) {

        }

        @Override
        public void onAudioTrackDisabled(Participant participant, AudioTrack audioTrack) {

        }

        @Override
        public void onVideoTrackEnabled(Participant participant, VideoTrack videoTrack) {

        }

        @Override
        public void onVideoTrackDisabled(Participant participant, VideoTrack videoTrack) {

        }
    }
}
