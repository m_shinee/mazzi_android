package com.woovoo.mazzi.file.util;

public class FileDownloading {

    private String messageId;
    private int position;
    private int downloadedPercent;

    public FileDownloading(String messageId, int position, int downloadedPercent) {
        this.messageId = messageId;
        this.position = position;
        this.downloadedPercent = downloadedPercent;
    }

    public String getMessageId() {
        return messageId;
    }

    public int getPosition() {
        return position;
    }

    public int getDownloadedPercent() {
        return downloadedPercent;
    }
}
