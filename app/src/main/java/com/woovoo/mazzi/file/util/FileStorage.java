package com.woovoo.mazzi.file.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class FileStorage {

    private static final String LOG_TAG = "FileStorage";
    private static FileStorage instance;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private FileStorage(Context context) {
        preferences = context.getSharedPreferences(LOG_TAG, Context.MODE_PRIVATE);
    }

    public static FileStorage getInstance(Context context) {
        if (instance == null)
            instance = new FileStorage(context);

        return instance;
    }

    public static FileStorage getInstance() {
        if (instance != null)
            return instance;

        throw new IllegalArgumentException("Should use getInstance(Context) at least once before using this method.");
    }

    @SuppressLint("CommitPrefEdits")
    private void doEdit() {
        editor = preferences.edit();
    }

    private void doApply() {
        if (editor != null) {
            editor.apply();
            editor = null;
        }
    }

    public void clearAll() {
        doEdit();
        editor.clear();
        doApply();
    }

    public boolean exist(String msgId) {
        return preferences.contains(msgId);
    }

    public void putFile(String msgId, String filename) {
        doEdit();
        editor.putString(msgId, filename);
        doApply();
    }

    public String getFilename(String msgId) {
        return preferences.getString(msgId, null);
    }

}
