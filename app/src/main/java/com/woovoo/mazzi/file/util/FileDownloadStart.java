package com.woovoo.mazzi.file.util;

import com.woovoo.mazzi.directory.MazziDirectory;

public class FileDownloadStart {

    private String messageId,
            url,
            fileType,
            filename;

    private int adapterPosition;

    public FileDownloadStart(String messageId, String url, String fileType, String filename, int adapterPosition) {
        this.messageId = messageId;
        this.url = url;
        this.fileType = fileType;
        this.filename = filename;
        this.adapterPosition = adapterPosition;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getUrl() {
        return url;
    }

    public String getFileType() {
        return fileType;
    }

    public String getFilename() {
        return filename;
    }

    public int getAdapterPosition() {
        return adapterPosition;
    }

    public String getFileLocalPath() {
        return MazziDirectory.getDirectoryByMessageType(getFileType()) + "/" + getFilename();
    }
}
