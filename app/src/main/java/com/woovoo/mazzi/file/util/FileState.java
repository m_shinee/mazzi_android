package com.woovoo.mazzi.file.util;

import java.util.HashMap;

public class FileState {

    private static FileState instance;
    private static HashMap<String, State> hashMap;

    public static FileState getInstance() {
        if (instance == null) {
            hashMap = new HashMap<>();
            instance = new FileState();
        }
        return instance;
    }

    private enum State {
        IDLE, DOWNLOADING
    }

    public void put(String msgId, State state) {
        hashMap.put(msgId, state);
    }

    public boolean isIdle(String msgId) {
        return !hashMap.containsKey(msgId) || hashMap.get(msgId) == State.IDLE;
    }

    public boolean isDownloading(String msgId) {
        return hashMap.containsKey(msgId) && hashMap.get(msgId) == State.DOWNLOADING;
    }

    public State setIdle() {
        return State.IDLE;
    }

    public State setDownloading() {
        return State.DOWNLOADING;
    }

}
