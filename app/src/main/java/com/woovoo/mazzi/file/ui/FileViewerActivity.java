package com.woovoo.mazzi.file.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.WebView;

import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.UrlConstants;

public class FileViewerActivity extends Activity {

    private static final String LOG_TAG = "FileViewerActivity";

    private WebView webView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_viewer);

        String filepath = getIntent().getStringExtra("filepath");

        Log.d(LOG_TAG, filepath);

        webView = findViewById(R.id.webView);
        webView.loadUrl(UrlConstants.getFileUrl(filepath));
    }

}