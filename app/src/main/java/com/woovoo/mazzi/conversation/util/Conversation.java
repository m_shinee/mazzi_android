package com.woovoo.mazzi.conversation.util;

import android.util.Log;

import com.woovoo.mazzi.util.FindOrRemove;
import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.msg.util.MsgPreferences;
import com.woovoo.mazzi.contact.util.ContactStorage;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.msg.util.Msg;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;
import java.util.Objects;

public class Conversation {

    private static final String LOG_TAG = "Conversation";

    public final String ID = "_id";
    public final String PARTICIPANTS = "participants";
    public final String THREAD = "is_thread";
    public final String CREATED_AT = "created_at";
    public final String TITLE = "title";
    public final String SECRET = "secret";
    public final String SHOW = "show";
    public final String IMAGE = "image";
    public final String PARTICIPANTS_INFO = "participantsinfo";

    /**
     * msg object
     */
    public final String MSG = "msg";
    public final String MSG_ID = "_id";
    public final String MSG_CONTENT = "content";
    public final String MSG_SEEN = "seen";
    public final String MSG_CREATED_AT = "created_at";
    public final String MSG_OWNER_ID = "owner_id";
    public final String MSG_TYPE = "type";
    public final String MSG_CONVERSATION_ID = "conversation_id";

    private String conversationId = "";
    private String title = "";
    private String image = "";
    private String content = "Chat is created";
    private String ownerId = "";
    private String messageId = "";
    private String messageType = "";
    private Long createdAt = MyDateFormat.currentTime();
    private boolean seen = false;
    private boolean thread = false;

    private JSONArray participants = new JSONArray();
    private JSONArray show = new JSONArray();

    /**
     * @param msg parse to Conversation
     */
    public Conversation(Msg msg) {
        this.conversationId = msg.getConversationId();
        this.messageId = msg.getMessageId();
        this.ownerId = msg.getOwnerId();
        this.messageType = msg.getMessageType();
        this.content = msg.getContent();
        this.seen = true;
        this.createdAt = msg.getCreatedAt();
        this.participants = MsgPreferences.getInstance().getParticipants();
    }

    /**
     * @param object Parse JSONObject
     */
    public Conversation(JSONObject object) {
        try {
            setConversationId(object.getString(ID));
            setParticipants(object.getJSONArray(PARTICIPANTS));
            setTitle(object.getString(TITLE));
            setImage(object.getString(IMAGE));
            setThread(object.getBoolean(THREAD));
            setCreatedAt(object.getLong(CREATED_AT));

            if (object.has(MSG)) {
                JSONObject msg = object.getJSONObject(MSG);
                setMessageId(msg.getString(MSG_ID));
                setOwnerId(msg.getString(MSG_OWNER_ID));
                setMessageType(msg.getString(MSG_TYPE));
                setContent(msg.getString(MSG_CONTENT));
                setCreatedAt(msg.getLong(MSG_CREATED_AT));
                setSeen(FindOrRemove.fromJSONArray(msg.getJSONArray(MSG_SEEN), ProfilePreferences.getInstance().getId()));
            }

            if (!isThread()) {
                String participantId = getFirstParticipant();
                if (participantId.equals(ProfilePreferences.getInstance().getId())) {
                    participantId = getSecondParticipant();
                }
                setTitle(ContactStorage.getInstance().getNickname(participantId));
                setImage(ContactStorage.getInstance().getImage(participantId));
            }

            setShow(object.getJSONArray(SHOW));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Conversation() {

    }

    /**
     * @param string Parse JSONString
     */
    public Conversation(String string) {
        try {
            JSONObject jsonObject = new JSONObject(string);
            this.conversationId = jsonObject.getString(MSG_CONVERSATION_ID);
            this.ownerId = jsonObject.getString(MSG_OWNER_ID);
            this.messageId = jsonObject.getString(MSG_ID);
            this.messageType = jsonObject.getString(MSG_TYPE);
            this.seen = MsgPreferences.getInstance().getConversationId().equals(getConversationId()) || FindOrRemove.fromJSONArray(jsonObject.getJSONArray(MSG_SEEN), ProfilePreferences.getInstance().getId());
            this.createdAt = jsonObject.getLong(CREATED_AT);
            this.content = jsonObject.getString(MSG_CONTENT);
            this.show = jsonObject.getJSONArray(SHOW);

            if (!isThread()) {
                this.title = ContactStorage.getInstance().getNickname(getOwnerId());
                this.image = ContactStorage.getInstance().getImage(getOwnerId());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public void setContent(String content) {
        generateContent();
        switch (getMessageType()) {
            case "text":
                this.content += content.length() > 10 ? content.substring(0, 10) + "..." : content;
                break;
            case "photo":
                this.content += "photo sent";
                break;
            case "video":
                this.content += "video sent";
                break;
            case "audio":
                this.content += "voice message sent";
                break;
            case "sticker":
                this.content += "sticker sent";
                break;
            case "document":
                this.content += "file sent";
                break;
            default:
                this.content += content;
                break;
        }
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public void setThread(boolean thread) {
        this.thread = thread;
    }

    public void setParticipants(JSONArray participants) {
        this.participants = participants;
    }

    public void setShow(JSONArray show) {
        this.show = show;
    }

    public String getConversationId() {
        return conversationId;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public String getMessageType() {
        return messageType;
    }

    public String getContent() {
        return content;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public String getMessageId() {
        return messageId;
    }

    public boolean isSeen() {
        return seen;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public boolean isThread() {
        return thread;
    }

    public JSONArray getParticipants() {
        return participants;
    }

    public JSONArray getShow() {
        return this.show;
    }

    public boolean isActive() {
        String participantId;
        for (int i = 0; i < getShow().length(); i++) {
            try {
                participantId = getShow().getString(i);
                if (!Objects.equals(participantId, ProfilePreferences.getInstance().getId())) {
                    return true;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static final Comparator<Conversation> BY_CREATED_AT = new Comparator<Conversation>() {
        @Override
        public int compare(Conversation conversation, Conversation t1) {
            return t1.createdAt.compareTo(conversation.createdAt);
        }
    };

    public int getViewType() {
        return isThread() ? 1 : 0;
    }

    @Override
    public boolean equals(Object obj) {
        boolean sameConversationId = false;
        if (obj != null && obj instanceof Conversation) {
            sameConversationId = Objects.equals(this.conversationId, ((Conversation) obj).conversationId);
        }
        return sameConversationId;
    }

    @Override
    public int hashCode() {
        return this.conversationId == null ? -1 : this.conversationId.hashCode();
    }

    /**
     * @return String first participant
     * @throws JSONException Error
     */
    private String getFirstParticipant() throws JSONException {
        return getParticipants().get(0).toString();
    }

    /**
     * @return String second participant
     * @throws JSONException Error
     */
    private String getSecondParticipant() throws JSONException {
        return getParticipants().get(1).toString();
    }

    /**
     * Who sent "content"
     *
     * @throws JSONException Error
     */
    private void generateContent() {
        if (getOwnerId().equals(ProfilePreferences.getInstance().getId())) {
            this.content = "Me: ";
        } else if (isThread()) {
            this.content = ContactStorage.getInstance().getNickname(getOwnerId()) + ": ";
        } else {
            this.content = "";
        }
    }

}
