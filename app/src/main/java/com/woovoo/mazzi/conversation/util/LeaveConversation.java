package com.woovoo.mazzi.conversation.util;

public class LeaveConversation {

    private String conversationId;

    public String getConversationId() {
        return conversationId;
    }

    LeaveConversation(Builder builder) {
        this.conversationId = builder.conversationId;
    }

    public static class Builder {

        private String conversationId;

        public Builder setConversationId(String conversationId) {
            this.conversationId = conversationId;
            return this;
        }

        public LeaveConversation build() {
            return new LeaveConversation(this);
        }
    }
}
