package com.woovoo.mazzi.conversation;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.woovoo.mazzi.msg.ui.MsgActivity;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.conversation.util.Conversation;
import com.woovoo.mazzi.util.UrlConstants;
import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.contact.util.ContactStorage;
import com.woovoo.mazzi.msg.util.MsgPreferences;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.MyViewHolder> {

    private static final String LOG_TAG = "ConversationAdapter";

    private ArrayList<Conversation> conversations;
    private RequestOptions requestOptions = new RequestOptions()
            .transforms(new CenterCrop(), new RoundedCorners(40))
            .override(100, 100);

    private RequestOptions requestOptionsCircle = new RequestOptions()
            .circleCrop()
            .override(100);

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView titleTextView, createdAtTextView, contentTextView;
        ImageView thumbnailImageView;
        LinearLayout membersPicture;
        ImageView dotImageView;

        MyViewHolder(final View itemView, int viewType) {
            super(itemView);

            titleTextView = itemView.findViewById(R.id.titleTextView);
            contentTextView = itemView.findViewById(R.id.contentTextView);
            createdAtTextView = itemView.findViewById(R.id.createdAtTextView);
            thumbnailImageView = itemView.findViewById(R.id.thumbnailImageView);
            dotImageView = itemView.findViewById(R.id.dotImageView);

            if (viewType == 1) {
                membersPicture = itemView.findViewById(R.id.membersPicture);
            }
        }
    }

    @Override
    public int getItemCount() {
        return conversations.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        if (viewType == 1) {
            layout = R.layout.conversations_group;
        } else {
            layout = R.layout.conversations;
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new MyViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Conversation conversation = conversations.get(position);

        holder.titleTextView.setText(conversation.getTitle());
        holder.contentTextView.setText(conversation.getContent());
        holder.contentTextView.setTag(conversation.isThread());

        if (conversation.getImage().trim().isEmpty()) {
            Glide.with(holder.itemView.getContext())
                    .load(R.mipmap.ic_contact_white)
                    .apply(requestOptions)
                    .into(holder.thumbnailImageView);
            holder.thumbnailImageView.setBackgroundResource(R.drawable.contact_profile_picture_radius);
            holder.thumbnailImageView.setPadding(15, 15, 15, 15);
        } else {
            Glide.with(holder.itemView.getContext())
                    .load(UrlConstants.getFileUrl(conversation.getImage()))
                    .apply(requestOptions)
                    .into(holder.thumbnailImageView);
            holder.thumbnailImageView.setBackground(null);
            holder.thumbnailImageView.setPadding(0, 0, 0, 0);
        }


        if (conversation.isThread()) {
            holder.membersPicture.removeAllViews();
            LinearLayout.LayoutParams params;
            ImageView imageView;

            JSONArray show = conversation.getShow();
            int n = show.length();
            for (int i = 0; i < n; i++) {
                String participantId = null;
                try {
                    participantId = show.getString(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                imageView = new ImageView(holder.itemView.getContext());
                imageView.setLayoutParams(params);
                imageView.setAdjustViewBounds(true);
                String senderPicture = ContactStorage.getInstance().getImage(participantId);
                if (senderPicture.isEmpty()) {
                    Glide.with(holder.itemView.getContext())
                            .load(R.mipmap.ic_contact_white)
                            .apply(requestOptionsCircle)
                            .into(imageView);
                    imageView.setBackgroundResource(R.drawable.oval_background_gray);
                    imageView.setPadding(5, 5, 5, 5);
                } else {
                    Glide.with(holder.itemView.getContext())
                            .load(UrlConstants.getFileUrl(senderPicture))
                            .apply(requestOptionsCircle)
                            .into(imageView);
                    imageView.setBackground(null);
                    imageView.setPadding(0, 0, 0, 0);
                }
                holder.membersPicture.addView(imageView);
                if (i == 2) {
                    if (n - i > 1) {
                        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
                        TextView textView = new TextView(holder.itemView.getContext());
                        textView.setLayoutParams(params);
                        textView.setBackgroundResource(R.drawable.conversation_users_counter);
                        textView.setTextColor(Color.WHITE);
                        textView.setText(holder.itemView.getResources().getString(R.string.region, (n - i - 1)));
                        textView.setGravity(Gravity.CENTER_VERTICAL);
                        holder.membersPicture.addView(textView);
                    }
                    break;
                }
            }
        } else {

        }

        if (conversation.isActive()) {
            holder.dotImageView.setBackgroundResource(R.drawable.oval_background_green);
        } else {
            holder.dotImageView.setBackground(null);
        }

        holder.createdAtTextView.setText(MyDateFormat.generate(conversation.getCreatedAt(), "hh:mm"));

        if (!conversation.isSeen()) {
            holder.itemView.setBackgroundResource(R.color.colorWhite);
            holder.titleTextView.setTypeface(null, Typeface.BOLD);
            holder.contentTextView.setTextColor(Color.BLACK);
            holder.contentTextView.setTypeface(null, Typeface.BOLD);
        } else {
            holder.itemView.setBackgroundResource(R.color.colorGray);
            holder.titleTextView.setTypeface(null, Typeface.NORMAL);
            holder.contentTextView.setTextColor(Color.parseColor("#808080"));
            holder.contentTextView.setTypeface(null, Typeface.NORMAL);
        }

        holder.itemView.setOnClickListener(new MyViewHolderOnClickListener(conversation, holder));
    }

    public ConversationAdapter(ArrayList<Conversation> conversations) {
        this.conversations = conversations;
    }

    @Override
    public int getItemViewType(int position) {
        return conversations.get(position).getViewType();
    }

    private static class MyViewHolderOnClickListener implements View.OnClickListener {
        private final Conversation conversation;
        private final MyViewHolder holder;

        MyViewHolderOnClickListener(Conversation conversation, MyViewHolder holder) {
            this.conversation = conversation;
            this.holder = holder;
        }

        @Override
        public void onClick(View v) {
            MsgPreferences.getInstance().setConversationId(conversation.getConversationId());
            MsgPreferences.getInstance().setId(conversation.getMessageId());
            MsgPreferences.getInstance().setSeen(conversation.isSeen());
            MsgPreferences.getInstance().setParticipants(conversation.getParticipants());

            v.setBackgroundColor(Color.TRANSPARENT);

            holder.titleTextView.setTypeface(null, Typeface.NORMAL);
            holder.contentTextView.setTextColor(Color.parseColor("#808080"));
            holder.contentTextView.setTypeface(null, Typeface.NORMAL);

            Intent intent = new Intent(v.getContext(), MsgActivity.class);
            intent.putExtra("title", conversation.getTitle());

            conversation.setSeen(true);
            v.getContext().startActivity(intent);
        }
    }
}