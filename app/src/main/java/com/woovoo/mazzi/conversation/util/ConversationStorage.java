package com.woovoo.mazzi.conversation.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class ConversationStorage extends Conversation {

    private static final String LOG_TAG = "ConversationStorage";
    private static ConversationStorage instance;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private ConversationStorage(Context context) {
        preferences = context.getSharedPreferences(LOG_TAG, Context.MODE_PRIVATE);
    }

    public static ConversationStorage getInstance(Context context) {
        if (instance == null) {
            instance = new ConversationStorage(context);
        }

        return instance;
    }

    public static ConversationStorage getInstance() {
        if (instance != null) {
            return instance;
        }

        throw new IllegalArgumentException("Should use getInstance(Context) at least once before using this method.");
    }

    public void clearAll() {
        doEdit();
        editor.clear();
        doApply();
    }

    @SuppressLint("CommitPrefEdits")
    private void doEdit() {
        editor = preferences.edit();
    }

    private void doApply() {
        if (editor != null) {
            editor.apply();
            editor = null;
        }
    }

    public void putConversations(JSONObject object) {
        try {
            object.remove(PARTICIPANTS_INFO);
            doEdit();
            editor.putString(object.getString(ID), object.toString());
            doApply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getTitle() {
        return preferences.getString("5a615ed733cb9b06bbc4a95b", "");
    }

    public Map<String, ?> getAll() {
        return preferences.getAll();
    }

    private boolean exist(String conversationId) {
        return preferences.contains(conversationId);
    }
}
