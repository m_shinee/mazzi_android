package com.woovoo.mazzi.conversation.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.conversation.util.LeaveConversation;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.conversation.ConversationAdapter;
import com.woovoo.mazzi.conversation.util.Conversation;
import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.util.UrlConstants;
import com.woovoo.mazzi.contact.util.RecentlyRegistered;
import com.woovoo.mazzi.contact.util.ContactStorage;
import com.woovoo.mazzi.conversation.util.ConversationStorage;
import com.woovoo.mazzi.profile.util.Profile;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.socket.util.OnlineStatus;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class ConversationFragment extends Fragment {

    private static final String LOG_TAG = "ConversationFragment";
    private ArrayList<Conversation> conversationList = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ConversationAdapter conversationAdapter;
    private RecyclerView recyclerView;
    private int limit = 40, skip = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        conversationAdapter = new ConversationAdapter(conversationList);
        layoutManager = new LinearLayoutManager(getContext());
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chats, container, false);
        recyclerView = view.findViewById(R.id.chat_list);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(conversationAdapter);
        fetchConversation();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });
        return view;
    }

    @Subscribe
    public void onEvent(Conversation conversation) {
        itemChanged(conversation);
        layoutManager.scrollToPosition(0);
    }

    @Subscribe
    public void onEvent(RecentlyRegistered recentlyRegistered) {
        Conversation conversation = new Conversation();
        conversation.setConversationId(recentlyRegistered.getConversationId());
        conversation.setParticipants(recentlyRegistered.getParticipants());
        conversation.setCreatedAt(MyDateFormat.currentTime());
        conversation.setShow(recentlyRegistered.getShow());
        conversation.setContent(getString(R.string.registered_to_mazzi));
        conversation.setTitle(recentlyRegistered.getNickname());
        addConversation(conversation);
        Collections.sort(conversationList, Conversation.BY_CREATED_AT);
        conversationAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void onEvent(OnlineStatus status) {
        refreshItems();
    }

    @Subscribe
    public void onEvent(LeaveConversation leaveConversation) {
        int i = 0;
        for (Conversation conversation : conversationList) {
            if (conversation.getConversationId().equals(leaveConversation.getConversationId())) {
                conversationList.remove(i);
                break;
            }
            i++;
        }
        refreshItems();
    }

    public void itemChanged(Conversation event) {
        int index = exists(event.getConversationId());
        if (index == -1) {
            getConversation(event.getConversationId());
        } else {
            Conversation conversation = conversationList.get(index);
            conversation.setMessageId(event.getMessageId());
            conversation.setSeen(event.isSeen());
            conversation.setMessageType(event.getMessageType());
            conversation.setOwnerId(event.getOwnerId());
            conversation.setCreatedAt(event.getCreatedAt());
            conversation.setContent(event.getContent());
            Collections.sort(conversationList, Conversation.BY_CREATED_AT);
            conversationAdapter.notifyDataSetChanged();
        }
    }

    private void fetchConversation() {
        JSONObject object = new JSONObject();
        try {
            object.put("user_id", ProfilePreferences.getInstance().getId());
            object.put("limit", limit);
            object.put("skip", skip);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MazziNetwork.postANRequest(UrlConstants.getConversations(), object)
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray conversations) {
                        ConversationStorage.getInstance().clearAll();
                        JSONArray participantsInfo;
                        JSONObject participant = new JSONObject(), mConversationObject;
                        try {
                            participant.put(Profile.ID, ProfilePreferences.getInstance().getId());
                            participant.put(Profile.NICKNAME, ProfilePreferences.getInstance().getNickname());
                            participant.put(Profile.LASTNAME, ProfilePreferences.getInstance().getLastname());
                            participant.put(Profile.SURNAME, ProfilePreferences.getInstance().getSurname());
                            participant.put(Profile.IMAGE, ProfilePreferences.getInstance().getImage());
                            participant.put(Profile.PHONE, ProfilePreferences.getInstance().getPhone());
                            participant.put(Profile.ONLINE_AT, ProfilePreferences.getInstance().getOnlineAt());
                            participant.put(Profile.STATE, 1);
                            ContactStorage.getInstance().putParticipant(participant);

                            for (int i = 0; i < conversations.length(); i++) {
                                mConversationObject = conversations.getJSONObject(i);
                                participantsInfo = mConversationObject.getJSONArray("participantsinfo");
                                for (int j = 0; j < participantsInfo.length(); j++) {
                                    participant = participantsInfo.getJSONObject(j);
                                    ContactStorage.getInstance().putParticipant(participant);
                                }
                                addConversation(new Conversation(mConversationObject));
                                ConversationStorage.getInstance().putConversations(mConversationObject);
                            }
                            Collections.sort(conversationList, Conversation.BY_CREATED_AT);
                            conversationAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Map<String, ?> conversations = ConversationStorage.getInstance().getAll();
                        int i = 0;
                        for (Map.Entry<String, ?> entry : conversations.entrySet()) {
                            if (i > 39)
                                return;
                            try {
                                addConversation(new Conversation(new JSONObject(entry.getValue().toString())));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Collections.sort(conversationList, Conversation.BY_CREATED_AT);
                            conversationAdapter.notifyDataSetChanged();
                            i++;
                        }
                    }
                });
    }

    private void getConversation(String conversationId) {
        JSONObject params = new JSONObject();
        try {
            params.put("_id", conversationId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MazziNetwork.postANRequest(UrlConstants.getConversation(), params)
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray participantsInfo;
                        JSONObject participant;
                        try {
                            participantsInfo = response.getJSONArray("participantsinfo");
                            for (int j = 0; j < participantsInfo.length(); j++) {
                                participant = participantsInfo.getJSONObject(j);
                                ContactStorage.getInstance().putParticipant(participant);
                            }
                            addConversation(new Conversation(response));
                            Collections.sort(conversationList, Conversation.BY_CREATED_AT);
                            conversationAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(LOG_TAG, anError.getErrorDetail());
                    }
                });
    }

    /**
     * @param conversationId conversation
     * @return -1 If conversation is not created
     */
    public int exists(String conversationId) {
        Conversation conversation = new Conversation();
        conversation.setConversationId(conversationId);
        return conversationList.indexOf(conversation);
    }

    public void addConversation(Conversation conversation) {
        conversationList.add(conversation);
    }

    private void refreshItems() {
        conversationAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }
}
