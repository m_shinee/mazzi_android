package com.woovoo.mazzi.camera.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;

import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitEventCallback;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;
import com.woovoo.mazzi.PreviewImageActivity;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.PreviewVideoActivity;
import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.directory.MazziDirectory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class CameraKitActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "CameraKitActivity";
    private static final int PREVIEW_IMAGE = 0;
    private static final int PREVIEW_VIDEO = 1;
    private CameraView cameraView;
    private boolean CAMERA_FRONT_FACING = false;
    private boolean VIDEO_RECORDING = false;
    private String FILE_LOCATION = "", FILE_NAME = "", THUMB_LOCATION = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camerakit);
        cameraView = findViewById(R.id.camera);

        ImageButton mCameraCloseButton = findViewById(R.id.close_camera);
        ImageButton mCameraSwitchButton = findViewById(R.id.switch_camera);
        ImageButton mCameraCaptureButton = findViewById(R.id.button_capture);
        ImageButton mVideoButton = findViewById(R.id.open_video);

        mCameraCaptureButton.setOnClickListener(this);
        mCameraSwitchButton.setOnClickListener(this);
        mCameraCloseButton.setOnClickListener(this);
        mVideoButton.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PREVIEW_IMAGE) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent();
                intent.putExtra("filepath", FILE_LOCATION);
                intent.putExtra("filename", FILE_NAME);
                intent.putExtra("type", "photo");
                setResult(RESULT_OK, intent);
                finish();
            }
        } else if (requestCode == PREVIEW_VIDEO) {
            if (resultCode == RESULT_OK) {
                createVideoThumb();
                Intent intent = new Intent();
                intent.putExtra("filepath", FILE_LOCATION);
                intent.putExtra("filename", FILE_NAME);
                intent.putExtra("type", "video");
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraView.start();
    }

    @Override
    protected void onPause() {
        cameraView.stop();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        cameraView.stop();
        super.onDestroy();
    }

    private File createVideoFile() {
        FILE_NAME = MyDateFormat.currentTime() + ".mp4";
        File file = new File(MazziDirectory.getVideoFilepath(FILE_NAME));
        FILE_LOCATION = file.getAbsolutePath();
        return file;
    }

    private void createVideoThumb() {
        File file = new File(MazziDirectory.getImageFilepath(FILE_NAME));
        THUMB_LOCATION = file.getAbsolutePath();

        Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(FILE_LOCATION, MediaStore.Video.Thumbnails.FULL_SCREEN_KIND);
        FileOutputStream out;
        try {
            out = new FileOutputStream(THUMB_LOCATION);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void createImageFile(Bitmap bitmap) {
        FILE_NAME = MyDateFormat.currentTime() + ".jpg";
        File file = new File(MazziDirectory.getImageFilepath(FILE_NAME));
        FILE_LOCATION = file.getAbsolutePath();
        FileOutputStream out;
        try {
            out = new FileOutputStream(FILE_LOCATION);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void imageCaptured(CameraKitImage cameraKitImage) {
        createImageFile(cameraKitImage.getBitmap());
        Intent previewImageActivity = new Intent(getApplicationContext(), PreviewImageActivity.class);
        previewImageActivity.putExtra("filepath", FILE_LOCATION);
        startActivityForResult(previewImageActivity, PREVIEW_IMAGE);
    }

    public void videoCaptured(CameraKitVideo cameraKitVideo) {
        Intent previewVideoActivity = new Intent(getApplicationContext(), PreviewVideoActivity.class);
        previewVideoActivity.putExtra("filepath", FILE_LOCATION);
        startActivityForResult(previewVideoActivity, PREVIEW_VIDEO);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.switch_camera:
                mCameraSwitchButtonClicked();
                break;
            case R.id.button_capture:
                mCameraCaptureButtonClicked();
                break;
            case R.id.close_camera:
                mCameraCloseButtonClicked();
                break;
            case R.id.open_video:
                mVideoButtonClicked();
                break;
        }
    }

    private void mCameraSwitchButtonClicked() {
        if (!CAMERA_FRONT_FACING) {
            cameraView.setFacing(CameraKit.Constants.FACING_FRONT);
            CAMERA_FRONT_FACING = true;
        } else {
            cameraView.setFacing(CameraKit.Constants.FACING_BACK);
            CAMERA_FRONT_FACING = false;
        }
    }

    private void mCameraCaptureButtonClicked() {
        cameraView.captureImage(new CameraKitEventCallback<CameraKitImage>() {
            @Override
            public void callback(CameraKitImage cameraKitImage) {
                imageCaptured(cameraKitImage);
            }
        });
    }

    private void mCameraCloseButtonClicked() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void mVideoButtonClicked() {
        if (!VIDEO_RECORDING) {
            VIDEO_RECORDING = true;
            cameraView.captureVideo(createVideoFile(), new CameraKitEventCallback<CameraKitVideo>() {
                @Override
                public void callback(CameraKitVideo cameraKitVideo) {
                    videoCaptured(cameraKitVideo);
                }
            });
        } else {
            VIDEO_RECORDING = false;
            cameraView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    cameraView.stopVideo();
                }
            }, 500);
        }
    }
}