package com.woovoo.mazzi.camera.ui;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageButton;

import com.wonderkiln.camerakit.CameraKit;
import com.wonderkiln.camerakit.CameraKitEventCallback;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraView;
import com.woovoo.mazzi.PreviewImageActivity;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.directory.MazziDirectory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class PhotoKitActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "PhotoKitActivity";
    private static final int PREVIEW_IMAGE = 0, SELECT_FILE_FROM_GALLERY = 1;
    private boolean CAMERA_FRONT_FACING = false;
    private CameraView cameraView;
    private String FILE_LOCATION = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camerakit_photo);
        cameraView = findViewById(R.id.camera);
        ImageButton mCameraCloseButton = findViewById(R.id.close_camera);
        ImageButton mCameraSwitchButton = findViewById(R.id.switch_camera);
        ImageButton mCameraCaptureButton = findViewById(R.id.button_capture);

        mCameraCaptureButton.setOnClickListener(this);
        mCameraSwitchButton.setOnClickListener(this);
        mCameraCloseButton.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_FILE_FROM_GALLERY) {
                Uri selectedMediaUri = data.getData();
                FILE_LOCATION = getRealPathFromUri(selectedMediaUri);
            }

            if (requestCode == SELECT_FILE_FROM_GALLERY || requestCode == PREVIEW_IMAGE) {
                Intent intent = new Intent();
                intent.putExtra("filepath", FILE_LOCATION);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        cameraView.start();
        super.onResume();
    }

    @Override
    protected void onPause() {
        cameraView.stop();
        super.onPause();
    }

    private void createImageFile(Bitmap bitmap) {
        String filename = MyDateFormat.currentTime() + ".jpg";
        File file = new File(MazziDirectory.getImageFilepath(filename));
        FILE_LOCATION = file.getAbsolutePath();
        FileOutputStream out;
        try {
            out = new FileOutputStream(FILE_LOCATION);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getRealPathFromUri(Uri mediaUri) {
        Cursor cursor = null;
        try {
            String[] project = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(mediaUri, project, null, null, null);
            if (cursor != null) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                String name = cursor.getString(columnIndex);
                cursor.close();
                return name;
            }
            return null;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void imageCaptured(CameraKitImage cameraKitImage) {
        createImageFile(cameraKitImage.getBitmap());
        Intent previewImageActivity = new Intent(getApplicationContext(), PreviewImageActivity.class);
        previewImageActivity.putExtra("filepath", FILE_LOCATION);
        startActivityForResult(previewImageActivity, PREVIEW_IMAGE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_capture:
                mCameraCaptureButtonClicked();
                break;
            case R.id.switch_camera:
                mCameraSwitchButtonClicked();
                break;
            case R.id.close_camera:
                mCameraCloseButtonClicked();
                break;
        }
    }

    private void mCameraCaptureButtonClicked() {
        cameraView.captureImage(new CameraKitEventCallback<CameraKitImage>() {
            @Override
            public void callback(CameraKitImage cameraKitImage) {
                imageCaptured(cameraKitImage);
            }
        });
    }

    private void mCameraSwitchButtonClicked() {
        if (!CAMERA_FRONT_FACING) {
            cameraView.setFacing(CameraKit.Constants.FACING_FRONT);
            CAMERA_FRONT_FACING = true;
        } else {
            cameraView.setFacing(CameraKit.Constants.FACING_BACK);
            CAMERA_FRONT_FACING = false;
        }
    }

    private void mCameraCloseButtonClicked() {
        setResult(RESULT_CANCELED);
        finish();
    }
}
