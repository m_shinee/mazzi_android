package com.woovoo.mazzi.msg;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.Player;
import com.woovoo.mazzi.file.util.FileStorage;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.util.UrlConstants;
import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.msg.util.Msg;

import java.util.List;

public class MsgAdapter extends RecyclerView.Adapter<MsgAdapter.ViewHolder> {

    private List<Msg> mMessage;
    private RecyclerClickListener listener;
    private RecyclerItemLongClickListener longClickListener;
    private int mMediaPlayingPosition = -1;

    private RequestOptions imageBorder = new RequestOptions()
            .transforms(new FitCenter(), new RoundedCorners(30))
            .override(500);

    private RequestOptions profilePicBorder = new RequestOptions()
            .transforms(new CenterCrop(), new RoundedCorners(40))
            .override(100, 100);

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView createdAtTextView,
                contentTextView,
                playerDurationTextView,
                documentNameTextView,
                documentSizeTextView,
                downloadedPercentTextView;

        private ImageView senderImageView,
                thumbnailImageView,
                videoThumbnail,
                audioImageViewFrom,
                audioImageViewTo,
                documentImageViewFrom,
                documentImageViewTo;

        private ImageView stickerImageView;

        private RelativeLayout documentView;

        private ViewHolder(final View itemView, int viewType) {
            super(itemView);

            createdAtTextView = itemView.findViewById(R.id.createdAtTextView);
            if (viewType % 2 != 0 || viewType != 0) {
                senderImageView = itemView.findViewById(R.id.senderImageView);
            }

            if (viewType < 2) {
                contentTextView = itemView.findViewById(R.id.contentTextView);
                return;
            }

            if (viewType > 1 && viewType < 4) {
                thumbnailImageView = itemView.findViewById(R.id.thumbnailImageView);
                return;
            }

            if (viewType > 3 && viewType < 6) {
                videoThumbnail = itemView.findViewById(R.id.videoThumbnailImageView);
                playerDurationTextView = itemView.findViewById(R.id.playerDurationTextView);
                return;
            }

            if (viewType > 5 && viewType < 8) {
                if (viewType == 6)
                    audioImageViewTo = itemView.findViewById(R.id.audioImageViewTo);
                else
                    audioImageViewFrom = itemView.findViewById(R.id.audioImageViewFrom);

                playerDurationTextView = itemView.findViewById(R.id.playerDurationTextView);
                return;
            }

            if (viewType > 7 && viewType < 10) {
                stickerImageView = itemView.findViewById(R.id.stickerImageView);
                return;
            }

            if (viewType > 9) {
                if (viewType == 10)
                    documentImageViewTo = itemView.findViewById(R.id.documentImageViewTo);
                else
                    documentImageViewFrom = itemView.findViewById(R.id.documentImageViewFrom);

                documentNameTextView = itemView.findViewById(R.id.documentNameTextView);
                documentSizeTextView = itemView.findViewById(R.id.documentSizeTextView);
                documentView = itemView.findViewById(R.id.documentView);
                downloadedPercentTextView = itemView.findViewById(R.id.downloadedPercentTextView);
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = -1;
        switch (viewType) {
            case 0:
                layout = R.layout.message_to;
                break;
            case 1:
                layout = R.layout.message_from;
                break;
            case 2:
                layout = R.layout.picture_to;
                break;
            case 3:
                layout = R.layout.picture_from;
                break;
            case 4:
                layout = R.layout.video_to;
                break;
            case 5:
                layout = R.layout.video_from;
                break;
            case 6:
                layout = R.layout.voice_to;
                break;
            case 7:
                layout = R.layout.voice_from;
                break;
            case 8:
                layout = R.layout.sticker_to;
                break;
            case 9:
                layout = R.layout.sticker_from;
                break;
            case 10:
                layout = R.layout.document_to;
                break;
            case 11:
                layout = R.layout.document_from;
                break;
        }

        final View v = LayoutInflater
                .from(parent.getContext())
                .inflate(layout, parent, false);

        return new ViewHolder(v, viewType);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Msg message = mMessage.get(position);

        holder.createdAtTextView.setText(MyDateFormat.generate(message.getCreatedAt(), "hh:mm"));
        if (holder.senderImageView != null) {
            Glide.with(holder.itemView.getContext())
                    .load((message.getSendersPicture().isEmpty()) ? R.mipmap.ic_launcher_round : UrlConstants.getFileUrl(message.getSendersPicture()))
                    .into(holder.senderImageView);
        }

        switch (message.getMessageType()) {
            case "text":
                holder.contentTextView.setText(message.getContent());
                break;
            case "photo":
                Glide.with(holder.itemView.getContext())
                        .load(UrlConstants.getFileUrl(message.getContent()))
                        .apply(imageBorder)
                        .into(holder.thumbnailImageView);
                break;
            case "video":
                Glide.with(holder.itemView.getContext())
                        .load(UrlConstants.getFileUrl(message.getContent() + ".jpg"))
                        .apply(imageBorder)
                        .into(holder.videoThumbnail);
                holder.playerDurationTextView.setText(Player.converDuration(message.getDuration()));
                break;
            case "audio":
                audioViewSetter(holder, holder.getAdapterPosition(), message);
                break;
            case "sticker":
                Glide.with(holder.itemView.getContext())
                        .load(UrlConstants.getFileUrl(message.getContent()))
                        .into(holder.stickerImageView);
                break;
            case "document":
                documentViewSetter(holder, message);
                break;
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickListener(message, holder.getAdapterPosition(), message.getMessageType());
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                longClickListener.onItemLongClickListener(message.getMessageId(), holder.getAdapterPosition());
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMessage.size();
    }

    @Override
    public int getItemViewType(int position) {
        Msg message = mMessage.get(position);
        int type = 1;
        if (message.getOwnerId().equals(ProfilePreferences.getInstance().getId())) {
            type = 0;
        }
        switch (message.getMessageType()) {
            case "text":
                type += 0;
                break;
            case "photo":
                type += 2;
                break;
            case "video":
                type += 4;
                break;
            case "audio":
                type += 6;
                break;
            case "sticker":
                type += 8;
                break;
            case "document":
                type += 10;
                break;
        }
        return type;
    }

    private void audioViewSetter(final ViewHolder holder, int position, final Msg message) {
        if (holder.audioImageViewTo != null) {
            holder.playerDurationTextView.setText(Player.converDuration(message.getDuration()));
            if (mMediaPlayingPosition == position) {
                holder.audioImageViewTo.setImageResource(R.drawable.vd_stop_white);
            } else {
                holder.audioImageViewTo.setImageResource(R.drawable.vd_player_play_white);
            }
        } else {

        }

        if (holder.audioImageViewFrom != null) {
            holder.playerDurationTextView.setText(Player.converDuration(message.getDuration()));
            if (mMediaPlayingPosition == position) {
                holder.audioImageViewFrom.setImageResource(R.drawable.vd_stop);
            } else {
                holder.audioImageViewFrom.setImageResource(R.drawable.vd_player_play);
            }
        } else {

        }
    }

    private void documentViewSetter(final ViewHolder holder, final Msg message) {
        if (message.getDownloadedPercent() > 0) {
            holder.downloadedPercentTextView.setText(holder.itemView.getResources().getString(R.string.downloaded_percent, message.getDownloadedPercent()));
        } else {
            holder.downloadedPercentTextView.setText("");
        }
        holder.documentNameTextView.setText(message.getFilename());
        holder.documentSizeTextView.setText(holder.itemView.getResources().getString(R.string.mb, message.getFileSizeToKb()));

        if (holder.documentImageViewTo != null) {
            if (FileStorage.getInstance().exist(message.getMessageId())) {
                holder.documentImageViewTo.setBackgroundResource(R.mipmap.ic_action_attach_white);
            } else {
                holder.documentImageViewTo.setBackgroundResource(R.mipmap.ic_action_download_white);
            }
        }

        if (holder.documentImageViewFrom != null) {
            if (FileStorage.getInstance().exist(message.getMessageId())) {
                holder.documentImageViewFrom.setBackgroundResource(R.mipmap.ic_action_attach);
            } else {
                holder.documentImageViewFrom.setBackgroundResource(R.mipmap.ic_action_download);
            }
        }
    }

    public Msg getItem(int position) {
        return mMessage.size() > 0 ? mMessage.get(position) : null;
    }

    public interface RecyclerClickListener {
        void onClickListener(Msg message, int position, String type);
    }

    public interface RecyclerItemLongClickListener {
        void onItemLongClickListener(String messageId, int position);
    }

    public void setMediaPlayingPosition(int position) {
        this.mMediaPlayingPosition = position;
    }

    public int getMediaPlayingPosition() {
        return mMediaPlayingPosition;
    }

    public MsgAdapter(List<Msg> messages, RecyclerClickListener listener, RecyclerItemLongClickListener longClickListener) {
        this.mMessage = messages;
        this.listener = listener;
        this.longClickListener = longClickListener;
    }
}
