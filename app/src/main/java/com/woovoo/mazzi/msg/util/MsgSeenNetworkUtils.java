package com.woovoo.mazzi.msg.util;

import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.socket.AppSocketListener;
import com.woovoo.mazzi.socket.SocketEventConstants;

import org.json.JSONException;
import org.json.JSONObject;

public class MsgSeenNetworkUtils {

    /**
     * Key @params
     */
    private static final String messageId = "message_id";
    private static final String userId = "user_id";

    public void send(String msgId) {
        JSONObject object = new JSONObject();
        try {
            object.put(messageId, msgId);
            object.put(userId, ProfilePreferences.getInstance().getId());
            AppSocketListener.getInstance().emit(SocketEventConstants.messageSeen, object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
