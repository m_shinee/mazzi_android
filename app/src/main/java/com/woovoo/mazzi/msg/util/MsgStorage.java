package com.woovoo.mazzi.msg.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;

public class MsgStorage extends MsgNetworkUtils {

    private static final String LOG_TAG = "MsgStorage";
    private static MsgStorage instance;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private MsgStorage(Context context) {
        preferences = context.getSharedPreferences(LOG_TAG, Context.MODE_PRIVATE);
    }

    public static MsgStorage getInstance(Context context) {
        if (instance == null)
            instance = new MsgStorage(context);

        return instance;
    }

    public static MsgStorage getInstance() {
        if (instance != null)
            return instance;

        throw new IllegalArgumentException("Should use getInstance(Context) at least once before using this method.");
    }

    @SuppressLint("CommitPrefEdits")
    private void doEdit() {
        editor = preferences.edit();
    }

    private void doApply() {
        if (editor != null) {
            editor.apply();
            editor = null;
        }
    }

    public void clearAll() {
        doEdit();
        editor.clear();
        doApply();
    }

    private boolean exist(String conversationId) {
        return preferences.contains(conversationId);
    }

    public JSONArray getAll(String conversationId) {
        try {
            if (exist(conversationId))
                return new JSONArray(preferences.getString(conversationId, null));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new JSONArray();
    }

    public void putMsg(String conversationId, JSONArray array) {
        doEdit();
        editor.putString(conversationId, array.toString());
        doApply();
    }

}