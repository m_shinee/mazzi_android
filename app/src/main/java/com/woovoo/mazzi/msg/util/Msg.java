package com.woovoo.mazzi.msg.util;

import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.contact.util.ContactStorage;
import com.woovoo.mazzi.profile.util.ProfilePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Msg {

    private static final String LOG_TAG = "Msg";

    private String messageId = "";
    private String messageType = "";
    private String sendersPicture = "";
    private String content = "";
    private String ownerId = "";
    private String conversationId = "";
    private String filename = "";
    private int downloadedPercent = 0;
    private long duration = 0;
    private long createdAt = MyDateFormat.currentTime();
    private int fileSize = 0;
    private boolean seen = false;

    public Msg() {
    }

    /**
     * @param object parse JSONObject
     */
    public Msg(JSONObject object) {
        try {
            if (object.has(MsgPreferences.ID))
                setMessageId(object.getString(MsgPreferences.ID));

            setOwnerId(object.getString(MsgPreferences.OWNER_ID));
            setMessageType(object.getString(MsgPreferences.MESSAGE_TYPE));
            setContent(object.getString(MsgPreferences.CONTENT));
            setDuration(object.getLong(MsgPreferences.DURATION));
            setCreatedAt(object.getLong(MsgPreferences.CREATED_AT));
            setConversationId(object.getString(MsgPreferences.CONVERSATION_ID));

            setFilename(object.has(MsgPreferences.FILENAME) ? object.getString(MsgPreferences.FILENAME) : "");
            setFileSize(object.has(MsgPreferences.FILE_SIZE) ? object.getInt(MsgPreferences.FILE_SIZE) : 0);

            if (!getOwnerId().equals(ProfilePreferences.getInstance().getId())) {
                setSendersPicture(ContactStorage.getInstance().getImage(getOwnerId()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * MsgActivity@sendRequest
     *
     * @param object         response
     * @param conversationId conversationId
     * @param type           messageType
     */
    public Msg(JSONObject object, String conversationId, String type) {
        try {
            setMessageId("");
            setMessageType(type);
            setConversationId(conversationId);
            setContent(object.getString(MsgPreferences.CONTENT));
            setDuration(object.getLong(MsgPreferences.DURATION));
            setOwnerId(ProfilePreferences.getInstance().getId());
            setCreatedAt(MyDateFormat.currentTime());

            setFilename(object.has(MsgPreferences.FILENAME) ? object.getString(MsgPreferences.FILENAME) : "");
            setFileSize(object.has(MsgPreferences.FILE_SIZE) ? object.getInt(MsgPreferences.FILE_SIZE) : 0);

            setSeen(true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param json_string parse JSONString
     */
    public Msg(String json_string) {
        try {
            JSONObject object = new JSONObject(json_string);
            setMessageId(object.getString(MsgPreferences.ID));
            setMessageType(object.getString(MsgPreferences.MESSAGE_TYPE));
            setContent(object.getString(MsgPreferences.CONTENT));
            setDuration(object.getLong(MsgPreferences.DURATION));
            setCreatedAt(object.getLong(MsgPreferences.CREATED_AT));
            setOwnerId(object.getString(MsgPreferences.OWNER_ID));
            setConversationId(object.getString(MsgPreferences.CONVERSATION_ID));
            setSendersPicture(ContactStorage.getInstance().getImage(getOwnerId()));

            setFilename(object.has(MsgPreferences.FILENAME) ? object.getString(MsgPreferences.FILENAME) : "");
            setFileSize(object.has(MsgPreferences.FILE_SIZE) ? object.getInt(MsgPreferences.FILE_SIZE) : 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public void setSendersPicture(String sendersPicture) {
        this.sendersPicture = sendersPicture;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public void setDownloadedPercent(int downloadedPercent) {
        this.downloadedPercent = downloadedPercent;
    }

    public String getConversationId() {
        return conversationId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public String getSendersPicture() {
        return sendersPicture;
    }

    public String getContent() {
        return content;
    }

    public String getMessageType() {
        return messageType;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getFilename() {
        return filename;
    }

    public boolean isSeen() {
        return seen;
    }

    public long getDuration() {
        return duration;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public int getFileSize() {
        return fileSize;
    }

    public int getDownloadedPercent() {
        return downloadedPercent;
    }

    public String getFileSizeToKb() {
        double size = (double) fileSize / 1024 / 1024;
        DecimalFormat df2 = new DecimalFormat(".##");
        df2.setRoundingMode(RoundingMode.UP);
        if (size < 1) {
            return 0 + df2.format(size);
        }
        return df2.format(size);
    }
}