package com.woovoo.mazzi.msg.util;

import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.socket.AppSocketListener;
import com.woovoo.mazzi.socket.SocketEventConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MsgTyping {

    private static final String typing = "typing";
    private static final String conversationId = "conversation_id";
    private static final String ownerId = "owner_id";
    private static final String participants = "participants";

    public void send() {
        JSONObject object = new JSONObject();
        try {
            object.put(typing, true);
            object.put(conversationId, MsgPreferences.getInstance().getConversationId());
            object.put(ownerId, ProfilePreferences.getInstance().getId());
            object.put(participants, MsgPreferences.getInstance().getParticipants());
            AppSocketListener.getInstance().emit(SocketEventConstants.messageTyping, object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}