package com.woovoo.mazzi.msg.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.woovoo.mazzi.profile.util.ProfilePreferences;

import org.json.JSONArray;
import org.json.JSONException;

public class MsgPreferences extends MsgNetworkUtils {

    private static final String LOG_TAG = "MsgPreferences";
    private static MsgPreferences instance;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private MsgPreferences(Context context) {
        preferences = context.getSharedPreferences(LOG_TAG, Context.MODE_PRIVATE);
    }

    public static MsgPreferences getInstance(Context context) {
        if (instance == null) {
            instance = new MsgPreferences(context);
        }

        return instance;
    }

    public static MsgPreferences getInstance() {
        if (instance != null) {
            return instance;
        }

        throw new IllegalArgumentException("Should use getInstance(Context) at least once before using this method.");
    }

    @SuppressLint("CommitPrefEdits")
    private void doEdit() {
        editor = preferences.edit();
    }

    private void doApply() {
        if (editor != null) {
            editor.apply();
            editor = null;
        }
    }

    public void clearAll() {
        doEdit();
        editor.clear();
        doApply();
    }

    public void setId(String messageId) {
        doEdit();
        editor.putString(ID, messageId);
        doApply();
    }

    public void setContent(String content) {
        doEdit();
        editor.putString(CONTENT, content);
        doApply();
    }

    public void setSeen(boolean isSeen) {
        doEdit();
        editor.putBoolean(SEEN, isSeen);
        doApply();
    }

    public void setCreatedAt(Long createdAt) {
        doEdit();
        editor.putLong(CREATED_AT, createdAt);
        editor.apply();
        editor = null;
    }

    public void setOwnerId(String ownerId) {
        doEdit();
        editor.putString(OWNER_ID, ownerId);
        doApply();
    }

    public void setMessageType(String messageType) {
        doEdit();
        editor.putString(MESSAGE_TYPE, messageType);
        doApply();
    }

    public void setDuration(Long duration) {
        doEdit();
        editor.putLong(DURATION, duration);
        doApply();
    }

    public void setConversationId(String conversationId) {
        doEdit();
        editor.putString(CONVERSATION_ID, conversationId);
        doApply();
    }

    public void setParticipants(JSONArray participants) {
        doEdit();
        editor.putString(mParticipants, participants.toString());
        doApply();
    }

    public void setFileName(String filename) {
        doEdit();
        editor.putString(FILENAME, filename);
        doApply();
    }

    public void setFileSize(int fileSize) {
        doEdit();
        editor.putInt(FILE_SIZE, fileSize);
        doApply();
    }

    public String getId() {
        return preferences.getString(ID, null);
    }

    public String getContent() {
        return preferences.getString(CONTENT, null);
    }

    public boolean isSeen() {
        return preferences.getBoolean(SEEN, false);
    }

    public JSONArray getSeen() {
        JSONArray array = new JSONArray();
        array.put(ProfilePreferences.getInstance().getId());
        return array;
    }

    public String getOwnerId() {
        return preferences.getString(OWNER_ID, null);
    }

    public String getMessageType() {
        return preferences.getString(MESSAGE_TYPE, null);
    }

    public Long getDuration() {
        return preferences.getLong(DURATION, 0);
    }

    public Long getCreatedAt() {
        return preferences.getLong(CREATED_AT, 0);
    }

    public String getConversationId() {
        return preferences.getString(CONVERSATION_ID, "");
    }

    public String getFileName() {
        return preferences.getString(FILENAME, "");
    }

    public int getFileSize() {
        return preferences.getInt(FILE_SIZE, 0);
    }

    public JSONArray getParticipants() {
        JSONArray array = new JSONArray();
        try {
            array = new JSONArray(preferences.getString(mParticipants, null));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return array;
    }

    public String getFirstParticipant() {
        JSONArray array;
        String participantId = null;
        try {
            array = new JSONArray(preferences.getString(mParticipants, null));
            for (int i = 0; i < array.length(); i++) {
                participantId = array.getString(0);
                if (!ProfilePreferences.getInstance().getId().equals(participantId))
                    return participantId;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return participantId;
    }
}
