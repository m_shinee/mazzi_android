package com.woovoo.mazzi.msg.util;

import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.socket.AppSocketListener;
import com.woovoo.mazzi.socket.SocketEventConstants;

import org.json.JSONException;
import org.json.JSONObject;

public class MsgNetworkUtils {

    private static final String LOG_TAG = "MsgNetworkUtils";

    /**
     * Socket key values
     */
    public static final String ID = "_id";
    public static final String CONVERSATION_ID = "conversation_id";
    public static final String CONTENT = "content";
    public static final String OWNER_ID = "owner_id";
    public static final String MESSAGE_TYPE = "type";
    public static final String SEEN = "seen";
    public static final String DURATION = "duration";
    public static final String CREATED_AT = "created_at";
    public static final String FILENAME = "filename";
    public static final String FILE_SIZE = "file_size";

    /**
     * key values
     */
    public static final String mParticipants = "participants";

    /**
     * Sent params to server
     */
    public void send() {
        JSONObject object = new JSONObject();
        try {
            object.put(CONVERSATION_ID, MsgPreferences.getInstance().getConversationId());
            object.put(CONTENT, MsgPreferences.getInstance().getContent());
            object.put(OWNER_ID, ProfilePreferences.getInstance().getId());
            object.put(MESSAGE_TYPE, MsgPreferences.getInstance().getMessageType());
            object.put(SEEN, MsgPreferences.getInstance().getSeen());
            object.put(DURATION, MsgPreferences.getInstance().getDuration());
            object.put(CREATED_AT, MyDateFormat.currentTime());
            object.put(FILENAME, MsgPreferences.getInstance().getFileName());
            object.put(FILE_SIZE, MsgPreferences.getInstance().getFileSize());
            AppSocketListener.getInstance().emit(SocketEventConstants.messageSend, object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
