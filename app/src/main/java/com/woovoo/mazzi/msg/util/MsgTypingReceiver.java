package com.woovoo.mazzi.msg.util;

import org.json.JSONException;
import org.json.JSONObject;

public class MsgTypingReceiver {

    private String conversationId;
    private String ownerId;

    public MsgTypingReceiver(String json_string) {
        try {
            JSONObject object = new JSONObject(json_string);
            this.conversationId = object.getString("conversation_id");
            this.ownerId = object.getString("owner_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getConversationId() {
        return conversationId;
    }

    public String getOwnerId() {
        return ownerId;
    }
}
