package com.woovoo.mazzi.msg.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.woovoo.mazzi.call.ui.DialingActivity;
import com.woovoo.mazzi.call.util.CallNetworkUtils;
import com.woovoo.mazzi.camera.ui.CameraKitActivity;
import com.woovoo.mazzi.conversation.util.LeaveConversation;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.conversation.util.Conversation;
import com.woovoo.mazzi.directory.MazziDirectory;
import com.woovoo.mazzi.sticker.ui.StickersFragment;
import com.woovoo.mazzi.sticker.util.Sticker;
import com.woovoo.mazzi.file.util.FileDownloadStart;
import com.woovoo.mazzi.file.util.FileDownloading;
import com.woovoo.mazzi.file.util.FileState;
import com.woovoo.mazzi.file.util.FileStorage;
import com.woovoo.mazzi.main.util.FragmentPagerSupport;
import com.woovoo.mazzi.msg.util.MsgNetworkUtils;
import com.woovoo.mazzi.msg.util.MsgPreferences;
import com.woovoo.mazzi.msg.util.MsgSeenNetworkUtils;
import com.woovoo.mazzi.msg.util.MsgStorage;
import com.woovoo.mazzi.msg.util.MsgTyping;
import com.woovoo.mazzi.slide.ui.ImageSlideActivity;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.msg.MsgAdapter;
import com.woovoo.mazzi.socket.SocketEventConstants;
import com.woovoo.mazzi.msg.util.Msg;
import com.woovoo.mazzi.socket.util.MazziInternetStatus;
import com.woovoo.mazzi.call.util.AccessToken;
import com.woovoo.mazzi.socket.AppSocketListener;
import com.woovoo.mazzi.util.UrlConstants;
import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.profile.util.ProfilePreferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.socket.emitter.Emitter;

public class MsgActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG = "MsgActivity";
    private static final int ACTIVITY_START_CAMERA_APP = 0,
            SELECT_FILE_FROM_GALLERY = 1,
            SELECT_AUDIO_FILE = 2,
            SELECT_OTHER_FILE = 3,
            MEDIA_SLIDE_POSITION = 11,
            TYPING_TIMER_LENGTH = 2000;

    private RecyclerView mMessagesView;
    private EditText mInputMessageView;
    private TextView internetStatusTextView;
    private ImageView mTypingImageView;
    private ImageButton sendButton,
            stickerButton,
            cameraOpen,
            galleryOpen,
            voiceCallOpen,
            attachFileOpen;
    private LinearLayoutManager layoutManager;
    private LinearLayout msgInputLayout,
            stickerLayout;

    private ViewPager mViewPager;

    private MediaPlayer mp;

    private ArrayList<Msg> mMessages = new ArrayList<>();
    private MsgAdapter mChatAdapter;

    private Msg msg = new Msg();
    private MsgNetworkUtils msgNetworkUtils;
    private MsgTyping msgTyping;
    private MsgSeenNetworkUtils msgSeenNetworkUtils;

    private Handler mTypingHandler,
            mTypingReceiverHandler;

    private int mSkip = 0,
            mLimit = 40;

    private JSONArray mFilesLink = new JSONArray();

    private String mConversationId,
            mTitle;
    private boolean isLoading = false,
            mAppend = true,
            messageHasMore = true;
    private boolean mTyping = false,
            mTypingReceive = false;

    private CallNetworkUtils callNetworkUtils = CallNetworkUtils.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msg);
        EventBus.getDefault().register(this);

        mp = new MediaPlayer();
        msgNetworkUtils = new MsgNetworkUtils();
        msgTyping = new MsgTyping();
        msgSeenNetworkUtils = new MsgSeenNetworkUtils();
        mTypingHandler = new Handler();
        mTypingReceiverHandler = new Handler();

        mMessagesView = findViewById(R.id.messages);

        mTypingImageView = findViewById(R.id.TypingImageView);
        internetStatusTextView = findViewById(R.id.internetStatus);

        msgInputLayout = findViewById(R.id.msgInputLayout);
        stickerLayout = findViewById(R.id.stickerLayout);

        mInputMessageView = findViewById(R.id.messageInput);
        sendButton = findViewById(R.id.sendButton);
        stickerButton = findViewById(R.id.stickerButton);
        cameraOpen = findViewById(R.id.cameraOpen);
        galleryOpen = findViewById(R.id.galleryOpen);
        voiceCallOpen = findViewById(R.id.voiceCallOpen);
        attachFileOpen = findViewById(R.id.attachFileOpen);

        prepareData();
        initializeClickListener();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(mTitle);
        }

        mChatAdapter = new MsgAdapter(mMessages, new MsgAdapter.RecyclerClickListener() {
            @Override
            public void onClickListener(Msg message, int position, String viewType) {
                if (Objects.equals(viewType, "video") || Objects.equals(viewType, "photo")) {
                    if (mp != null) {
                        mp.stop();
                    }
                    int previous = mChatAdapter.getMediaPlayingPosition();
                    mChatAdapter.setMediaPlayingPosition(-1);
                    mChatAdapter.notifyItemChanged(previous);
                    Intent intent = new Intent(getApplicationContext(), ImageSlideActivity.class);
                    intent.putExtra("selected_file", message.getContent());
                    startActivityForResult(intent, MEDIA_SLIDE_POSITION);
                    return;
                }

                if (Objects.equals(viewType, "document")) {
                    if (FileStorage.getInstance().exist(message.getMessageId())) {
                        openFile(message.getMessageId());
                    } else if (FileState.getInstance().isIdle(message.getMessageId())) {
                        setDownload(message, position);
                        FileState.getInstance().put(message.getMessageId(), FileState.getInstance().setDownloading());
                    } else if (FileState.getInstance().isDownloading(message.getMessageId())) {
                        Toast.makeText(getApplicationContext(), getString(R.string.quota_exceeded), Toast.LENGTH_SHORT).show();
                    }
                    return;
                }

                if (Objects.equals(viewType, "audio")) {
                    if (mChatAdapter.getMediaPlayingPosition() == position) {
                        if (mp != null && mp.isPlaying()) {
                            int previous = mChatAdapter.getMediaPlayingPosition();
                            mChatAdapter.setMediaPlayingPosition(-1);
                            mChatAdapter.notifyItemChanged(previous);
                            mp.stop();
                        } else {
                            prepareAudio(message);
                        }
                    } else {
                        int previous = mChatAdapter.getMediaPlayingPosition();
                        mChatAdapter.setMediaPlayingPosition(position);
                        mChatAdapter.notifyItemChanged(previous);
                        prepareAudio(message);
                    }
                }
                mChatAdapter.notifyItemChanged(position);
            }
        }, new MsgAdapter.RecyclerItemLongClickListener() {
            @Override
            public void onItemLongClickListener(final String messageId, final int position) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MsgActivity.this);
                alertDialog.setTitle(getString(R.string.warning));
                alertDialog.setMessage(getString(R.string.are_you_sure_to_delete_this_message));
                alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            deleteMsg(messageId, position);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog dialog = alertDialog.create();
                dialog.show();
            }
        });

        mMessagesView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mMessagesView.setAdapter(mChatAdapter);
        layoutManager = (LinearLayoutManager) mMessagesView.getLayoutManager();
        layoutManager.setStackFromEnd(true);
        firstTimeLoadMessage();

        AppSocketListener.getInstance().addOnHandler(SocketEventConstants.messageTypingReceiver, onMessageTypingReceiver);
        AppSocketListener.getInstance().addOnHandler(SocketEventConstants.sentMessage, onSentMessage);

        mInputMessageView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                hideEmojiSticker();
            }
        });

        mInputMessageView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mInputMessageView.getText() == null)
                    return;

                if (!mTyping) {
                    msgTyping.send();
                    mTyping = true;
                }
                mTypingHandler.postDelayed(onTypingTimeout, TYPING_TIMER_LENGTH);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().trim().isEmpty())
                    sendButton.setImageResource(R.mipmap.ic_action_send_gray);
                else
                    sendButton.setImageResource(R.mipmap.ic_action_send);
            }
        });

        mMessagesView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    hideSoftKeyboard();
                    hideEmojiSticker();
                    return true;
                }
                return false;
            }
        });

        initializeViewPager();
        AccessToken.getInstance().fetch(mConversationId);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (MsgPreferences.getInstance().getParticipants().length() > 1) {
            getMenuInflater().inflate(R.menu.group_chat_menu, menu);
        } else {
            getMenuInflater().inflate(R.menu.chat_items, menu);
        }
        return true;
    }

    private void initializeViewPager() {
        mViewPager = findViewById(R.id.fragment_container);
        setUpViewPager();
        setViewPager();

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        View sticker = getLayoutInflater().inflate(R.layout.tab_emoji_sticker, null);
        ImageView imageView = sticker.findViewById(R.id.tab_emoji_sticker_icon);
        imageView.setImageResource(R.drawable.tab_ic_action_emoji_sticker);

        tabLayout.getTabAt(0).setCustomView(sticker);
    }

    private void setUpViewPager() {
        FragmentPagerSupport adapter = new FragmentPagerSupport(getSupportFragmentManager());
        adapter.addFragment(new StickersFragment());
        mViewPager.setAdapter(adapter);
    }

    private void setViewPager() {
        mViewPager.setCurrentItem(0);
    }

    private void prepareData() {
        sendSeenStatus(MsgPreferences.getInstance().isSeen());
        mTitle = getIntent().getStringExtra("title");

        mConversationId = MsgPreferences.getInstance().getConversationId();
        callNetworkUtils.setConversationId(mConversationId);
        callNetworkUtils.setParticipantId(MsgPreferences.getInstance().getFirstParticipant());
        callNetworkUtils.setCallerName(mTitle);
    }

    private void initializeClickListener() {
        sendButton.setOnClickListener(this);
        stickerButton.setOnClickListener(this);
        cameraOpen.setOnClickListener(this);
        galleryOpen.setOnClickListener(this);
        voiceCallOpen.setOnClickListener(this);
        attachFileOpen.setOnClickListener(this);
    }

    private void prepareAudio(Msg message) {
        if (mp == null)
            return;
        try {
            mp.reset();
            mp.setDataSource(UrlConstants.getFileUrl(message.getContent()));
            mp.prepareAsync();
            mp.setOnPreparedListener(new MyMediaOnPreparedListener());
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.stop();
                    mChatAdapter.notifyItemChanged(mChatAdapter.getMediaPlayingPosition());
                    mChatAdapter.setMediaPlayingPosition(-1);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private JSONObject getParams() {
        JSONObject object = new JSONObject();
        try {
            object.put("conversation_id", mConversationId);
            object.put("limit", mLimit);
            object.put("skip", mSkip);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

    private void firstTimeLoadMessage() {
        MazziNetwork.postANRequest(UrlConstants.getConversationMessages(), getParams())
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            mFilesLink = response.getJSONObject("conversation").getJSONArray("flink");
                            ProfilePreferences.getInstance().putFiles(mConversationId, mFilesLink.toString());

                            mMessagesView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                                @Override
                                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                                    super.onScrollStateChanged(recyclerView, newState);
                                }

                                @Override
                                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                    super.onScrolled(recyclerView, dx, dy);
                                    if (!isLoading && layoutManager.findFirstVisibleItemPosition() == 1) {
                                        isLoading = true;
                                        mAppend = false;
                                        mSkip += mLimit;
                                        loadMessages();
                                    }
                                }
                            });
                            JSONArray array = response.getJSONArray("messages");
                            MsgStorage.getInstance().clearAll();
                            MsgStorage.getInstance().putMsg(mConversationId, array);
                            showLoadedMessages(array);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        isLoading = false;
                    }

                    @Override
                    public void onError(ANError anError) {
                        showLoadedMessages(MsgStorage.getInstance().getAll(mConversationId));
                        isLoading = false;
                    }
                });
    }

    private void loadMessages() {
        if (messageHasMore) {
            MazziNetwork.postANRequest(UrlConstants.getConversationMessages(), getParams())
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getJSONArray("messages").length() < mLimit) {
                                    messageHasMore = false;
                                }
                                showLoadedMessages(response.getJSONArray("messages"));
                                isLoading = false;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d(LOG_TAG, anError.getLocalizedMessage());
                            isLoading = false;
                        }
                    });
        }
    }

    private void showLoadedMessages(JSONArray messages) {
        try {
            int from = 0;
            for (int i = messages.length() - 1; i > -1; i--) {
                msg = new Msg(messages.getJSONObject(i));
                if (mAppend) {
                    appendMessage(msg);
                } else {
                    prependMessage(from++, msg);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void deleteMsg(String messageId, final int position) throws JSONException {
        JSONObject object = new JSONObject();
        object.put("_id", messageId);
        MazziNetwork.postANRequest(UrlConstants.getMsgDelete(), object)
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean deleted = response.has("success") && response.getBoolean("success");
                            if (deleted) {
                                mMessages.remove(position);
                                mChatAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(LOG_TAG, "" + anError.getErrorCode());
                    }
                });
    }

    private void sendSeenStatus(boolean seen) {
        if (!seen) {
            msgSeenNetworkUtils.send(MsgPreferences.getInstance().getId());
        }
    }

    private void attemptSend() {
        String content = mInputMessageView.getText().toString().trim();
        if (TextUtils.isEmpty(content)) {
            mInputMessageView.requestFocus();
            return;
        }
        mInputMessageView.setText("");
        msg = new Msg();
        msg.setConversationId(mConversationId);
        msg.setMessageType("text");
        msg.setContent(content);
        msg.setDuration(0);
        msg.setCreatedAt(MyDateFormat.currentTime());
        msg.setOwnerId(ProfilePreferences.getInstance().getId());
        msg.setFilename("");
        msg.setFileSize(0);
        msg.setSeen(true);
        send();
    }

    /**
     * Send request to Socket and Event
     */
    private void send() {
        MsgPreferences.getInstance().setContent(msg.getContent());
        MsgPreferences.getInstance().setDuration(msg.getDuration());
        MsgPreferences.getInstance().setMessageType(msg.getMessageType());
        MsgPreferences.getInstance().setFileName(msg.getFilename());
        MsgPreferences.getInstance().setFileSize(msg.getFileSize());
        msgNetworkUtils.send();
    }

    public void appendMessage(Msg msg) {
        mMessages.add(msg);
        mChatAdapter.notifyItemInserted(mMessages.size() - 1);
    }

    public void prependMessage(int from, Msg msg) {
        mMessages.add(from, msg);
        mChatAdapter.notifyItemInserted(0);
    }

    private void scrollToBottom() {
        mMessagesView.scrollToPosition(mChatAdapter.getItemCount() - 1);
    }

    @Override
    protected void onDestroy() {
        if (mp != null) {
            mp.stop();
            mp.release();
        }
        MsgPreferences.getInstance().setConversationId("");
        EventBus.getDefault().unregister(this);
        AppSocketListener.getInstance().off(SocketEventConstants.messageTypingReceiver);
        AppSocketListener.getInstance().off(SocketEventConstants.sentMessage);
        super.onDestroy();
    }

    @Subscribe
    public void onEvent(MazziInternetStatus internet) {
        if (internet.isConnected()) {
            internetStatusTextView.setText(null);
            internetStatusTextView.setVisibility(View.GONE);
        } else {
            internetStatusTextView.setText(getString(R.string.no_internet_connection));
            internetStatusTextView.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onEvent(Msg msg) {
        // TODO: shineer msg irsen ued storage ryy append xiix
        if (mConversationId.equals(msg.getConversationId())) {
            if (msg.getMessageType().equals("phone") || msg.getMessageType().equals("video")) {
                mFilesLink.put(msg.getContent());
                ProfilePreferences.getInstance().putFiles(mConversationId, mFilesLink.toString());
            }
            appendMessage(msg);
            MsgPreferences.getInstance().setId(msg.getMessageId());
            sendSeenStatus(false);
            mTypingImageView.setVisibility(View.GONE);
            scrollToBottom();
        }
    }

    @Subscribe
    public void onEvent(Sticker sticker) {
        msg = new Msg();
        msg.setConversationId(mConversationId);
        msg.setMessageType("sticker");
        msg.setContent(sticker.getUrl());
        msg.setDuration(0);
        msg.setCreatedAt(MyDateFormat.currentTime());
        msg.setOwnerId(ProfilePreferences.getInstance().getId());
        msg.setSeen(true);
        send();
    }

    @Subscribe
    public void onEvent(FileDownloading file) {
        Msg message = mChatAdapter.getItem(file.getPosition());
        if (message != null && Objects.equals(message.getMessageId(), file.getMessageId())) {
            message.setDownloadedPercent(file.getDownloadedPercent());
            mChatAdapter.notifyItemChanged(file.getPosition());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String fileLocation;
        if (requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {
            fileLocation = data.getStringExtra("filepath");
            switch (data.getStringExtra("type")) {
                case "photo":
                    uploadFile(UrlConstants.uploadImage(), fileLocation, "photo");
                    break;
                case "video":
                    uploadFile(UrlConstants.uploadVideo(), fileLocation, "video");
                    break;
            }
        } else if (requestCode == SELECT_AUDIO_FILE && resultCode == RESULT_OK) {
            Uri selectedFile = data.getData();
            if (selectedFile != null) {
                fileLocation = getRealPathFromUri(selectedFile);
                uploadFile(UrlConstants.uploadAudio(), fileLocation, "audio");
            }
        } else if (requestCode == SELECT_FILE_FROM_GALLERY && resultCode == RESULT_OK) {
            Uri selectedFile = data.getData();
            if (selectedFile != null) {
                fileLocation = getRealPathFromUri(selectedFile);
                if (selectedFile.toString().contains("image")) {
                    uploadFile(UrlConstants.uploadImage(), fileLocation, "photo");
                } else if (selectedFile.toString().contains("video")) {
                    uploadFile(UrlConstants.uploadVideo(), fileLocation, "video");
                }
            }
        } else if (requestCode == SELECT_OTHER_FILE && resultCode == RESULT_OK) {
            Uri selectedFile = data.getData();
            if (selectedFile != null) {
                fileLocation = selectedFile.getPath();
                Log.d(LOG_TAG, fileLocation);
                uploadFile(UrlConstants.uploadDocument(), fileLocation, "document");
            }
        }
    }

    private void uploadFile(String url, String filePath, final String type) {
        MazziNetwork.uploadANRequest(url, filePath, mConversationId)
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        Log.d(LOG_TAG, String.valueOf(bytesUploaded));
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (type.equals("photo")) {
                                response.put(MsgPreferences.CONTENT, response.getString("thumb"));
                                mFilesLink.put(response.getString("thumb"));
                            } else {
                                response.put(MsgPreferences.CONTENT, response.getString("url"));
                                if (type.equals("video")) {
                                    mFilesLink.put(response.getString("url"));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            msg = new Msg(response, mConversationId, type);
                            ProfilePreferences.getInstance().putFiles(mConversationId, mFilesLink.toString());
                            send();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(LOG_TAG, anError.getErrorBody());
                    }
                });
    }

    public String getRealPathFromUri(Uri mediaUri) {
        Cursor cursor = null;
        try {
            String[] project = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(mediaUri, project, null, null, null);
            if (cursor != null) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                String name = cursor.getString(columnIndex);
                cursor.close();
                return name;
            }
            return null;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private Emitter.Listener onMessageTypingReceiver = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject jsonObject = (JSONObject) args[0];
                    try {
                        if (!jsonObject.getString("conversation_id").equals(mConversationId) || jsonObject.getString("owner_id").equals(ProfilePreferences.getInstance().getId())) {
                            return;
                        }

                        addTyping();

                        if (!mTypingReceive) {
                            mTypingReceive = true;
                        }
                        mTypingReceiverHandler.postDelayed(onTypingReceiverTimeout, TYPING_TIMER_LENGTH);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Emitter.Listener onSentMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject object = (JSONObject) args[0];
                    try {
                        msg.setMessageId(object.getString("_id"));
                        EventBus.getDefault().post(new Conversation(msg));
                        appendMessage(msg);
                        scrollToBottom();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private Runnable onTypingReceiverTimeout = new Runnable() {
        @Override
        public void run() {
            if (!mTypingReceive) return;
            mTypingReceive = false;
            removeTyping();
        }
    };

    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
            if (!mTyping) return;
            mTyping = false;
        }
    };

    private void addTyping() {
        mTypingImageView.setVisibility(View.VISIBLE);
    }

    private void removeTyping() {
        mTypingImageView.setVisibility(View.GONE);
    }

    private boolean isAvailable(Intent intent) {
        final PackageManager manager = this.getPackageManager();
        List<ResolveInfo> info = manager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        return info.size() > 0;
    }

    private void leaveConversation() {
        JSONObject object = new JSONObject();
        try {
            object.put("conversation_id", mConversationId);
            object.put("user_id", ProfilePreferences.getInstance().getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MazziNetwork.postANRequest(UrlConstants.leaveConversation(), object)
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.has("success") && response.getBoolean("success")) {
                                EventBus.getDefault().post(new LeaveConversation.Builder()
                                        .setConversationId(mConversationId)
                                        .build());
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.failed), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                break;
            case R.id.voiceCall:
                goToDialingActivity(false);
                break;
            case R.id.videoCall:
                goToDialingActivity(true);
                break;
            case R.id.leaveConversation:
                leaveConversation();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.messageInput:
                break;
            case R.id.sendButton:
                attemptSend();
                break;
            case R.id.stickerButton:
                openStickerClicked();
                break;
            case R.id.cameraOpen:
                goToCameraKitActivity();
                break;
            case R.id.galleryOpen:
                openGallery();
                break;
            case R.id.voiceCallOpen:
                openAudioRecord();
                break;
            case R.id.attachFileOpen:
                openFilePicker();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (stickerLayout.getVisibility() == View.VISIBLE) {
            hideEmojiSticker();
            return;
        }

        super.onBackPressed();
    }

    private void hideOrDisplayEmojiSticker() {
        if (stickerLayout.getVisibility() == View.GONE) {
            showEmojiSticker();
        } else {
            hideEmojiSticker();
        }
    }

    private void setDownload(Msg msg, final int position) {
        EventBus.getDefault().post(new FileDownloadStart(msg.getMessageId(),
                msg.getContent(),
                msg.getMessageType(),
                msg.getFilename(),
                position));
    }

    private void showEmojiSticker() {
        stickerButton.setImageResource(R.mipmap.ic_action_emoji);
        stickerLayout.clearAnimation();
        stickerLayout.animate()
                .translationY(0)
                .alpha(1.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        stickerLayout.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void hideEmojiSticker() {
        stickerButton.setImageResource(R.mipmap.ic_action_emoji_gray);
        stickerLayout.clearAnimation();
        stickerLayout.animate()
                .translationY(stickerLayout.getHeight())
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        stickerLayout.setVisibility(View.GONE);
                    }
                });
    }

    private void hideSoftKeyboard() {
        mInputMessageView.clearFocus();
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(msgInputLayout.getWindowToken(), 0);
        }
    }

    private void openFile(String messageId) {
        String url = FileStorage.getInstance().getFilename(messageId);
        File file = new File(MazziDirectory.getDocumentFilepath(url));
        try {
            Uri uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".fileprovider", file);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (url.contains(".doc") || url.contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.contains(".ppt") || url.contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.contains(".xls") || url.contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.contains(".zip") || url.contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/x-wav");
            } else if (url.contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.contains(".wav") || url.contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.contains(".jpg") || url.contains(".jpeg") || url.contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.contains(".3gp") || url.contains(".mpg") ||
                    url.contains(".mpeg") || url.contains(".mpe") || url.contains(".mp4") || url.contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(), "No application found which can open the file", Toast.LENGTH_SHORT).show();
        }
    }

    private static class MyMediaOnPreparedListener implements MediaPlayer.OnPreparedListener {
        @Override
        public void onPrepared(MediaPlayer mediaPlayer) {
            mediaPlayer.start();
        }
    }

    // Menu clicked
    private void goToDialingActivity(boolean isVideoCall) {
        Intent intent = new Intent(getApplicationContext(), DialingActivity.class);
        intent.putExtra("is_video", isVideoCall);
        startActivity(intent);
    }

    private void openStickerClicked() {
        hideSoftKeyboard();
        hideOrDisplayEmojiSticker();
    }

    private void goToCameraKitActivity() {
        Intent callCameraApplicationIntent = new Intent(getApplicationContext(), CameraKitActivity.class);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
    }

    private void openGallery() {
        Intent callGalleryApplicationIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        callGalleryApplicationIntent.setType("image/* video/*");
        startActivityForResult(callGalleryApplicationIntent, SELECT_FILE_FROM_GALLERY);
    }

    private void openAudioRecord() {
        Intent recordSoundAction = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
        if (isAvailable(recordSoundAction)) {
            startActivityForResult(recordSoundAction, SELECT_AUDIO_FILE);
        } else {
            Toast.makeText(getApplicationContext(), R.string.does_not_support_this_action, Toast.LENGTH_SHORT).show();
        }
    }

    private void openFilePicker() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

//                 special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
//                 if you want any file type, you can skip next line
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (getPackageManager().resolveActivity(sIntent, 0) != null) {
            chooserIntent = Intent.createChooser(sIntent, getString(R.string.choose_file));
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{intent});
        } else {
            chooserIntent = Intent.createChooser(intent, getString(R.string.choose_file));
        }
        startActivityForResult(chooserIntent, SELECT_OTHER_FILE);
    }
}