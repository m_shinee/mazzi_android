package com.woovoo.mazzi.sticker.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.woovoo.mazzi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StickerStorage {

    private static final String LOG_TAG = "StickerStorage";
    private static StickerStorage instance;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private StickerStorage(Context context) {
        preferences = context.getSharedPreferences(LOG_TAG, Context.MODE_PRIVATE);
        putDefault();
    }

    public static StickerStorage getInstance(Context context) {
        if (instance == null)
            instance = new StickerStorage(context);

        return instance;
    }

    public static StickerStorage getInstance() {
        if (instance != null)
            return instance;

        throw new IllegalArgumentException("Should use getInstance(Context) at least once before using this method.");
    }

    @SuppressLint("CommitPrefEdits")
    private void doEdit() {
        editor = preferences.edit();
    }

    private void doApply() {
        if (editor != null) {
            editor.apply();
            editor = null;
        }
    }

    public void clearAll() {
        doEdit();
        editor.clear();
        doApply();
    }

    private boolean exist(String stickerId) {
        return preferences.contains(stickerId);
    }

    private void putDefault() {
        if (!exist("5a616c5b2616de0e349d4947")) {
            doEdit();
            JSONArray array = new JSONArray();
            JSONObject object = new JSONObject();
            try {
                object.put("name", "huu1");
                object.put("url", "stickers/mongolhuu/huu1.png");
                object.put("resource", R.drawable.huu1);
                array.put(object);

                object = new JSONObject();
                object.put("name", "huu2");
                object.put("url", "stickers/mongolhuu/huu2.png");
                object.put("resource", R.drawable.huu2);
                array.put(object);

                object = new JSONObject();
                object.put("name", "huu3");
                object.put("url", "stickers/mongolhuu/huu3.png");
                object.put("resource", R.drawable.huu3);
                array.put(object);

                object = new JSONObject();
                object.put("name", "huu4");
                object.put("url", "stickers/mongolhuu/huu4.png");
                object.put("resource", R.drawable.huu4);
                array.put(object);

                object = new JSONObject();
                object.put("name", "huu5");
                object.put("url", "stickers/mongolhuu/huu5.png");
                object.put("resource", R.drawable.huu5);
                array.put(object);

                object = new JSONObject();
                object.put("name", "huu6");
                object.put("url", "stickers/mongolhuu/huu6.png");
                object.put("resource", R.drawable.huu6);
                array.put(object);
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                editor.putString("5a616c5b2616de0e349d4947", array.toString());
                doApply();
            }
        }
    }

    public JSONArray getDefault() {
        JSONArray array = new JSONArray();
        try {
            array = new JSONArray(preferences.getString("5a616c5b2616de0e349d4947", null));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return array;
    }

    public JSONObject getSticker(String conversationId, int position) {
        JSONArray array;
        JSONObject object = new JSONObject();
        if (!exist(conversationId)) {
            return object;
        }

        try {
            array = new JSONArray(preferences.getString(conversationId, null));
            object = array.getJSONObject(position);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return object;
    }

}
