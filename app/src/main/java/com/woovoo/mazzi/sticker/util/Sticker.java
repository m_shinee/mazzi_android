package com.woovoo.mazzi.sticker.util;

import org.json.JSONException;
import org.json.JSONObject;

public class Sticker {

    private String id;
    private String name;
    private String url;

    public Sticker(JSONObject object) {
        try {
//            this.id = object.getString("id");
            this.name = object.getString("name");
            this.url = object.getString("url");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }
}