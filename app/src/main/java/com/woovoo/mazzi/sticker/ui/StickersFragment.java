package com.woovoo.mazzi.sticker.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.woovoo.mazzi.R;
import com.woovoo.mazzi.sticker.StickerImageAdapter;
import com.woovoo.mazzi.sticker.util.Sticker;
import com.woovoo.mazzi.sticker.util.StickerStorage;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

public class StickersFragment extends Fragment {

    private static final String LOG_TAG = "StickersFragment";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sticker, container, false);
        GridView gridView = view.findViewById(R.id.stickerGridView);
        gridView.setAdapter(new StickerImageAdapter(view.getContext()));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                JSONObject object = StickerStorage.getInstance().getSticker("5a616c5b2616de0e349d4947", i);
                EventBus.getDefault().post(new Sticker(object));
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
