package com.woovoo.mazzi.sticker;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.woovoo.mazzi.sticker.util.StickerStorage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StickerImageAdapter extends BaseAdapter {
    private Context mContext;
    private JSONArray stickers;

    public StickerImageAdapter(Context context) {
        this.mContext = context;
        stickers = StickerStorage.getInstance().getDefault();
    }

    @Override
    public int getCount() {
        return stickers.length();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            GridView.LayoutParams params = new GridView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            imageView.setPadding(10, 10, 10, 10);
            imageView.setLayoutParams(params);
            imageView.setAdjustViewBounds(true);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            imageView = (ImageView) convertView;
        }

        int resourceId = 0;
        try {
            JSONObject object;
            object = stickers.getJSONObject(i);
            resourceId = object.getInt("resource");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        imageView.setImageResource(resourceId);
        return imageView;
    }
}