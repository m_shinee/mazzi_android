package com.woovoo.mazzi;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.VideoView;

import java.io.File;

public class PreviewVideoActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_video);

        final String videoPath = getIntent().getStringExtra("filepath");

        ImageButton mChooseButton = findViewById(R.id.choose);
        ImageButton mRetryButton = findViewById(R.id.retry);
        VideoView playerView = findViewById(R.id.video_player);
        playerView.setVideoPath(videoPath);
        playerView.start();

        mChooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });

        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = new File(videoPath);
                boolean deleted = file.delete();
                if (deleted) {
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
