package com.woovoo.mazzi;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.io.File;

public class PreviewImageActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_image);

        ImageView mCapturedImage = findViewById(R.id.captured_image);
        ImageButton mChooseButton = findViewById(R.id.choose);
        ImageButton mRetryButton = findViewById(R.id.retry);
        final String filepath = getIntent().getStringExtra("filepath");
        final File file = new File(filepath);

        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
        mCapturedImage.setImageBitmap(bitmap);

        mChooseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });

        mRetryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File file = new File(filepath);
                boolean deleted = file.delete();
                if (deleted) {
                    setResult(RESULT_CANCELED);
                    finish();
                }
            }
        });

    }
}
