package com.woovoo.mazzi.util;

import java.util.Locale;

public class Player {

    public static String converDuration(long duration) {
        return String.format(Locale.ENGLISH, "%d:%02d", duration / 60, duration % 100);
    }

}