package com.woovoo.mazzi.util;

public class UrlConstants {

    public static final String CHAT_SERVER_URL = "https://sms.mazzi.mn:8001/";
    private static final String FILE_SERVER_URL = "https://post.mazzi.mn:8003/";

    private static String getChatUrl(String segment) {
        return CHAT_SERVER_URL + segment;
    }

    public static String getFileUrl(String segment) {
        return FILE_SERVER_URL + segment;
    }

    public static String updateProfile() {
        return getChatUrl("update_pro");
    }

    public static String getRegisteredUsers() {
        return getChatUrl("user_info");
    }

    public static String getContactProfile() {
        return getChatUrl("user_info_id");
    }

    public static String getConversationMessages() {
        return getChatUrl("get_messages");
    }

    public static String getConversations() {
        return getChatUrl("conversations");
    }

    public static String getConversation() {
        return getChatUrl("get_conversation");
    }

    public static String getSecurityKey() {
        return getChatUrl("getkey");
    }

    public static String signIn() {
        return getChatUrl("login");
    }

    public static String uploadImage() {
        return getFileUrl("imageupload");
    }

    public static String uploadVideo() {
        return getFileUrl("videoupload");
    }

    public static String uploadAudio() {
        return getFileUrl("audioupload");
    }

    public static String uploadDocument() {
        return getFileUrl("documentupload");
    }

    public static String createPrivateConversation() {
        return getChatUrl("create_private");
    }

    public static String createGroupConversation() {
        return getChatUrl("create_group");
    }

    public static String leaveConversation() {
        return getChatUrl("leave_conversation");
    }

    public static String getCallAccessTokenServer() {
        return getFileUrl("twilio");
    }

    public static String getMsgDelete() {
        return getChatUrl("delete_message");
    }

}