package com.woovoo.mazzi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MyDateFormat {

    public static String generate(long d) {
        generate(d, "yy/mm/dd hh:mm");
        try {
            DateFormat dateFormat = new SimpleDateFormat("yy/mm/dd hh:mm", Locale.ENGLISH);
            Date date = (new Date(d));
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String generate(long d, String mFormat) {
        try {
            DateFormat dateFormat = new SimpleDateFormat(mFormat, Locale.ENGLISH);
            Date date = (new Date(d));
            return dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static long currentTime() {
        return (new Date()).getTime();
    }

}