package com.woovoo.mazzi.util;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.woovoo.mazzi.directory.MazziDirectory;
import com.woovoo.mazzi.profile.util.ProfilePreferences;

import org.json.JSONObject;

import java.io.File;

public class MazziNetwork {
    private static final String LOG_TAG = "MazziNetwork";

    public static ANRequest postANRequest(String url, JSONObject params) {
        return AndroidNetworking.post(url)
                .addJSONObjectBody(params)
                .addHeaders("authorization", "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC")
                .setTag(LOG_TAG)
                .setPriority(Priority.MEDIUM)
                .build();
    }

    public static ANRequest getANRequest(String url, String conversationId) {
        return AndroidNetworking.get(url)
                .addQueryParameter("user_id", ProfilePreferences.getInstance().getId())
                .addQueryParameter("conversation_id", conversationId)
                .addQueryParameter("token", "BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC")
                .setTag(LOG_TAG)
                .setPriority(Priority.LOW)
                .build();
    }

    public static ANRequest uploadANRequest(String url, String filepath, String conversationId) {
        return AndroidNetworking.upload(url)
                .addMultipartFile("file", new File(filepath))
                .addMultipartParameter("conversation_id", conversationId)
                .setTag(LOG_TAG)
                .setPriority(Priority.MEDIUM)
                .build();
    }

    public static ANRequest downloadANRequest(String url, String messageType, String filename) {
        return AndroidNetworking.download(url, MazziDirectory.getDirectoryByMessageType(messageType), filename)
                .setTag(LOG_TAG)
                .setPriority(Priority.MEDIUM)
                .build();
    }
}