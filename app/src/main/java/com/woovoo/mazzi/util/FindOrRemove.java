package com.woovoo.mazzi.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FindOrRemove {

    public static boolean fromJSONArray(JSONArray jsonArray, String value) {
        return jsonArray.toString().contains("\"" + value + "\"");
    }

    public static JSONArray removeFromJSONArray(JSONArray jsonArray, String value) {
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                if (jsonArray.getString(i).equals(value)) {
                    jsonArray.remove(i);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

}