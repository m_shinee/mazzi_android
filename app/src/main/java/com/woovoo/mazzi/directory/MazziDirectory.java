package com.woovoo.mazzi.directory;

import android.os.Environment;

import java.io.File;

public class MazziDirectory {

    private static final String KEY_NAME = "Mazzi";

    public static String getImagePath() {
        return getImageDirectory().getPath();
    }

    public static String getVideoPath() {
        return getVideoDirectory().getPath();
    }

    public static String getDocumentPath() {
        return getDocumentDirectory().getPath();
    }

    public static String getAudioPath() {
        return getAudioDirectory().getPath();
    }

    public static String getDirectoryByMessageType(String messageType) {
        switch (messageType) {
            case "photo":
                return getImagePath();
            case "video":
                return getVideoPath();
            case "document":
                return getDocumentPath();
            case "audio":
                return getAudioPath();
            default:
                return getDocumentPath();
        }
    }

    public static String getImageFilepath(String filename) {
        return getImagePath() + "/" + filename;
    }

    public static String getVideoFilepath(String filename) {
        return getVideoPath() + "/" + filename;
    }

    public static String getDocumentFilepath(String filename) {
        return getDocumentPath() + "/" + filename;
    }

    public static String getAudioFilepath(String filename) {
        return getAudioPath() + "/" + filename;
    }

    public static File getImageDirectory() {
        return getDefaultPath(Environment.DIRECTORY_PICTURES);
    }

    public static File getVideoDirectory() {
        return getDefaultPath(Environment.DIRECTORY_MOVIES);
    }

    public static File getDocumentDirectory() {
        return getDefaultPath(Environment.DIRECTORY_DOCUMENTS);
    }

    public static File getAudioDirectory() {
        return getDefaultPath(Environment.DIRECTORY_MUSIC);
    }

    private static File getDefaultPath(String standardDirector) {
        return new File(Environment.getExternalStoragePublicDirectory(standardDirector), KEY_NAME);
    }

}
