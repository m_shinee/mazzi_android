package com.woovoo.mazzi.directory;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.File;

public class CreateDirectoriesTask extends AsyncTask<Void, Void, Boolean> {

    private static final String LOG_TAG = "CreateDirectoriesTask";

    @Override
    protected Boolean doInBackground(Void... voids) {
        File audioDirector = new File(MazziDirectory.getAudioPath());
        File documentDirector = new File(MazziDirectory.getDocumentPath());
        File videoDirector = new File(MazziDirectory.getVideoPath());
        File imageDirector = new File(MazziDirectory.getImagePath());

        if (!audioDirector.mkdirs()) {
            Log.e(LOG_TAG, "Sub audios directory not created");
        }

        if (!documentDirector.mkdirs()) {
            Log.e(LOG_TAG, "Sub documents directory not created");
        }

        if (!videoDirector.mkdirs()) {
            Log.e(LOG_TAG, "Sub videos directory not created");
        }

        if (!imageDirector.mkdirs()) {
            Log.e(LOG_TAG, "Sub pictures directory not created");
        }
        return true;
    }
}
