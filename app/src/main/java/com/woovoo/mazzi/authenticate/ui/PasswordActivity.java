package com.woovoo.mazzi.authenticate.ui;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.util.UrlConstants;
import com.woovoo.mazzi.profile.util.ProfilePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

public class PasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String LOG_TAG = "PasswordActivity";

    private String mPhoneNumber, mRegion, mFirebasePhoneNumber;
    private ProgressBar progressBar;
    private FrameLayout progressBarHolder;
    private String mVerificationId;

    private EditText textPassword;
    private TextView textError;

    private Button mVerifyButton;
    private Button mResendButton;

    private Chronometer chronometer;

    // Firebase
    private boolean mVerificationInProgress = false;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    // [START declare_auth]
    private FirebaseAuth mAuth;
    // [END declare_auth]

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        mPhoneNumber = getIntent().getStringExtra("phone");
        mRegion = getIntent().getStringExtra("region");

        mFirebasePhoneNumber = "+" + mRegion + mPhoneNumber;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(String.format("+(%s) %s", mRegion, mPhoneNumber));
        }


        progressBar = findViewById(R.id.progressbar);
        progressBarHolder = findViewById(R.id.progressBarHolder);
        textError = findViewById(R.id.error);
        textPassword = findViewById(R.id.textPassword);
        textPassword.requestFocus();

        textPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().trim().isEmpty()) {
                    mVerifyButton.setBackgroundResource(R.drawable.invalid_rounded_button);
                } else {
                    mVerifyButton.setBackgroundResource(R.drawable.rounded_button);
                }
            }
        });

        mVerifyButton = findViewById(R.id.verifyPhone);
        mResendButton = findViewById(R.id.resendVerification);
        chronometer = findViewById(R.id.countdown);

        mResendButton.setEnabled(false);

        mVerifyButton.setOnClickListener(this);
        mResendButton.setOnClickListener(this);

        // Firebase
        // [START initialize_auth]
        mAuth = FirebaseAuth.getInstance();
        mAuth.setLanguageCode("en");
        // [END initialize_auth]

        // Initialize phone auth callbacks
        // [START phone_auth_callbacks]
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(LOG_TAG, "onVerificationCompleted:" + credential);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                // [START_EXCLUDE silent]
                // Update the UI and attempt sign in with the phone credential
                // updateUI(STATE_VERIFY_SUCCESS, credential);
                // [END_EXCLUDE]
                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(LOG_TAG, "onVerificationFailed", e);
                // [START_EXCLUDE silent]
                mVerificationInProgress = false;
                // [END_EXCLUDE]

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    Toast.makeText(getApplicationContext(), getString(R.string.invalid_phone_number), Toast.LENGTH_SHORT).show();
                    // Invalid request
                    // [START_EXCLUDE]
                    textError.setText(getString(R.string.invalid_phone_number));
                    // [END_EXCLUDE]
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    textError.setText(R.string.quota_exceeded);
                    Snackbar.make(findViewById(android.R.id.content), getString(R.string.quota_exceeded),
                            Snackbar.LENGTH_SHORT).show();
                    // [END_EXCLUDE]
                }

                hideLoader();
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                // Log.d(LOG_TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                textError.setText(getString(R.string.code_sent));

                // [START_EXCLUDE]
                // Update UI
                hideLoader();
                // updateUI(STATE_CODE_SENT);
                // [END_EXCLUDE]
            }
        };
        // [END phone_auth_callbacks]

        startPhoneNumberVerification(mFirebasePhoneNumber);
        startCountDown();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            setResult(RESULT_CANCELED);
            finish();
            return true;
        }
        return true;
    }

    private void signIn() {
        mVerificationId = "";
        JSONObject params = new JSONObject();
        try {
            params.put("phone", mRegion + mPhoneNumber);
            params.put("key", mVerificationId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MazziNetwork.postANRequest(UrlConstants.signIn(), params)
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (!response.getBoolean("error")) {
                                ProfilePreferences.getInstance().putId(response.getString("_id"));
                                ProfilePreferences.getInstance().putNickname(response.getString("nickname"));
                                ProfilePreferences.getInstance().putLastname(response.getString("lastname"));
                                ProfilePreferences.getInstance().putEmail(response.getString("email"));
                                ProfilePreferences.getInstance().putSurname(response.getString("surname"));
                                ProfilePreferences.getInstance().putImage(response.getString("image"));
                                ProfilePreferences.getInstance().putRegion(mRegion);
                                ProfilePreferences.getInstance().putPhone(mPhoneNumber);
                                ProfilePreferences.getInstance().putBirthday(response.getLong("birthday"));
                                ProfilePreferences.getInstance().putConfirmed(response.getBoolean("is_confirmed"));
                                ProfilePreferences.getInstance().putOnlineAt(MyDateFormat.currentTime());
                                setResult(RESULT_OK);
                                finish();
                            } else {
                                textError.setText(getString(R.string.invalid_code));
                                Toast.makeText(getApplicationContext(), getString(R.string.invalid_code), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        hideLoader();
                        Log.d(LOG_TAG, anError.getLocalizedMessage());
                        textError.setText(anError.getLocalizedMessage());
                        Toast.makeText(getApplicationContext(), getString(R.string.invalid_code), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    /**
     * Firebase
     */
    private void startPhoneNumberVerification(String phoneNumber) {
        showLoader();

        // [START start_phone_auth]
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
        // [END start_phone_auth]

        mVerificationInProgress = true;
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        if (code.length() < 6) {
            hideLoader();
            textError.setText(getString(R.string.invalid_code));
            return;
        }

        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]

        signInWithPhoneAuthCredential(credential);
    }

    // [START resend_verification]
    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }
    // [END resend_verification]

    // [START sign_in_with_phone]
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            signIn();
                        } else {
                            hideLoader();
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                textError.setText(getString(R.string.invalid_code));
                            }
                        }
                    }
                });
    }
    // [END sign_in_with_phone]

    @Override
    public void onClick(View view) {
        textError.setText("");
        switch (view.getId()) {
            case R.id.verifyPhone:
                showLoader();
                verifyPhoneNumberWithCode(mVerificationId, textPassword.getText().toString().trim());
//                signIn();
                break;
            case R.id.resendVerification:
                showLoader();
                resendVerificationCode(mFirebasePhoneNumber, mResendToken);
                startCountDown();
                break;
        }
    }

    private void showLoader() {
        progressBarHolder.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void hideLoader() {
        progressBarHolder.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void startCountDown() {
        mResendButton.setEnabled(false);
        mResendButton.setBackgroundResource(R.drawable.invalid_rounded_button);
        chronometer.stop();
        chronometer.setVisibility(View.VISIBLE);
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                long sec = ((SystemClock.elapsedRealtime() - chronometer.getBase()) / 1000) % 60;
                if (sec > 50) {
                    mResendButton.setEnabled(true);
                    mResendButton.setBackgroundResource(R.drawable.rounded_button);
                    chronometer.setVisibility(View.GONE);
                    chronometer.stop();
                }
            }
        });
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();
    }
}
