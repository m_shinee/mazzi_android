package com.woovoo.mazzi.authenticate.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.main.ui.MainActivity;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.util.UrlConstants;

import org.json.JSONException;
import org.json.JSONObject;

public class AskUsernameActivity extends Activity {

    private static final String LOG_TAG = "ProfileActivity";

    private ProgressBar progressBar;
    private FrameLayout progressBarHolder;
    private String nickname;
    private EditText nicknameEditText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_first_time);

        nicknameEditText = findViewById(R.id.nickname);
        Button saveButton = findViewById(R.id.save);
        progressBar = findViewById(R.id.progressbar);
        progressBarHolder = findViewById(R.id.progressBarHolder);

        nicknameEditText.requestFocus();
        nicknameEditText.setText(ProfilePreferences.getInstance().getNickname());

        nicknameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == 11 || i == EditorInfo.IME_ACTION_DONE) {
                    saveNickname();
                    return true;
                }
                return false;
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNickname();
            }
        });
    }

    private void saveNickname() {
        progressBarHolder.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        nicknameEditText.setError(null);
        nickname = nicknameEditText.getText().toString().trim();

        if (nickname.isEmpty()) {
            nicknameEditText.setError(getString(R.string.empty_input));
            nicknameEditText.requestFocus();
            progressBarHolder.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            return;
        }

        JSONObject params = new JSONObject();
        try {
            params.put("_id", ProfilePreferences.getInstance().getId());
            params.put("nickname", nickname);
            params.put("lastname", ProfilePreferences.getInstance().getLastname());
            params.put("surname", ProfilePreferences.getInstance().getSurname());
            params.put("gender", "");
            params.put("birthday", ProfilePreferences.getInstance().getBirthday());
            params.put("image", ProfilePreferences.getInstance().getImage());
            params.put("email", ProfilePreferences.getInstance().getEmail());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MazziNetwork.postANRequest(UrlConstants.updateProfile(), params)
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProfilePreferences.getInstance().putNickname(nickname);
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressBarHolder.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(getApplicationContext(), getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
