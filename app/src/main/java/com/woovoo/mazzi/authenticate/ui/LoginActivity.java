package com.woovoo.mazzi.authenticate.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.util.UrlConstants;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends Activity implements View.OnClickListener {

    private static final String LOG_TAG = "LoginActivity";
    private int mPasswordActivityRequest = 1;
    private String mPhone_number, mRegion;

    private EditText phone_input, region;

    private ProgressBar progressBar;
    private FrameLayout progressBarHolder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        phone_input = findViewById(R.id.phone_input);
        region = findViewById(R.id.zipcode);
        region.setEnabled(false);

        progressBar = findViewById(R.id.progressbar);
        progressBarHolder = findViewById(R.id.progressBarHolder);

        phone_input.requestFocus();

        phone_input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == 10 || actionId == EditorInfo.IME_ACTION_NEXT) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button button = findViewById(R.id.sign_in_button);

        button.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (RESULT_OK == resultCode && requestCode == mPasswordActivityRequest) {
            Intent intent = new Intent(getApplicationContext(), AskUsernameActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void attemptLogin() {
        showLoader();

        if (!validate()) {
            return;
        }

        JSONObject params = new JSONObject();
        try {
            params.put("phone", mPhone_number);
            params.put("region", mRegion);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MazziNetwork.postANRequest(UrlConstants.getSecurityKey(), params)
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideLoader();
                        Intent intent = new Intent(getApplicationContext(), PasswordActivity.class);
                        intent.putExtra("phone", mPhone_number);
                        intent.putExtra("region", mRegion);
                        startActivityForResult(intent, mPasswordActivityRequest);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(LOG_TAG, anError.getLocalizedMessage());
                        hideLoader();
                        phone_input.requestFocus();
                        phone_input.setError(getString(R.string.connection_error));
                    }
                });
    }

    private boolean validate() {
        phone_input.setError(null);

        mPhone_number = phone_input.getText().toString().trim();
        mRegion = region.getText().toString().trim();
        hideLoader();
        if (TextUtils.isEmpty(mPhone_number)) {
            phone_input.setError(getString(R.string.empty_input));
            phone_input.requestFocus();
            return false;
        } else if (mPhone_number.length() < 8) {
            phone_input.requestFocus();
            phone_input.setError(getString(R.string.invalid_phone_number));
            return false;
        }
        return true;
    }

    private void showLoader() {
        progressBarHolder.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void hideLoader() {
        progressBarHolder.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                attemptLogin();
                break;
        }
    }
}
