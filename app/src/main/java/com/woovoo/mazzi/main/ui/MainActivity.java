package com.woovoo.mazzi.main.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.woovoo.mazzi.authenticate.ui.LoginActivity;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.file.util.FileDownloadStart;
import com.woovoo.mazzi.file.util.FileDownloading;
import com.woovoo.mazzi.file.util.FileState;
import com.woovoo.mazzi.file.util.FileStorage;
import com.woovoo.mazzi.main.util.FragmentPagerSupport;
import com.woovoo.mazzi.contact.ui.ContactFragment;
import com.woovoo.mazzi.contact.ui.ContactSearchActivity;
import com.woovoo.mazzi.contact.ui.CreateGroupActivity;
import com.woovoo.mazzi.conversation.ui.ConversationFragment;
import com.woovoo.mazzi.profile.util.Profile;
import com.woovoo.mazzi.profile.ui.ProfileActivity;
import com.woovoo.mazzi.socket.AppSocketListener;
import com.woovoo.mazzi.util.UrlConstants;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.socket.SocketEventConstants;
import com.woovoo.mazzi.socket.util.MazziInternetStatus;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import io.socket.client.Socket;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String LOG_TAG = "MainActivity";

    private DrawerLayout drawer;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private ViewPager mViewPager;
    private ImageView profilePicture;

    private TextView internetStatusTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppSocketListener.getInstance().restartSocket();
        EventBus.getDefault().register(this);

        internetStatusTextView = findViewById(R.id.internetStatus);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        actionBarDrawerToggle.setHomeAsUpIndicator(R.mipmap.ic_action_hanburger_white);
        actionBarDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(GravityCompat.START);
            }
        });

        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView nickname = headerView.findViewById(R.id.profile_nickname);

        nickname.setText(ProfilePreferences.getInstance().getNickname());
        navigationView.setNavigationItemSelectedListener(this);

        profilePicture = headerView.findViewById(R.id.profile_pic);
        if (!ProfilePreferences.getInstance().getImage().isEmpty()) {
            profilePicture.setPadding(0, 0, 0, 0);
            Glide.with(getApplicationContext())
                    .load(UrlConstants.getFileUrl(ProfilePreferences.getInstance().getImage()))
                    .apply(RequestOptions.centerCropTransform())
                    .into(profilePicture);
        }

        mViewPager = findViewById(R.id.fragment_container);
        setUpViewPager();
        setViewPager();

        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        View view1 = getLayoutInflater().inflate(R.layout.tab_chat, null);
        ImageView image1 = view1.findViewById(R.id.icon);
        image1.setImageResource(R.drawable.tab_ic_action_chat);

        View view2 = getLayoutInflater().inflate(R.layout.tab_chat, null);
        ImageView image2 = view2.findViewById(R.id.icon);
        image2.setImageResource(R.drawable.tab_action_ic_contact);

        tabLayout.getTabAt(0).setCustomView(view1);
        tabLayout.getTabAt(1).setCustomView(view2);

        ProfilePreferences.getInstance().setAppIsOpened(true);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                goToContactSearchActivity();
                return true;
            case R.id.create_group:
                goToCreateGroupActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        if (id == R.id.nav_profile) {
            goToProfileActivity();
        } else if (id == R.id.nav_logout) {
            askForLogout();
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        AppSocketListener.getInstance().off(Socket.EVENT_CONNECT_ERROR);
        AppSocketListener.getInstance().off(Socket.EVENT_CONNECT_TIMEOUT);
        AppSocketListener.getInstance().off(SocketEventConstants.messageReceiver);
        AppSocketListener.getInstance().off(SocketEventConstants.recentlyRegistered);
        AppSocketListener.getInstance().off(SocketEventConstants.callReceiver);

        EventBus.getDefault().unregister(this);
        ProfilePreferences.getInstance().setAppIsOpened(false);
        super.onDestroy();
    }

    @Subscribe
    public void onEvent(MazziInternetStatus internet) {
        if (internet.isConnected()) {
            internetStatusTextView.setText(null);
            internetStatusTextView.setVisibility(View.GONE);
        } else {
            internetStatusTextView.setText(getString(R.string.no_internet_connection));
            internetStatusTextView.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe
    public void onEvent(Profile profile) {
        if (!ProfilePreferences.getInstance().getImage().isEmpty()) {
            String profileImage = ProfilePreferences.getInstance().getImage().replace("thumb", "original");
            profilePicture.setPadding(0, 0, 0, 0);
            Glide.with(getApplicationContext())
                    .load(UrlConstants.getFileUrl(profileImage))
                    .apply(RequestOptions.centerCropTransform())
                    .into(profilePicture);
        }
    }

    @Subscribe
    public void onEvent(final FileDownloadStart file) {
        MazziNetwork.downloadANRequest(UrlConstants.getFileUrl(file.getUrl()), file.getFileType(), file.getFilename())
                .setDownloadProgressListener(new DownloadProgressListener() {
                    @Override
                    public void onProgress(long bytesDownloaded, long totalBytes) {
                        EventBus.getDefault().post(new FileDownloading(file.getMessageId(), file.getAdapterPosition(),
                                (int) (bytesDownloaded / (totalBytes / 100))));
                    }
                })
                .startDownload(new DownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        FileState.getInstance().put(file.getMessageId(), FileState.getInstance().setIdle());
                        FileStorage.getInstance().putFile(file.getMessageId(), file.getFilename());
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e(LOG_TAG, anError.getErrorBody());
                    }
                });
    }

    private void goToContactSearchActivity() {
        Intent intent = new Intent(getApplicationContext(), ContactSearchActivity.class);
        startActivity(intent);
    }

    private void goToCreateGroupActivity() {
        Intent intent = new Intent(getApplicationContext(), CreateGroupActivity.class);
        startActivity(intent);
    }

    private void goToProfileActivity() {
        Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
        startActivity(intent);
    }

    private void setUpViewPager() {
        FragmentPagerSupport adapter = new FragmentPagerSupport(getSupportFragmentManager());
        adapter.addFragment(new ConversationFragment());
        adapter.addFragment(new ContactFragment());
        mViewPager.setAdapter(adapter);
    }

    public void setViewPager() {
        mViewPager.setCurrentItem(0);
    }

    public void askForLogout() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.log_out))
                .setMessage(getString(R.string.ask_logout))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        leave();
                    }
                })
                .setNegativeButton(getString(R.string.no), new MyOnClickListener())
                .show();
    }

    private void leave() {
        FirebaseAuth.getInstance().signOut();
        ProfilePreferences.getInstance().clear();
        AppSocketListener.getInstance().removeHandlers();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private static class MyOnClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            // do nothing
        }
    }
}
