package com.woovoo.mazzi;

import android.app.Application;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.woovoo.mazzi.call.util.CallNetworkUtils;
import com.woovoo.mazzi.call.util.CallReceiver;
import com.woovoo.mazzi.conversation.util.Conversation;
import com.woovoo.mazzi.conversation.util.ConversationStorage;
import com.woovoo.mazzi.directory.CreateDirectoriesTask;
import com.woovoo.mazzi.sticker.util.StickerStorage;
import com.woovoo.mazzi.file.util.FileState;
import com.woovoo.mazzi.file.util.FileStorage;
import com.woovoo.mazzi.msg.util.Msg;
import com.woovoo.mazzi.contact.util.RecentlyRegistered;
import com.woovoo.mazzi.msg.util.MsgPreferences;
import com.woovoo.mazzi.contact.util.ContactStorage;
import com.woovoo.mazzi.msg.util.MsgStorage;
import com.woovoo.mazzi.socket.SocketEventConstants;
import com.woovoo.mazzi.socket.SocketListener;
import com.woovoo.mazzi.socket.AppSocketListener;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.socket.util.MazziInternetStatus;
import com.woovoo.mazzi.socket.util.OnlineStatus;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class AppContext extends Application implements SocketListener {

    private static final String LOG_TAG = "AppContext";

    @Override
    public void onCreate() {
        super.onCreate();
        new CreateDirectoriesTask().execute();
        AndroidNetworking.initialize(getApplicationContext());
        ProfilePreferences.getInstance(getApplicationContext());
        ProfilePreferences.getInstance().putDeviceId();

        CallNetworkUtils.getInstance();
        MsgPreferences.getInstance(getApplicationContext());
        MsgStorage.getInstance(getApplicationContext());
        ConversationStorage.getInstance(getApplicationContext());
        ContactStorage.getInstance(getApplicationContext());
        StickerStorage.getInstance(getApplicationContext());
        FileStorage.getInstance(getApplicationContext());
        FileState.getInstance();

        AppSocketListener.getInstance();
        AppSocketListener.getInstance().initialize(getApplicationContext());
        AppSocketListener.getInstance().setActiveSocketListener(this);
    }

    @Override
    public void onTerminate() {
        Log.d(LOG_TAG, "onTerminate");
        AppSocketListener.getInstance().destroy(getApplicationContext());
        super.onTerminate();
    }


    @Override
    public void onSocketConnected() {
        AppSocketListener.getInstance().addOnHandler(Socket.EVENT_CONNECT_ERROR, onConnectError);
        AppSocketListener.getInstance().addOnHandler(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        if (!ProfilePreferences.getInstance().isConfirmed()) {
            JSONObject verification = new JSONObject();
            try {
                verification.put("phone", ProfilePreferences.getInstance().getPhone());
                AppSocketListener.getInstance().emit(SocketEventConstants.sendVerification, verification);
                ProfilePreferences.getInstance().putConfirmed(true);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        EventBus.getDefault().post(new MazziInternetStatus(true));
    }

    @Override
    public void onSocketDisconnected() {
        EventBus.getDefault().post(new MazziInternetStatus(false));
    }

    @Override
    public void onMessageReceiver(String jsonString) {
        EventBus.getDefault().post(new Conversation(jsonString));
        EventBus.getDefault().post(new Msg(jsonString));
    }

    @Override
    public void onRecentlyRegistered(String jsonString) {
        EventBus.getDefault().post(new RecentlyRegistered(jsonString));
    }

    @Override
    public void onCallReceiver() {
        EventBus.getDefault().post(new CallReceiver());
    }

    @Override
    public void onUserStatus(String json_string) {
        OnlineStatus status = new OnlineStatus(json_string);
        ContactStorage.getInstance().setOnline(status.getUserId(), status.isConnected(), status.getOnlineAt());
        EventBus.getDefault().post(status);
    }

    private Emitter.Listener onConnectError = new MyListener();

    private static class MyListener implements Emitter.Listener {
        @Override
        public void call(Object... args) {
            Log.d(LOG_TAG, "Connection error");
        }
    }
}