package com.woovoo.mazzi.contact.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.woovoo.mazzi.R;
import com.woovoo.mazzi.contact.ContactAdapter;
import com.woovoo.mazzi.contact.util.Contact;
import com.woovoo.mazzi.contact.util.MobileContacts;

import java.util.ArrayList;

public class ContactSearchActivity extends AppCompatActivity {

    private EditText editText;
    private ContactAdapter contactAdapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        editText = findViewById(R.id.text);
        recyclerView = findViewById(R.id.contacts);
        contactAdapter = new ContactAdapter(MobileContacts.getInstance().get());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(contactAdapter);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString().toLowerCase());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            case R.id.clear:
                editText.setText("");
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.clear_search, menu);
        return true;
    }

    private void filter(String text) {
        ArrayList<Contact> result = new ArrayList<>();

        StringBuilder stringBuilder;
        for (Contact contact : MobileContacts.getInstance().get()) {

            stringBuilder = new StringBuilder();
            for (String s : contact.getPhones()) {
                stringBuilder.append(s);
                stringBuilder.append(",");
            }

            if (contact.getContactName().toLowerCase().contains(text) || stringBuilder.toString().contains(text)) {
                result.add(contact);
            }
        }
        contactAdapter = new ContactAdapter(result);
        recyclerView.setAdapter(contactAdapter);
    }
}