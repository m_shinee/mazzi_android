package com.woovoo.mazzi.contact.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class MobileContacts {

    private static final String LOG_TAG = "MobileContacts";

    private static MobileContacts instance;
    private ArrayList<Contact> mContactList;
    private ArrayList<String> params = new ArrayList<>();

    private MobileContacts() {

    }

    public static MobileContacts getInstance() {
        if (instance == null) {
            instance = new MobileContacts();
        }
        return instance;
    }

    public void fetch(Context context) {
        mContactList = new ArrayList<>();
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                null,
                null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if (cursor != null && cursor.moveToFirst()) {
            do {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                    Contact contact = new Contact();
                    contact.setContactId(id);
                    contact.setContactName(displayName);

                    Uri URI_PHONE = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                    String SELECTION_PHONE = ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?";
                    String[] SELECTION_ARRAY_PHONE = new String[]{id};
                    Cursor currPhone = contentResolver.query(URI_PHONE, null, SELECTION_PHONE, SELECTION_ARRAY_PHONE, null);

                    if (currPhone != null && currPhone.getCount() > 0) {
                        List<String> phoneMap = new ArrayList<>();
                        LinkedHashSet<String> lhs = new LinkedHashSet<>();
                        int indexPhoneNo = currPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        while (currPhone.moveToNext()) {
                            String phoneNoStr = currPhone.getString(indexPhoneNo);
                            phoneNoStr = phoneNoStr.replace("+", "");
                            phoneNoStr = phoneNoStr.replace("-", "");
                            phoneMap.add(phoneNoStr);
                            if (phoneNoStr.length() <= 8) {
                                params.add("976" + phoneNoStr);
                            } else {
                                params.add(phoneNoStr);
                            }
                        }
                        lhs.addAll(phoneMap);
                        phoneMap.clear();
                        phoneMap.addAll(lhs);
                        contact.setPhones(phoneMap);
                        mContactList.add(contact);
                        currPhone.close();
                    }

                    contact.setContactRegistered(false);
                }
            } while (cursor.moveToNext());
            cursor.close();
        }

        LinkedHashSet<String> lhs = new LinkedHashSet<>();

        lhs.addAll(params);
        params.clear();
        params.addAll(lhs);
    }

    public JSONArray getNumbers() {
        return new JSONArray(params);
    }

    public ArrayList<Contact> get() {
        return mContactList;
    }
}