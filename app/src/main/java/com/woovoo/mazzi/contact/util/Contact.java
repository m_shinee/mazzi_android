package com.woovoo.mazzi.contact.util;

import android.os.SystemClock;

import com.woovoo.mazzi.profile.util.Profile;
import com.woovoo.mazzi.util.MyDateFormat;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Contact extends Profile {

    private String contactId;
    private String contactName;
    private String image;
    private String userId;
    private String nickname;
    private String lastname;
    private String surname;
    private String region;
    private String gender;
    private String phone;
    private String email;
    private long birthday;

    private List<String> phones;

    private Boolean isRegistered = false;
    private Boolean selected = false;

    public static final Comparator<Contact> BY_REGISTERED = new Comparator<Contact>() {
        @Override
        public int compare(Contact contact, Contact t1) {
            return t1.isRegistered.compareTo(contact.isRegistered);
        }
    };

    public Contact() {
    }

    public void parseFromJSONObject(JSONObject object) throws JSONException {
        this.userId = object.getString(ID);
        this.surname = object.getString(SURNAME);
        this.nickname = object.getString(NICKNAME);
        this.lastname = object.getString(LASTNAME);
        this.image = object.getString(IMAGE);
        this.gender = object.getString(GENDER);
        this.birthday = object.has(BIRTHDAY) ? object.getLong(BIRTHDAY) : 0;
        this.phone = object.has(PHONE) ? object.getString(PHONE) : "";
        this.email = object.has(EMAIL) ? object.getString(EMAIL) : "";
        this.isRegistered = true;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public String getContactId() {
        return this.contactId;
    }

    public void setContactId(String id) {
        this.contactId = id;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String name) {
        this.contactName = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isRegistered() {
        return isRegistered;
    }

    public void setContactRegistered(boolean registered) {
        this.isRegistered = registered;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setBirthday(long birthday) {
        this.birthday = birthday;
    }

    public long getBirthday() {
        return birthday;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Boolean isSelected() {
        return this.selected;
    }

    /**********************************/
    public static class PHONE_TYPE {
        // / Phone Type
        public static final int HOME = 1;
        public static final int MOBILE = 2;
        public static final int WORK = 3;
        public static final int FAX_WORK = 4;
        public static final int FAX_HOME = 5;
        public static final int PAGER = 6;
        public static final int OTHER = 7;
    }

    @Override
    public boolean equals(Object obj) {
        boolean sameUserId = false;
        if (obj != null && obj instanceof Contact) {
            sameUserId = Objects.equals(this.userId, ((Contact) obj).userId);
        }
        return sameUserId;
    }

    @Override
    public int hashCode() {
        return this.userId == null ? -1 : this.userId.hashCode();
    }

}