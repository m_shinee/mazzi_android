package com.woovoo.mazzi.contact;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.woovoo.mazzi.profile.ui.ContactProfileActivity;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.msg.ui.MsgActivity;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.contact.util.Contact;
import com.woovoo.mazzi.util.UrlConstants;
import com.woovoo.mazzi.msg.util.MsgPreferences;
import com.woovoo.mazzi.profile.util.ProfilePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {

    private static final String TAG = "ContactAdapter";

    private List<Contact> contactList;
    private RequestOptions requestOptions = new RequestOptions()
            .transforms(new CenterCrop(), new RoundedCorners(40))
            .override(100);

    private RequestOptions requestOptionCircle = new RequestOptions()
            .centerCrop()
            .circleCrop()
            .override(100);

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView contactNameTextView, tagTextView;
        ImageView thumbnailImageView, inviteButtonImageView;

        MyViewHolder(View itemView) {
            super(itemView);
            thumbnailImageView = itemView.findViewById(R.id.thumbnailImageView);
            contactNameTextView = itemView.findViewById(R.id.contactNameTextView);
            tagTextView = itemView.findViewById(R.id.tagTextView);
            inviteButtonImageView = itemView.findViewById(R.id.inviteButtonImageView);
        }

    }

    public ContactAdapter(List<Contact> contactList) {
        this.contactList = contactList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_contacts,
                        parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Contact contact = contactList.get(position);
        if (contact.isRegistered()) {
            if (contact.getImage() == null || contact.getImage().isEmpty()) {
                Glide.with(holder.itemView.getContext())
                        .load(R.mipmap.ic_contact_white)
                        .apply(requestOptions)
                        .into(holder.thumbnailImageView);
                holder.thumbnailImageView.setBackgroundResource(R.drawable.contact_profile_picture_radius);
                holder.thumbnailImageView.setPadding(15, 15, 15, 15);
            } else {
                Glide.with(holder.itemView.getContext())
                        .load(UrlConstants.getFileUrl(contact.getImage()))
                        .apply(requestOptions)
                        .into(holder.thumbnailImageView);
                holder.thumbnailImageView.setBackground(null);
                holder.thumbnailImageView.setPadding(0, 0, 0, 0);
            }
            Glide.with(holder.itemView.getContext())
                    .load(R.mipmap.ic_logo_white)
                    .apply(requestOptionCircle)
                    .into(holder.inviteButtonImageView);
            holder.inviteButtonImageView.setBackgroundResource(R.drawable.oval_background);
            holder.tagTextView.setText(contact.getNickname());
        } else {
            Glide.with(holder.itemView.getContext())
                    .load(R.mipmap.ic_contact_white)
                    .apply(requestOptions)
                    .into(holder.thumbnailImageView);
            holder.thumbnailImageView.setBackgroundResource(R.drawable.contact_profile_picture_radius);
            holder.thumbnailImageView.setPadding(15, 15, 15, 15);
            holder.inviteButtonImageView.setImageDrawable(null);
            holder.inviteButtonImageView.setBackground(null);
            StringBuilder phones = new StringBuilder();
            for (String phone : contact.getPhones()) {
                phones.append(phone).append(", ");
            }
            holder.tagTextView.setText(phones.substring(0, phones.length() - 2));
        }

        holder.contactNameTextView.setText(contact.getContactName());
        holder.itemView.setOnClickListener(new MyOnClickListener(contact));
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    private static class MyOnClickListener implements View.OnClickListener {
        private Contact mContact;

        MyOnClickListener(Contact contact) {
            this.mContact = contact;
        }

        @Override
        public void onClick(final View view) {
            if (mContact.isRegistered()) {
                Intent intent = new Intent(view.getContext(), ContactProfileActivity.class);
                intent.putExtra("user_id", mContact.getUserId());
                view.getContext().startActivity(intent);
            } else {
                Uri uri = Uri.parse("smsto:" + mContact.getPhones().get(0));
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.putExtra("sms_body", "Hey, I'm using Mazzi to chat. Join me! Download it");
                view.getContext().startActivity(intent);
            }
        }
    }
}