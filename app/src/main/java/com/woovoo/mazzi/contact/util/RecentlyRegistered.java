package com.woovoo.mazzi.contact.util;

import com.woovoo.mazzi.profile.util.Profile;
import com.woovoo.mazzi.profile.util.ProfilePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RecentlyRegistered extends Profile {

    private static final String LOG_TAG = "RecentlyRegistered";

    /**
     * key @params (Not socket)
     */
    private static final String CONTACT_NAME = "contact_name";
    private static final String CONVERSATION_ID = "conversation_id";

    /**
     * Socket @params
     */
    private String mOwnerId = "";
    private String mNickname;
    private String mPhone;

    /**
     * Not socket @params
     */
    private String mContactName;
    private String mConversationId;
    private JSONArray mParticipants = new JSONArray();
    private JSONArray mShow;

    public RecentlyRegistered() {
    }

    /**
     * @param string Parse JSONString
     */
    public RecentlyRegistered(String string) {
        JSONObject object;
        try {
            object = new JSONObject(string);
            setOwnerId(object.getString(ID));
            setNickname(object.getString(NICKNAME));
            setPhone(object.getString(PHONE));

            setContactName(object.getString(CONTACT_NAME));
            setConversationId(object.getString(CONVERSATION_ID));

            mParticipants.put(getOwnerId());
            mShow = getParticipants();
            mShow.put(ProfilePreferences.getInstance().getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setOwnerId(String mOwnerId) {
        this.mOwnerId = mOwnerId;
    }

    public void setNickname(String mNickname) {
        this.mNickname = mNickname;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public void setContactName(String mContactName) {
        this.mContactName = mContactName;
    }

    public void setConversationId(String mConversationId) {
        this.mConversationId = mConversationId;
    }

    public void setParticipants(JSONArray mParticipants) {
        this.mParticipants = mParticipants;
    }

    public void setShow(JSONArray mShow) {
        this.mShow = mShow;
    }

    public String getOwnerId() {
        return mOwnerId;
    }

    public String getNickname() {
        return mNickname;
    }

    public String getPhone() {
        return mPhone;
    }

    public String getContactName() {
        return mContactName;
    }

    public String getConversationId() {
        return mConversationId;
    }

    public JSONArray getParticipants() {
        return mParticipants;
    }

    public JSONArray getShow() {
        return mShow;
    }
}