package com.woovoo.mazzi.contact;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.contact.util.Contact;
import com.woovoo.mazzi.util.UrlConstants;

import java.util.ArrayList;

public class SelectedContactsAdapter extends RecyclerView.Adapter<SelectedContactsAdapter.MyViewHolder> {

    private ArrayList<Contact> mContactList = new ArrayList<>();
    private RequestOptions requestOptions = new RequestOptions()
            .transforms(new CenterCrop(), new RoundedCorners(40))
            .override(100, 100);

    private final OnItemClickListener mListener;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_users, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Contact contact = mContactList.get(position);
        holder.nickname.setText(contact.getContactName().length() > 5 ? contact.getContactName().substring(0, 5) : contact.getContactName());
        if (contact.getImage().isEmpty()) {
            Glide.with(holder.itemView.getContext())
                    .load(R.mipmap.ic_contact_white)
                    .apply(requestOptions)
                    .into(holder.thumbnail);
            holder.thumbnail.setBackgroundResource(R.drawable.contact_profile_picture_radius);
            holder.thumbnail.setPadding(15, 15, 15, 15);
        } else {
            Glide.with(holder.itemView.getContext())
                    .load(UrlConstants.getFileUrl(contact.getImage()))
                    .apply(requestOptions)
                    .into(holder.thumbnail);
            holder.thumbnail.setBackground(null);
            holder.thumbnail.setPadding(0, 0, 0, 0);
        }

        holder.bind(contact, mListener);
    }

    @Override
    public int getItemCount() {
        return mContactList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView thumbnail;
        TextView nickname;

        MyViewHolder(View itemView) {
            super(itemView);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            nickname = itemView.findViewById(R.id.nickname);
        }

        void bind(final Contact contact, final OnItemClickListener listener) {
            itemView.setOnClickListener(new MyOnClickListener(listener, contact));
        }

        private static class MyOnClickListener implements View.OnClickListener {
            private final OnItemClickListener listener;
            private final Contact contact;

            MyOnClickListener(OnItemClickListener listener, Contact contact) {
                this.listener = listener;
                this.contact = contact;
            }

            @Override
            public void onClick(View view) {
                listener.onItemClick(contact);
            }
        }
    }

    public SelectedContactsAdapter(ArrayList<Contact> contacts, OnItemClickListener listener) {
        this.mContactList = contacts;
        mListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(Contact contact);
    }
}
