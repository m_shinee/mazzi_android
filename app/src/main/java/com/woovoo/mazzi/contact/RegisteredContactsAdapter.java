package com.woovoo.mazzi.contact;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.contact.util.Contact;
import com.woovoo.mazzi.util.UrlConstants;

import java.util.ArrayList;

public class RegisteredContactsAdapter extends RecyclerView.Adapter<RegisteredContactsAdapter.MyViewHolder> {

    private ArrayList<Contact> mContactList;
    private RequestOptions mRequestOptions = new RequestOptions()
            .transforms(new CenterCrop(), new RoundedCorners(40))
            .override(100, 100);
    private final OnItemClickListener mListener;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.registered_contact_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Contact contact = mContactList.get(position);
        if (contact.getImage() == null || contact.getImage().isEmpty()) {
            Glide.with(holder.itemView.getContext())
                    .load(R.mipmap.ic_contact_white)
                    .apply(mRequestOptions)
                    .into(holder.thumbnail);
            holder.thumbnail.setBackgroundResource(R.drawable.contact_profile_picture_radius);
            holder.thumbnail.setPadding(15, 15, 15, 15);
        } else {
            Glide.with(holder.itemView.getContext())
                    .load(UrlConstants.getFileUrl(contact.getImage()))
                    .apply(mRequestOptions)
                    .into(holder.thumbnail);
            holder.thumbnail.setBackground(null);
            holder.thumbnail.setPadding(0, 0, 0, 0);
        }
        holder.fullname.setText(contact.getContactName());

        if (contact.isSelected()) {
            holder.selected_contact.setVisibility(View.VISIBLE);
        } else {
            holder.selected_contact.setVisibility(View.GONE);
        }

        holder.bind(mContactList.get(position), mListener);
    }

    @Override
    public int getItemCount() {
        return mContactList.size();
    }

    public RegisteredContactsAdapter(ArrayList<Contact> contactList, OnItemClickListener listener) {
        this.mContactList = contactList;
        mListener = listener;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView fullname;
        ImageView thumbnail, selected_contact;

        MyViewHolder(View itemView) {
            super(itemView);

            fullname = itemView.findViewById(R.id.contact_name);
            thumbnail = itemView.findViewById(R.id.thumbnail);
            selected_contact = itemView.findViewById(R.id.selected_contact);
        }

        void bind(final Contact contact, final OnItemClickListener listener) {
            itemView.setOnClickListener(new MyOnClickListener(listener, contact));
        }

        private static class MyOnClickListener implements View.OnClickListener {
            private final OnItemClickListener listener;
            private final Contact contact;

            MyOnClickListener(OnItemClickListener listener, Contact contact) {
                this.listener = listener;
                this.contact = contact;
            }

            @Override
            public void onClick(View view) {
                listener.onItemClick(contact);
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Contact contact);
    }
}
