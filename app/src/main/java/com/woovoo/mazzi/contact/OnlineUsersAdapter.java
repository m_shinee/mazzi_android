package com.woovoo.mazzi.contact;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.UrlConstants;
import com.woovoo.mazzi.contact.util.Contact;

import java.util.List;

public class OnlineUsersAdapter extends RecyclerView.Adapter<OnlineUsersAdapter.MyViewHolder> {

    private static final String LOG_TAG = "OnlineUsersAdapter";

    private List<Contact> contactList;
    private RequestOptions requestOptions = new RequestOptions()
            .transforms(new CenterCrop(), new RoundedCorners(40))
            .override(100);

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_online_users, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Contact contact = contactList.get(position);
        holder.nicknameTextView.setText(contact.getNickname());
        Glide.with(holder.itemView.getContext())
                .load(UrlConstants.getFileUrl(contact.getImage()))
                .apply(requestOptions)
                .into(holder.thumbnailImageView);
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView nicknameTextView;
        ImageView thumbnailImageView;

        MyViewHolder(View itemView) {
            super(itemView);
            nicknameTextView = itemView.findViewById(R.id.nicknameTextView);
            thumbnailImageView = itemView.findViewById(R.id.thumbnailImageView);
        }
    }

    public OnlineUsersAdapter(List<Contact> contactList) {
        this.contactList = contactList;
    }
}
