package com.woovoo.mazzi.contact.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.profile.util.Profile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class ContactStorage extends Profile {

    private static final String LOG_TAG = "participants";
    private static ContactStorage instance;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    public static ContactStorage getInstance(Context context) {
        if (instance == null) {
            instance = new ContactStorage(context.getApplicationContext());
        }
        return instance;
    }

    public static ContactStorage getInstance() {
        if (instance != null)
            return instance;

        throw new IllegalArgumentException("Should use getInstance(Context) at least once before using this method.");
    }

    public void putParticipant(JSONObject object) {
        try {
            String id = object.getString(ID);
            object.remove(ID);
            doEdit();
            editor.putString(id, object.toString());
            doApply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getImage(String participantId) {
        String image = "";
        try {
            JSONObject object = new JSONObject(preferences.getString(participantId, ""));
            image = object.has(IMAGE) ? object.getString(IMAGE) : "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return image;
    }

    public String getNickname(String participantId) {
        String nickname = null;
        try {
            JSONObject object = new JSONObject(preferences.getString(participantId, ""));
            nickname = object.getString(NICKNAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return nickname;
    }

    public String getSurname(String participantId) {
        String surname = null;
        try {
            JSONObject object = new JSONObject(preferences.getString(participantId, ""));
            surname = object.getString(SURNAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return surname;
    }

    public String getLastname(String participantId) {
        String lastname = null;
        try {
            JSONObject object = new JSONObject(preferences.getString(participantId, null));
            lastname = object.getString(LASTNAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return lastname;
    }

    public String getPhone(String participantId) {
        String phone = null;
        try {
            JSONObject object = new JSONObject(preferences.getString(participantId, null));
            phone = object.getString(PHONE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return phone;
    }

    public int getState(String participantId) {
        int state = 0;
        try {
            JSONObject object = new JSONObject(preferences.getString(participantId, null));
            state = object.getInt(STATE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return state;
    }

    public long getOnlineAt(String participantId) {
        long onlineAt = MyDateFormat.currentTime();
        try {
            JSONObject object = new JSONObject(preferences.getString(participantId, null));
            onlineAt = object.getLong(ONLINE_AT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return onlineAt;
    }

    public boolean exist(String key) {
        return preferences.contains(key);
    }

    public void setOnline(String participantId, boolean isOnline, long onlineAt) {
        if (exist(participantId)) {
            try {
                doEdit();
                JSONObject object = new JSONObject(preferences.getString(participantId, null));
                object.put(STATE, isOnline ? 1 : 0);
                object.put(ONLINE_AT, onlineAt);
                editor.putString(participantId, object.toString());
                doApply();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public Map<String, ?> getAll() {
        return preferences.getAll();
    }

    @SuppressLint("CommitPrefEdits")
    private void doEdit() {
        editor = preferences.edit();
    }

    private void doApply() {
        if (editor != null) {
            editor.apply();
            editor = null;
        }
    }

    public void clearAll() {
        doEdit();
        editor.clear();
        doApply();
    }

    private ContactStorage(Context context) {
        preferences = context.getSharedPreferences(LOG_TAG, Context.MODE_PRIVATE);
    }
}
