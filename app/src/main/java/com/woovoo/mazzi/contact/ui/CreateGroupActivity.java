package com.woovoo.mazzi.contact.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.woovoo.mazzi.camera.ui.PhotoKitActivity;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.util.MyDateFormat;
import com.woovoo.mazzi.contact.SelectedContactsAdapter;
import com.woovoo.mazzi.contact.RegisteredContactsAdapter;
import com.woovoo.mazzi.contact.util.Contact;
import com.woovoo.mazzi.contact.util.MobileContacts;
import com.woovoo.mazzi.conversation.util.Conversation;
import com.woovoo.mazzi.msg.ui.MsgActivity;
import com.woovoo.mazzi.msg.util.MsgPreferences;
import com.woovoo.mazzi.profile.util.ProfilePreferences;
import com.woovoo.mazzi.util.UrlConstants;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class CreateGroupActivity extends AppCompatActivity {

    private static final int START_CAMERA_APP = 852;
    private static final String TAG = "CreateGroupActivity";
    private String conversationImage = "", conversationName = "";
    private JSONArray participants = new JSONArray();

    private EditText editText, conversationNameButton;
    private ImageButton createButton;
    private ImageView conversationImageView;
    private RegisteredContactsAdapter registeredContactsAdapter;
    private SelectedContactsAdapter selectedContactsAdapter;
    private RecyclerView registeredContactsRecyclerView;
    private LinearLayout selectedContactView;

    private ArrayList<Contact> originalTemp = new ArrayList<>();
    private ArrayList<Contact> temp = new ArrayList<>();
    private ArrayList<Contact> selectedContacts = new ArrayList<>();

    private RequestOptions conversationImageBorder = new RequestOptions()
            .transforms(new CenterCrop(), new RoundedCorners(40))
            .override(100, 100);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        registeredContactsRecyclerView = findViewById(R.id.registered_contacts);
        RecyclerView selectedContactsRecyclerView = findViewById(R.id.selected_contacts);
        editText = findViewById(R.id.text);
        conversationNameButton = findViewById(R.id.conversation_name);
        selectedContactView = findViewById(R.id.selected_contacts_view);
        createButton = findViewById(R.id.create_button);
        conversationImageView = findViewById(R.id.conversation_picture);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        for (Contact contact : MobileContacts.getInstance().get()) {
            if (contact.isRegistered()) {
                contact.setSelected(false);
                originalTemp.add(contact);
            }
        }

        temp = originalTemp;

        initializeRegisteredContactsAdapters();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        registeredContactsRecyclerView.setLayoutManager(layoutManager);

        selectedContactsAdapter = new SelectedContactsAdapter(selectedContacts, new SelectedContactsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Contact contact) {
                contact.setSelected(false);
                Contact c = new Contact();
                c.setUserId(contact.getUserId());
                int index = selectedContacts.indexOf(c);
                if (index != -1) {
                    selectedContacts.remove(index);
                }
                if (selectedContacts.size() == 0)
                    selectedContactView.setVisibility(View.GONE);
                registeredContactsAdapter.notifyDataSetChanged();
                selectedContactsAdapter.notifyDataSetChanged();
                changeCreateButtonSrc();
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        selectedContactsRecyclerView.setLayoutManager(linearLayoutManager);
        selectedContactsRecyclerView.setAdapter(selectedContactsAdapter);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filter(editable.toString());
            }
        });

        conversationNameButton.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                conversationName = conversationNameButton.getText().toString();
                changeCreateButtonSrc();
            }
        });

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidation()) {
                    participants = new JSONArray();
                    participants.put(ProfilePreferences.getInstance().getId());
                    for (Contact contact : selectedContacts) {
                        participants.put(contact.getUserId());
                    }
                    JSONObject params = new JSONObject();
                    try {
                        params.put("participants", participants);
                        params.put("title", conversationName);
                        params.put("image", conversationImage);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    MazziNetwork.postANRequest(UrlConstants.createGroupConversation(), params)
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Conversation conversation = new Conversation(response);

                                    participants.remove(0);
                                    MsgPreferences.getInstance().setParticipants(participants);
                                    MsgPreferences.getInstance().setSeen(true);
                                    MsgPreferences.getInstance().setId("");
                                    MsgPreferences.getInstance().setConversationId(conversation.getConversationId());
                                    MsgPreferences.getInstance().setCreatedAt(MyDateFormat.currentTime());

                                    EventBus.getDefault().post(conversation);

                                    Intent intent = new Intent(getApplicationContext(), MsgActivity.class);
                                    intent.putExtra("title", conversationName);
                                    startActivity(intent);
                                    finish();
                                }

                                @Override
                                public void onError(ANError anError) {
                                    Log.d(TAG, anError.getLocalizedMessage());
                                }
                            });
                }
            }
        });

        conversationImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PhotoKitActivity.class);
                startActivityForResult(intent, START_CAMERA_APP);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == START_CAMERA_APP && resultCode == RESULT_OK) {
            uploadImage(data.getStringExtra("filepath"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            case R.id.clear:
                editText.setText("");
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.clear_search, menu);
        return true;
    }

    private void initializeRegisteredContactsAdapters() {
        registeredContactsAdapter = new RegisteredContactsAdapter(temp, new RegisteredContactsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Contact contact) {
                if (!contact.isSelected()) {
                    contact.setSelected(true);
                    selectedContactView.setVisibility(View.VISIBLE);
                    selectedContacts.add(0, contact);
                    selectedContactsAdapter.notifyDataSetChanged();
                } else {
                    contact.setSelected(false);
                    Contact c = new Contact();
                    c.setUserId(contact.getUserId());
                    int index = selectedContacts.indexOf(c);
                    if (index != -1) {
                        selectedContacts.remove(index);
                    }
                    if (selectedContacts.size() == 0)
                        selectedContactView.setVisibility(View.GONE);
                    selectedContactsAdapter.notifyDataSetChanged();
                }
                registeredContactsAdapter.notifyDataSetChanged();
                changeCreateButtonSrc();
            }
        });
        registeredContactsRecyclerView.setAdapter(registeredContactsAdapter);
    }

    private boolean checkValidation() {
        return conversationName.trim().length() != 0 && selectedContacts.size() > 1;
    }

    private void changeCreateButtonSrc() {
        if (checkValidation()) {
            createButton.setImageResource(R.mipmap.ic_action_send);
        } else {
            createButton.setImageResource(R.mipmap.ic_action_send_gray);
        }
    }

    private void filter(String text) {
        temp = new ArrayList<>();

        StringBuilder stringBuilder;
        for (Contact contact : originalTemp) {
            stringBuilder = new StringBuilder();
            for (String s : contact.getPhones()) {
                stringBuilder.append(s);
                stringBuilder.append(",");
            }

            if (contact.getContactName().toLowerCase().contains(text) || stringBuilder.toString().contains(text)) {
                temp.add(contact);
            }
        }
        initializeRegisteredContactsAdapters();
        registeredContactsAdapter.notifyDataSetChanged();
    }

    private void uploadImage(String filePath) {
        AndroidNetworking.upload(UrlConstants.uploadImage())
                .addMultipartFile("file", new File(filePath))
                .addMultipartParameter("conversation_id", ProfilePreferences.getInstance().getId())
                .setTag(TAG)
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        Log.d(TAG, String.valueOf(bytesUploaded));
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            conversationImage = response.getString("url").replace("\\", "");
                            Glide.with(getApplicationContext())
                                    .load(UrlConstants.getFileUrl(conversationImage))
                                    .apply(conversationImageBorder)
                                    .into(conversationImageView);
                            conversationImageView.setPadding(0, 0, 0, 0);
                            conversationImageView.setBackground(null);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.d(TAG, anError.getLocalizedMessage());
                    }
                });
    }
}