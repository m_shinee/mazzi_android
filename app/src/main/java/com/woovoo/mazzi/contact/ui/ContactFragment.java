package com.woovoo.mazzi.contact.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.contact.ContactAdapter;
import com.woovoo.mazzi.contact.util.Contact;
import com.woovoo.mazzi.contact.util.MobileContacts;
import com.woovoo.mazzi.contact.util.ContactStorage;
import com.woovoo.mazzi.util.UrlConstants;
import com.woovoo.mazzi.contact.util.RecentlyRegistered;
import com.woovoo.mazzi.socket.util.OnlineStatus;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class ContactFragment extends Fragment {

    private static final String LOG_TAG = "ContactFragment";

    private ContactAdapter contactAdapter;

    private RecyclerView contactRecyclerView;

    private List<Contact> onlineUserList;

    public ContactFragment() {
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        MobileContacts.getInstance().fetch(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        contactRecyclerView = view.findViewById(R.id.contactRecyclerView);

        JSONObject params = new JSONObject();
        try {
            params.put("phone_numbers", MobileContacts.getInstance().getNumbers());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        MazziNetwork.postANRequest(UrlConstants.getRegisteredUsers(), params)
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray mRegisteredContacts = response.getJSONArray("result");
                            for (Contact contact : MobileContacts.getInstance().get()) {
                                if (mRegisteredContacts.length() == 0) {
                                    break;
                                }
                                for (int i = 0; i < mRegisteredContacts.length(); i++) {
                                    JSONObject info = mRegisteredContacts.getJSONObject(i);
                                    for (String phone : contact.getPhones()) {
                                        if (info.getString("phone").contains(phone)) {
                                            contact.parseFromJSONObject(info);
                                            ContactStorage.getInstance().putParticipant(info);
                                            mRegisteredContacts.remove(i);
                                            break;
                                        }
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            initializeContactRecyclerView();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        initializeContactRecyclerView();
                    }
                });


        return view;
    }

    @Subscribe
    public void onEvent(RecentlyRegistered recentlyRegistered) {
        StringBuilder stringBuilder;
        for (Contact contact : MobileContacts.getInstance().get()) {
            stringBuilder = new StringBuilder();
            for (String s : contact.getPhones()) {
                stringBuilder.append(s);
                stringBuilder.append(",");
            }

            if (stringBuilder.toString().contains(recentlyRegistered.getPhone()) || recentlyRegistered.getPhone().contains(stringBuilder)) {
                contact.setUserId(recentlyRegistered.getOwnerId());
                contact.setNickname(recentlyRegistered.getNickname());
                contact.setContactRegistered(true);
            }
        }
        contactAdapter.notifyDataSetChanged();
    }

    @Subscribe
    public void onEvent(OnlineStatus status) {
        Log.e(LOG_TAG, "OnlineStatus");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void initializeContactRecyclerView() {
        Collections.sort(MobileContacts.getInstance().get(), Contact.BY_REGISTERED);
        contactAdapter = new ContactAdapter(MobileContacts.getInstance().get());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        contactRecyclerView.setLayoutManager(layoutManager);
        contactRecyclerView.setItemAnimator(new DefaultItemAnimator());
        contactRecyclerView.setAdapter(contactAdapter);
    }
}