package com.woovoo.mazzi.socket;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.LocalBroadcastManager;

import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.ANResponse;
import com.woovoo.mazzi.call.util.CallNetworkUtils;
import com.woovoo.mazzi.R;
import com.woovoo.mazzi.call.ui.IncomingActivity;
import com.woovoo.mazzi.WelcomeActivity;
import com.woovoo.mazzi.util.MazziNetwork;
import com.woovoo.mazzi.contact.util.Contact;
import com.woovoo.mazzi.contact.util.MobileContacts;
import com.woovoo.mazzi.util.UrlConstants;
import com.woovoo.mazzi.msg.util.MsgPreferences;
import com.woovoo.mazzi.contact.util.ContactStorage;
import com.woovoo.mazzi.profile.util.ProfilePreferences;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Process;
import android.util.Log;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.Transport;

public class SocketBackgroundService extends Service {

    private static final String LOG_TAG = "SocketBackgroundService";

    private static final int CALL_REQUEST = 8000;

    private boolean serviceBind = false;
    private final LocalBinder mBinder = new LocalBinder();
    private Socket mSocket;
    private CallNetworkUtils callNetworkUtils;

    @Override
    public void onCreate() {
        super.onCreate();
        callNetworkUtils = CallNetworkUtils.getInstance();
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        initializeSocket();
        addSocketHandlers();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return serviceBind;
    }

    class LocalBinder extends Binder {
        SocketBackgroundService getService() {
            return SocketBackgroundService.this;
        }
    }

    public void setServiceBind(boolean serviceBind) {
        this.serviceBind = serviceBind;
    }

    public void showMessageNotifications(JSONObject datas) throws JSONException {
        if (!ProfilePreferences.getInstance().appIsOpened()) {
            String title = ContactStorage.getInstance().getNickname(datas.getString(MsgPreferences.OWNER_ID));
            String message = "";
            switch (datas.getString(MsgPreferences.MESSAGE_TYPE)) {
                case "text":
                    message = datas.getString(MsgPreferences.CONTENT);
                    break;
                case "photo":
                    message = getString(R.string.send_image);
                    break;
                case "video":
                    message = getString(R.string.send_video);
                    break;
                case "audio":
                    message = getString(R.string.send_audio);
                    break;
                case "call":
                    message = getString(R.string.send_call);
                    break;
                case "sticker":
                    message = getString(R.string.sticker);
                    break;
                case "document":
                    message = getString(R.string.file_sent);
                    break;
            }
            Intent toLaunch;
            toLaunch = new Intent(getApplicationContext(), WelcomeActivity.class);
            toLaunch.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            sendNotification(title, message, toLaunch);
        }
    }

    public void showNewParticipantNotifications(String title) throws JSONException {
        Intent toLaunch;
        toLaunch = new Intent(getApplicationContext(), WelcomeActivity.class);
        sendNotification(title, getString(R.string.registered_to_mazzi), toLaunch);
    }

    public void sendNotification(String title, String message, Intent toLaunch) {
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                toLaunch,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification n = new Notification.Builder(getApplicationContext())
                .setColor(getResources().getColor(R.color.colorPrimary))
                .setSmallIcon(R.mipmap.ic_logo_white)
                .setContentTitle(title)
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancelAll();
            notificationManager.notify(0, n);
        }
    }

    public void emit(String event, Object[] args, Ack ack) {
        mSocket.emit(event, args, ack);
    }

    public void emit(String event, Object... args) {
        try {
            mSocket.emit(event, args, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void off(String event) {
        mSocket.off(event);
    }

    public boolean isSocketConnected() {
        return mSocket != null && mSocket.connected();
    }

    public void connect() {
        mSocket.connect();
    }

    public void disconnect() {
        mSocket.disconnect();
    }

    public void restartSocket() {
        mSocket.off();
        mSocket.disconnect();
        initializeSocket();
        addSocketHandlers();
    }

    public void addOnHandler(String event, Emitter.Listener listener) {
        mSocket.on(event, listener);
    }

    public void removeMessageReceiveHandler() {
        mSocket.off(SocketEventConstants.messageReceiver);
    }

    public void removeVideoCallReceiverHandler() {
        mSocket.off(SocketEventConstants.callReceiver);
    }

    public void removeRecentlyRegistererHandler() {
        mSocket.off(SocketEventConstants.recentlyRegistered);
    }

    public void removeUserStatusHandler() {
        mSocket.off(SocketEventConstants.userStatus);
    }

    public void addMessageReceiveHandler() {
        mSocket.off(SocketEventConstants.messageReceiver);
        mSocket.on(SocketEventConstants.messageReceiver, new Emitter.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void call(Object... args) {
                JSONObject object = (JSONObject) args[0];
                try {
                    showMessageNotifications(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(SocketEventConstants.messageReceiver);
                intent.putExtra("json_string", object.toString());
                broadcastEvent(intent);
            }
        });
    }

    public void addRecentlyRegistered() {
        mSocket.off(SocketEventConstants.recentlyRegistered);
        mSocket.on(SocketEventConstants.recentlyRegistered, new Emitter.Listener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void call(Object... args) {
                if (ProfilePreferences.getInstance().getId() == null) {
                    return;
                }

                JSONObject data = (JSONObject) args[0];
                try {
                    String contactName = getContactName(data.getString("phone"));
                    if (!ProfilePreferences.getInstance().getId().equals(data.getString("_id")) && contactName != null) {
                        data.put("contact_name", contactName);
                        sendRequest(data);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void addVideoCallReceiverHandler() {
        mSocket.off(SocketEventConstants.callReceiver);
        mSocket.on(SocketEventConstants.callReceiver, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject object = (JSONObject) args[0];
                callNetworkUtils.parseJSONObjectToSetter(object);

                if (callNetworkUtils.isIncoming()) {
                    if (ProfilePreferences.getInstance().appIsOpened()) {
                        startIncomingActivity();
                    } else {
                        startForegroundIncomingActivity();
                    }
                    return;
                }

                if (!callNetworkUtils.isIncoming()) {
                    Intent intent = new Intent(SocketEventConstants.callReceiver);
                    broadcastEvent(intent);
                }
            }
        });
    }

    public void addUserStatusHandler() {
        mSocket.off(SocketEventConstants.userStatus);
        mSocket.on(SocketEventConstants.userStatus, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject object = (JSONObject) args[0];
                Intent intent = new Intent(SocketEventConstants.userStatus);
                intent.putExtra("json_string", object.toString());
                broadcastEvent(intent);
            }
        });
    }

    public String getContactName(final String phoneNumber) {
        String contactName = null;
        for (Contact contact : MobileContacts.getInstance().get()) {
            for (String phone : contact.getPhones()) {
                if (phone.contains(phoneNumber)) {
                    contact.setContactRegistered(true);
                    contactName = contact.getContactName();
                    break;
                }
            }
            if (contactName != null) {
                contactName = "";
                break;
            }
        }
        return contactName;
    }

    private void broadcastEvent(Intent intent) {
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void initializeSocket() {
        try {
            IO.Options options = new IO.Options();
            options.forceNew = true;
            options.query = "userId=" + ProfilePreferences.getInstance().getId() + "&device_id=" + ProfilePreferences.getInstance().getDeviceId();
            mSocket = IO.socket(UrlConstants.CHAT_SERVER_URL, options);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void addSocketHandlers() {
        mSocket.io().on(Manager.EVENT_TRANSPORT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Transport transport = (Transport) args[0];

                transport.on(Transport.EVENT_REQUEST_HEADERS, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        @SuppressWarnings("unchecked")
                        Map<String, List<String>> headers = (Map<String, List<String>>) args[0];
                        // send cookie value to server.
                        headers.put("authorization", Collections.singletonList("BPXjEvWfXxx5qpQHHFMEJEB8fA3xAczC"));
                    }
                }).on(Transport.EVENT_RESPONSE_HEADERS, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        // i don't know
                    }
                });
            }
        });
        mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Intent intent = new Intent(SocketEventConstants.onConnect);
                intent.putExtra("connected", true);
                broadcastEvent(intent);
            }
        });
        mSocket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Intent intent = new Intent(SocketEventConstants.onDisconnect);
                intent.putExtra("connected", false);
                broadcastEvent(intent);
            }
        });
        mSocket.on(Socket.EVENT_CONNECT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Intent intent = new Intent(SocketEventConstants.onConnectError);
                broadcastEvent(intent);
            }
        });
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Intent intent = new Intent(SocketEventConstants.onConnectError);
                broadcastEvent(intent);
            }
        });
        if (ProfilePreferences.getInstance().getId() != null) {
            addMessageReceiveHandler();
            addRecentlyRegistered();
            addVideoCallReceiverHandler();
            addUserStatusHandler();
        }
        mSocket.connect();
    }

    private void sendRequest(JSONObject datas) throws JSONException {
        JSONObject params = new JSONObject();
        params.put("user_id", ProfilePreferences.getInstance().getId());
        params.put("participant_id", datas.getString("_id"));

        ANRequest request = MazziNetwork.postANRequest(UrlConstants.createPrivateConversation(), params);
        ANResponse response = request.executeForJSONObject();

        if (response.isSuccess()) {
            JSONObject object = (JSONObject) response.getResult();
            datas.put("conversation_id", object.getString("_id"));

            showNewParticipantNotifications(datas.getString("contact_name"));

            Intent intent = new Intent(SocketEventConstants.recentlyRegistered);
            intent.putExtra("json_string", datas.toString());
            broadcastEvent(intent);

            ContactStorage.getInstance().putParticipant(datas);
        } else {
            Log.e(LOG_TAG, response.getError().toString());
        }
    }

    private void startIncomingActivity() {
        Intent intent = new Intent(getApplicationContext(), IncomingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void startForegroundIncomingActivity() {
        Intent intent = new Intent(getApplicationContext(), IncomingActivity.class);
        intent.setFlags(Intent.FLAG_RECEIVER_FOREGROUND);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, CALL_REQUEST, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        try {
            pendingIntent.send();
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }
    }

}