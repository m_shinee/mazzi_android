package com.woovoo.mazzi.socket;

public interface SocketListener {
    void onSocketConnected();

    void onSocketDisconnected();

    void onMessageReceiver(String json_string);

    void onRecentlyRegistered(String json_string);

    void onCallReceiver();

    void onUserStatus(String json_string);
}
