package com.woovoo.mazzi.socket;

public class SocketEventConstants {
    static String onConnect = "onConnect";
    static String onDisconnect = "onDisconnect";
    static String onConnectError = "onConnectError";
    public static String messageReceiver = "receive_message";
    public static String messageSend = "send_message";
    public static String messageSeen = "mark_read";
    public static String messageTyping = "typing";
    public static String messageTypingReceiver = "typing_receive";
    public static String sendVerification = "verification";
    public static String recentlyRegistered = "joined";
    public static String sentMessage = "sent_message";
    public static String userStatus = "user_status";

    /**
     * Params
     * int 1 = calling
     * int 2 = reject
     * int 3 = busy
     * int 4 = answered
     */
    public static String call = "send_videocall";
    public static String callReceiver = "receive_videocall";
}