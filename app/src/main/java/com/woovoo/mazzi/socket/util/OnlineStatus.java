package com.woovoo.mazzi.socket.util;

import org.json.JSONException;
import org.json.JSONObject;

public class OnlineStatus {

    private static final String IS_CONNECTED = "is_connected";
    private static final String USER_ID = "user_id";
    private static final String ONLINE_AT = "online_at";

    private boolean connected;
    private String userId;
    private long onlineAt;

    public OnlineStatus(String jsonString) {
        try {
            JSONObject object = new JSONObject(jsonString);
            this.connected = object.getBoolean(IS_CONNECTED);
            this.userId = object.getString(USER_ID);
            this.onlineAt = object.getLong(ONLINE_AT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return connected;
    }

    public String getUserId() {
        return userId;
    }

    public long getOnlineAt() {
        return onlineAt;
    }
}
