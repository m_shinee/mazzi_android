package com.woovoo.mazzi.socket;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import io.socket.client.Ack;
import io.socket.emitter.Emitter;

public class AppSocketListener implements SocketListener {

    private static AppSocketListener sharedInstance;
    private SocketBackgroundService socketBackgroundService;
    private SocketListener activeSocketListener;

    public void setActiveSocketListener(SocketListener listener) {
        this.activeSocketListener = listener;
        if (socketBackgroundService != null && socketBackgroundService.isSocketConnected()) {
            onSocketConnected();
        }
    }

    public static AppSocketListener getInstance() {
        if (sharedInstance == null) {
            sharedInstance = new AppSocketListener();
        }
        return sharedInstance;
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            socketBackgroundService = ((SocketBackgroundService.LocalBinder) service).getService();
            socketBackgroundService.setServiceBind(true);
            if (socketBackgroundService.isSocketConnected()) {
                onSocketConnected();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            socketBackgroundService.setServiceBind(false);
            socketBackgroundService = null;
            onSocketDisconnected();
        }
    };

    public void initialize(Context context) {
        Intent intent = new Intent(context, SocketBackgroundService.class);
        context.startService(intent);
        context.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(context).registerReceiver(socketConnectReceiver, new IntentFilter(SocketEventConstants.onConnect));
        LocalBroadcastManager.getInstance(context).registerReceiver(socketConnectErrorReceiver, new IntentFilter(SocketEventConstants.onConnectError));
        LocalBroadcastManager.getInstance(context).registerReceiver(messageReceiver, new IntentFilter(SocketEventConstants.messageReceiver));
        LocalBroadcastManager.getInstance(context).registerReceiver(recentlyRegistered, new IntentFilter(SocketEventConstants.recentlyRegistered));
        LocalBroadcastManager.getInstance(context).registerReceiver(callReceiver, new IntentFilter(SocketEventConstants.callReceiver));
        LocalBroadcastManager.getInstance(context).registerReceiver(userStatus, new IntentFilter(SocketEventConstants.userStatus));
    }

    private BroadcastReceiver socketConnectReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean isConnected = intent.getBooleanExtra("connected", false);
            if (isConnected) {
                onSocketConnected();
            } else {
                onSocketDisconnected();
            }
        }
    };

    private BroadcastReceiver socketConnectErrorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onSocketDisconnected();
        }
    };

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                onMessageReceiver(intent.getStringExtra("json_string"));
            }
        }
    };

    private BroadcastReceiver recentlyRegistered = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                onRecentlyRegistered(intent.getStringExtra("json_string"));
            }
        }
    };

    private BroadcastReceiver callReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onCallReceiver();
        }
    };

    private BroadcastReceiver userStatus = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getExtras() != null) {
                onUserStatus(intent.getStringExtra("json_string"));
            }
        }
    };

    public void destroy(Context context) {
        socketBackgroundService.setServiceBind(false);
        context.unbindService(serviceConnection);
        LocalBroadcastManager.getInstance(context).unregisterReceiver(socketConnectReceiver);
        LocalBroadcastManager.getInstance(context).unregisterReceiver(messageReceiver);
        LocalBroadcastManager.getInstance(context).unregisterReceiver(recentlyRegistered);
        LocalBroadcastManager.getInstance(context).unregisterReceiver(callReceiver);
        LocalBroadcastManager.getInstance(context).unregisterReceiver(userStatus);
    }

    @Override
    public void onSocketConnected() {
        if (activeSocketListener != null) {
            activeSocketListener.onSocketConnected();
        }
    }

    @Override
    public void onSocketDisconnected() {
        if (activeSocketListener != null) {
            activeSocketListener.onSocketDisconnected();
        }
    }

    @Override
    public void onMessageReceiver(String s) {
        if (activeSocketListener != null) {
            activeSocketListener.onMessageReceiver(s);
        }
    }

    @Override
    public void onRecentlyRegistered(String json_string) {
        if (activeSocketListener != null) {
            activeSocketListener.onRecentlyRegistered(json_string);
        }
    }

    @Override
    public void onCallReceiver() {
        if (activeSocketListener != null) {
            activeSocketListener.onCallReceiver();
        }
    }

    @Override
    public void onUserStatus(String json_string) {
        if (activeSocketListener != null) {
            activeSocketListener.onUserStatus(json_string);
        }
    }

    public void addOnHandler(String event, Emitter.Listener listener) {
        socketBackgroundService.addOnHandler(event, listener);
    }

    public void emit(String event, Object[] args, Ack ack) {
        socketBackgroundService.emit(event, args, ack);
    }

    public void emit(String event, Object... args) {
        socketBackgroundService.emit(event, args);
    }

    public void connect() {
        socketBackgroundService.connect();
    }

    private void disconnect() {
        socketBackgroundService.disconnect();
    }

    public void off(String event) {
        if (socketBackgroundService != null) {
            socketBackgroundService.off(event);
        }
    }

    public void restartSocket() {
        if (socketBackgroundService != null) {
            socketBackgroundService.restartSocket();
        }
    }

    private void removeMessageReceiverHandler() {
        if (socketBackgroundService != null) {
            socketBackgroundService.removeMessageReceiveHandler();
        }
    }

    private void removeVideoCallReceiverHandler() {
        if (socketBackgroundService != null) {
            socketBackgroundService.removeVideoCallReceiverHandler();
        }
    }

    private void removeRecentlyRegistererHandler() {
        if (socketBackgroundService != null) {
            socketBackgroundService.removeRecentlyRegistererHandler();
        }
    }

    private void removeUserStatusHandler() {
        if (socketBackgroundService != null) {
            socketBackgroundService.removeUserStatusHandler();
        }
    }

    public void removeHandlers() {
        AppSocketListener.getInstance().disconnect();
        removeMessageReceiverHandler();
        removeVideoCallReceiverHandler();
        removeRecentlyRegistererHandler();
        removeUserStatusHandler();
    }

}
