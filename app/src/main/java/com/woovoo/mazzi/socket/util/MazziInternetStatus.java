package com.woovoo.mazzi.socket.util;

public class MazziInternetStatus {

    private boolean isConnected = false;

    public MazziInternetStatus(boolean isConnected) {
        this.isConnected = isConnected;
    }

    public boolean isConnected() {
        return isConnected;
    }
}
